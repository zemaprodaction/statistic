FROM openjdk:15-slim as builder
WORKDIR statistic
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} statistic.jar
RUN java -Djarmode=layertools -jar statistic.jar extract

FROM adoptopenjdk:13-jre-hotspot
WORKDIR statistic
ENV JAVA_TOOL_OPTIONS -agentlib:jdwp=transport=dt_socket,address=*:5006,server=y,suspend=n
COPY --from=builder statistic/dependencies/ ./
COPY --from=builder statistic/spring-boot-loader/ ./
COPY --from=builder statistic/snapshot-dependencies/ ./
COPY --from=builder statistic/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
