package com.onlinegym.statistic.utils

import com.nhaarman.mockitokotlin2.mock
import reactor.core.publisher.Mono
import java.security.Principal
val PRINCIPAL_NAME = "user"
val principal: Principal = mock()
val principalMono = Mono.just(principal)

