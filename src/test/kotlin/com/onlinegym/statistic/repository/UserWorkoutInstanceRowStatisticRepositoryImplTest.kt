package com.onlinegym.statistic.repository

import com.onlinegym.statistic.StatisticApplication
import com.onlinegym.statistic.entity.UserWorkoutInstanceRowStatisticData
import org.junit.jupiter.api.AfterEach

import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [StatisticApplication::class])
internal class UserWorkoutInstanceRowStatisticRepositoryImplTest {

    @Autowired
    private lateinit var repositoryUser: UserWorkoutInstanceRowStatisticRepository

    @Autowired
    private lateinit var template: ReactiveMongoTemplate

    @AfterEach
    fun clearDataBase() {
        template.dropCollection(UserWorkoutInstanceRowStatisticData::class.java).subscribe()
    }

//    @Test
//    fun save() {
//        val workoutDataList = listOf(WorkoutData(workoutInstanceId = "wi1", userId = "user1", workoutId = "work1", workoutProgramId = "wp1"),
//            WorkoutData(workoutInstanceId = "wi1", userId = "user1", workoutId = "work1", workoutProgramId = "wp1"))
//        repository.save(workoutDataList).collectList().block()
//        StepVerifier.create(repository.findByUserId("user1"))
//            .expectNextMatches {
//                it.userId == "user1"
//            }.expectNextMatches {
//                it.userId == "user1"
//            }.verifyComplete()
//    }
//
//    @Test
//    fun findTheMostPopularWorkoutTraining() {
//        val workoutDataList = listOf(WorkoutData(workoutInstanceId = "wi1", userId = "user1", workoutId = "work1", workoutProgramId = "wp1"),
//            WorkoutData(workoutInstanceId = "wi2", userId = "user1", workoutId = "work3", workoutProgramId = "wp1"),
//            WorkoutData(workoutInstanceId = "wi3", userId = "user1", workoutId = "work2", workoutProgramId = "wp1"),
//            WorkoutData(workoutInstanceId = "wi2", userId = "user1", workoutId = "work3", workoutProgramId = "wp1"),
//            WorkoutData(workoutInstanceId = "wi1", userId = "user1", workoutId = "work1", workoutProgramId = "wp1"),
//            WorkoutData(workoutInstanceId = "wi1", userId = "user2", workoutId = "work1", workoutProgramId = "wp1"),
//            WorkoutData(workoutInstanceId = "wi1", userId = "user1", workoutId = "work1", workoutProgramId = "wp1"))
//        repository.save(workoutDataList).collectList().block()
//        StepVerifier.create(repository.findTheMostPopularWorkoutTraining("user1"))
//            .expectNextMatches {
//                it.workoutId == "work1"
//            }.expectNextMatches {
//                it.workoutId == "work3"
//            }.expectNextMatches {
//                it.workoutId == "work2"
//            }.verifyComplete()
//    }
//
//    @Test
//    fun getUserHistory() {
//        val currentDate = LocalDateTime.now()
//        val workoutDataList = listOf(WorkoutData(workoutInstanceId = "wi1", userId = "user1", workoutId = "work1", workoutProgramId = "wp1",
//            date = currentDate),
//            WorkoutData(workoutInstanceId = "wi2", userId = "user1", workoutId = "work3", workoutProgramId = "wp1", date = currentDate.minusDays(30)),
//            WorkoutData(workoutInstanceId = "wi3", userId = "user1", workoutId = "work2", workoutProgramId = "wp1", date = currentDate.minusDays(20)),
//            WorkoutData(workoutInstanceId = "wi2", userId = "user1", workoutId = "work3", workoutProgramId = "wp1", date = currentDate.minusDays(26)),
//            WorkoutData(workoutInstanceId = "wi1", userId = "user1", workoutId = "work1", workoutProgramId = "wp1", date = currentDate.minusDays(16)),
//            WorkoutData(workoutInstanceId = "wi1", userId = "user1", workoutId = "work1", workoutProgramId = "wp1", date = currentDate.minusDays(15)),
//            WorkoutData(workoutInstanceId = "wi1", userId = "user1", workoutId = "work1", workoutProgramId = "wp1", date = currentDate.minusDays(31)))
//        repository.save(workoutDataList).collectList().block()
//        StepVerifier.create(repository.getUserHistory("user1", 0, 7))
//            .expectNextMatches {
//                it.date.toLocalDate() == currentDate.toLocalDate()
//            }.expectNextMatches {
//                it.date.toLocalDate() == currentDate.minusDays(15).toLocalDate()
//            }.expectNextMatches {
//                it.date.toLocalDate()  == currentDate.minusDays(16).toLocalDate()
//            }.expectNextMatches {
//                it.date.toLocalDate()  == currentDate.minusDays(20).toLocalDate()
//            }.expectNextMatches {
//                it.date.toLocalDate()  == currentDate.minusDays(26).toLocalDate()
//            }.expectNextMatches {
//                it.date.toLocalDate()  == currentDate.minusDays(30).toLocalDate()
//            }.expectNextMatches {
//                it.date.toLocalDate()  == currentDate.minusDays(31).toLocalDate()
//            }.verifyComplete()
//
//        StepVerifier.create(repository.getUserHistory("user1", 6, 7))
//           .expectNextMatches {
//                it.date.toLocalDate()  == currentDate.minusDays(31).toLocalDate()
//            }.verifyComplete()
//    }
}
