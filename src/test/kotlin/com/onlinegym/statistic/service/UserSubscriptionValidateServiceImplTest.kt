package com.onlinegym.statistic.service

import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.service.user.UserSubscriptionValidateServiceImpl
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class UserSubscriptionValidateServiceImplTest {
    val service = UserSubscriptionValidateServiceImpl()
    private val testWPId = "testWPID"

    private val userId = "testId"
    val workoutProgram = UserWorkoutProgram(trainingWorkoutProgramId = testWPId, userId = userId)
    @Test
    fun whenUserAlreadyHasSubscriptionShouldReturnFalse() {
        val user = User(userId, testWPId)

        val result = service.isUserCanSubscribeToWorkoutProgram(user, workoutProgram)
        assertFalse(result);
    }

    @Test
    fun `when user does not have subscription should return true`() {
        val user = User(userId)
        val result = service.isUserCanSubscribeToWorkoutProgram(user, workoutProgram)
        assertTrue(result);
    }
}
