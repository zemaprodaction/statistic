package com.onlinegym.statistic.service

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import com.onlinegym.statistic.dto.ExerciseResultDto
import com.onlinegym.statistic.dto.ExerciseType
import com.onlinegym.statistic.dto.WorkoutInstanceResultDto
import com.onlinegym.statistic.entity.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import reactor.kotlin.core.publisher.toMono

import reactor.test.StepVerifier

internal class UserWorkoutInstanceCalculatedStatisticServiceImplTest {

    private val exerciseRowStatisticsService = mock(UserExerciseRowStatisticsService::class.java)
    private val service = UserWorkoutInstanceCalculatedStatisticServiceImpl(exerciseRowStatisticsService)
    private val measures = Measure(measureTypeId = "m1", actualValue = 10.0, expectedValue = 100.0, measurePrefix = MeasurePrefix.SEC.prefix)
    private val element = UserExerciseRowStatisticData(restTime = 10, exerciseType = ExerciseType.STRENGTH, actualRestTime = 10, measures = listOf(measures), exerciseId = "exID", userId = "user")
    private val exercisesResultList = listOf(element, element, element)
    private val result = WorkoutInstanceResult(workoutInstanceId = "wi1", exercisesRowStatisticListId = exercisesResultList.map { "exId" },
        totalWorkoutTime = 100, totalWork = 100)
    private val userWorkoutInstanceRowStatisticData = UserWorkoutInstanceRowStatisticData(userWorkoutInstanceId = "1", userId = "user",
        workoutInstanceResult = result)
    @BeforeEach
    fun setUp(){
        whenever(exerciseRowStatisticsService.getExerciseByRangeId(any())).thenReturn(exercisesResultList.toMono())
    }

    @Test
    fun `should return sum of planned rest of all workout sets`() {

        StepVerifier.create(service.getWorkoutInstanceResultByWorkoutInstanceResult(userWorkoutInstanceRowStatisticData, emptyList())).expectNextMatches {
            it.restTime.planedRestTime == 30
        }.verifyComplete()
    }

    @Test
    fun `should return sum of actual rest of all workout sets`() {
        StepVerifier.create(service.getWorkoutInstanceResultByWorkoutInstanceResult(userWorkoutInstanceRowStatisticData, emptyList())).expectNextMatches {
            it.restTime.actualRestTime == 30
        }.verifyComplete()
    }

    @Test
    fun `should return total work of all workout sets`() {
        StepVerifier.create(service.getWorkoutInstanceResultByWorkoutInstanceResult(userWorkoutInstanceRowStatisticData, emptyList())).expectNextMatches {
            it.percentOfCompletedWork == 10
        }.verifyComplete()
    }

    @Test
    fun `exercise summary should not be empty`() {
        StepVerifier.create(service.getWorkoutInstanceResultByWorkoutInstanceResult(userWorkoutInstanceRowStatisticData, emptyList())).expectNextMatches {
            it.exerciseSummary.size == 1
        }.verifyComplete()
    }

    @Test
    fun `total workout time should not be 100`() {
        StepVerifier.create(service.getWorkoutInstanceResultByWorkoutInstanceResult(userWorkoutInstanceRowStatisticData, emptyList())).expectNextMatches {
            it.totalWorkoutTime == 100
        }.verifyComplete()
    }

}
