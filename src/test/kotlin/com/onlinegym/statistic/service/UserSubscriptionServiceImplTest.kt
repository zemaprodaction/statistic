package com.onlinegym.statistic.service

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.whenever
import com.onlinegym.statistic.dto.SubscribtionDto
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.excptionHandler.StatisticServiceException
import com.onlinegym.statistic.service.user.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import reactor.test.StepVerifier
import java.security.Principal

internal class UserSubscriptionServiceImplTest {

    private val userService = mock(UserService::class.java)
    private val userSubscriptionValidateService = mock(UserSubscriptionValidateService::class.java)
    private val userWorkoutProgramService = mock(UserWorkoutProgramService::class.java)
    private val userWorkoutProgramArchiveService = mock(UserWorkoutProgramArchiveService::class.java)
    private val service: UserSubscriptionService = UserSubscriptionServiceImpl(userService, userSubscriptionValidateService,
        userWorkoutProgramService, userWorkoutProgramArchiveService)

    private val principal = mock(Principal::class.java)
    private val principalMono = principal.toMono()

    private val userId = "userId"
    private val userWorkoutProgramId = "testUserWorkoutProgram"
    private val userWorkoutProgram = UserWorkoutProgram(trainingWorkoutProgramId = userWorkoutProgramId, userId = userId)
    private val user = User(userId, userWorkoutProgramId)

    @BeforeEach
    fun initialize() {
        whenever(principal.name).thenReturn(userId)
        whenever(userService.findById(userId)).thenReturn(Mono.empty())
        whenever(userService.save(any())).thenReturn(user.toMono())
        whenever(userWorkoutProgramService.getActiveWorkoutProgramForUser(any())).thenReturn(userWorkoutProgram.toMono())
        whenever(userWorkoutProgramService.save(any())).thenReturn(userWorkoutProgram.toMono())
        whenever(userSubscriptionValidateService.isUserCanSubscribeToWorkoutProgram(any(), any())).thenReturn(true)
        whenever(userSubscriptionValidateService.isUserCanArchiveCurrentWorkoutProgram(any(), any())).thenReturn(true)
        whenever(userWorkoutProgramArchiveService.addWorkoutProgramToArchive(any())).thenReturn(userWorkoutProgram.toMono())
    }

    @Test
    fun `when user exists should return user`() {
        val subscriptionDto = SubscribtionDto(trainingWorkoutProgramId = userWorkoutProgramId)
        StepVerifier.create(service.subscribeUserToWorkoutProgram(principal.toMono(), subscriptionDto.toMono()))
            .expectNextMatches {
                it.id == user.id && it.activeWorkoutProgramId !== null
            }
            .verifyComplete()
    }

    @Test
    fun `when user not exists should save user`() {
        whenever(userWorkoutProgramService.getActiveWorkoutProgramForUser(any())).thenReturn(Mono.empty())
        val subscriptionDto = SubscribtionDto(trainingWorkoutProgramId = userWorkoutProgramId)
        StepVerifier.create(service.subscribeUserToWorkoutProgram(principal.toMono(), subscriptionDto.toMono()))
            .expectNextMatches {
                it.id == user.id
            }.verifyComplete()
        verify(userService, times(1)).save(any())
    }

    @Test
    fun `when user subscription does not pass validate should throw exception`() {
        whenever(userSubscriptionValidateService.isUserCanSubscribeToWorkoutProgram(any(), any())).thenReturn(false)
        whenever(userSubscriptionValidateService.isUserCanArchiveCurrentWorkoutProgram(any(), any())).thenReturn(false)
        val subscriptionDto = SubscribtionDto(trainingWorkoutProgramId = userWorkoutProgramId)
        StepVerifier.create(service.subscribeUserToWorkoutProgram(principal.toMono(), subscriptionDto.toMono()))
            .expectError(StatisticServiceException::class.java).verify()
    }

    @Test
    fun `when user does not have active workout program should subscribe to new workout program succssfully`() {
        whenever(userService.findById(userId)).thenReturn(User(userId).toMono())
        whenever(userWorkoutProgramService.getActiveWorkoutProgramForUser(any())).thenReturn(Mono.empty())
        val subscriptionDto = SubscribtionDto(trainingWorkoutProgramId = userWorkoutProgramId)
        StepVerifier.create(service.subscribeUserToWorkoutProgram(principal.toMono(), subscriptionDto.toMono()))
            .expectNext(User(userId, userWorkoutProgramId)).verifyComplete()
        verify(userWorkoutProgramArchiveService, times(0)).addWorkoutProgramToArchive(any())
    }
}
