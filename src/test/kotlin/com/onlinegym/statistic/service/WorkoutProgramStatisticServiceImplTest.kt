package com.onlinegym.statistic.service

import com.onlinegym.statistic.StatisticApplication
import com.onlinegym.statistic.dto.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension


@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [StatisticApplication::class])
internal class WorkoutProgramStatisticServiceImplTest {

    @Autowired
    private lateinit var service: WorkoutProgramStatisticService

    @Autowired
    private lateinit var exerciseDataStatisticService: UserExerciseRowStatisticsService

    @Autowired
    private lateinit var workoutInstanceCalculateStatisticsService: WorkoutInstanceCalculateStatisticsService

    @Autowired
    private lateinit var workoutProgramStatisticService: WorkoutProgramStatisticService

    @Autowired
    private lateinit var userWorkoutInstanceRowService: UserWorkoutInstanceRowStatisticService

    @Autowired
    lateinit var mongoTemplate: ReactiveMongoTemplate

    @MockBean
    lateinit var userWorkoutProgramService: UserWorkoutProgramService

    private lateinit var workoutProgramStatisticDto: WorkoutProgramStatisticDto

    private val WORKOUT_INSTANCE_ID_1 = "test_workout_instance_id_1"

    private val AMOUNT_ALL_WORKOUT1 = 3

    private val WORKOUT_PRORAM_ID = "wp1"

    @BeforeEach
    fun beforeAll() {
//        whenever(principal.name).thenReturn(PRINCIPAL_NAME)
//        val testMeasure = listOf(Measure("repetition", actualValue = 50.0, expectedValue = 100.0, MeasurePrefix.COUNT.prefix))
//        val finishedExercise = listOf(ExerciseDto(measures = testMeasure, exerciseId = "testEx1", actualRestTime = 30),
//            ExerciseDto(measures = testMeasure, exerciseId = "testEx2", actualRestTime = 40))
//        val workoutData =
//            WorkoutDataDto(workoutInstanceId = WORKOUT_INSTANCE_ID_1, exercises = finishedExercise, totalWorkoutTime = 100, workoutDataId = "wi1")
//        workoutProgramStatisticDto =
//            WorkoutProgramStatisticDto(workoutProgramId = WORKOUT_PRORAM_ID,
//                workout = WorkoutDto(workoutId = "testWorkoutId1", workoutData = workoutData), amountAllWorkout = AMOUNT_ALL_WORKOUT1,
//                nowUTCTime())
//        val scheduleDay1 = ScheduledDay(listOf(ScheduledWorkout(isFinished = true, id = "workout_instance_1",
//            userWorkoutInstanceId = "123", listOfWorkoutPhases = emptyList())), LocalDateTime.now())
//        val scheduleDay2 = ScheduledDay(listOf(ScheduledWorkout(isFinished = true, id = "workout_instance_2",
//            userWorkoutInstanceId = "123", listOfWorkoutPhases = emptyList())), LocalDateTime.now())
//        val scheduleDay3 = ScheduledDay(listOf(ScheduledWorkout(isFinished = false, id = "workout_instance_3",
//            userWorkoutInstanceId = "123", listOfWorkoutPhases = emptyList())), LocalDateTime.now())
//        val scheduleDay4 = ScheduledDay(listOf(ScheduledWorkout(isFinished = false, id = "workout_instance_3",
//            userWorkoutInstanceId = "123", listOfWorkoutPhases = emptyList())), LocalDateTime.now())
//        val calendar: Map<LocalDateTime, ScheduledDay> = mapOf(nowUTCTime() to scheduleDay1, nowUTCTime().plusDays(1) to scheduleDay2,
//            nowUTCTime().plusDays(2) to scheduleDay3, nowUTCTime().plusDays(3) to scheduleDay4)
//        val scheduler = UserWorkoutProgramScheduler(calendar)
//        val userWorkoutProgram = UserWorkoutProgram("wp1", "", scheduler, "user")
//        whenever(userWorkoutProgramService.getUserWorkoutProgramByIdAndUser("user", "wp1"))
//            .thenReturn(userWorkoutProgram.toMono())
    }
//
//    @AfterEach
//    fun cleanDatabase() {
//        mongoTemplate.dropCollection(User::class.java).subscribe()
//        mongoTemplate.dropCollection(ExerciseData::class.java).subscribe()
//        mongoTemplate.dropCollection(WorkoutStatistic::class.java).subscribe()
//        mongoTemplate.dropCollection(WorkoutData::class.java).subscribe()
//        mongoTemplate.dropCollection(WorkoutProgramStatistic::class.java).subscribe()
//    }

//    @Test
//    fun `should save user with workout program statistic when call save method`() {
//        service.saveWorkoutProgramStatistics(principalMono, workoutProgramStatisticDto.toMono()).block()
//
//        StepVerifier.create(workoutProgramStatisticService.getAllWorkoutProgramStatistics(principalMono))
//            .expectNextMatches {
//                it.workoutProgramId == WORKOUT_PRORAM_ID &&
//                    it.finishedWorkoutInstance.size == 1
//            }.verifyComplete()
//
//        service.saveWorkoutProgramStatistics(principalMono, workoutProgramStatisticDto.toMono()).block()
//
//        StepVerifier.create(workoutProgramStatisticService.getAllWorkoutProgramStatistics(principalMono))
//            .expectNextMatches {
//                it.workoutProgramId == WORKOUT_PRORAM_ID &&
//                    it.finishedWorkoutInstance.size == 2
//            }.verifyComplete()
//
//        StepVerifier.create(exerciseDataStatisticService.findExerciseStatisticForUser(principalMono))
//            .expectNextCount(4)
//            .verifyComplete()
//
//    }
//
//    @Test
//    fun `should save workout statistic to user when call save method`() {
//        service.saveWorkoutProgramStatistics(principalMono, workoutProgramStatisticDto.toMono()).block()
//        StepVerifier.create(workoutDataService.findByUserId(principalMono))
//            .expectNextMatches {
//                it.percentOfCompletedWork == 50
//            }.verifyComplete()
//    }
//
//    @Test
//    fun `should save exercise data statistic call save method`() {
//        service.saveWorkoutProgramStatistics(principalMono, workoutProgramStatisticDto.toMono()).block()
//        StepVerifier.create(exerciseDataStatisticService.findExerciseStatisticForUser(principalMono))
//            .expectNextCount(2).verifyComplete()
//    }
//
//    @Test
//    fun `should find workout program by id`() {
//        service.saveWorkoutProgramStatistics(principalMono, workoutProgramStatisticDto.toMono()).block()
//        StepVerifier.create(workoutProgramStatisticService.getWorkoutProgramStatistic(principalMono, WORKOUT_PRORAM_ID))
//            .expectNextCount(1).verifyComplete()
//    }
//
//    @Test
//    fun `should return valid WorkoutSummaryDto when save all statistic`() {
//        StepVerifier.create(service.saveWorkoutProgramStatistics(principalMono, workoutProgramStatisticDto.toMono()))
//            .expectNextMatches {
//                it.exerciseCount == 2 &&
//                    it.actualRestTimeFormat == "00:01:10.000"
//            }.verifyComplete()
//
//    }
//
//    @Test
//    fun getPercentProgressOfWorkoutProgram() {
//
//        StepVerifier.create(service.getPercentProgressOfWorkoutProgram("user", "wp1"))
//            .expectNextMatches {
//                it == 50
//            }.verifyComplete()
//
//    }

}

