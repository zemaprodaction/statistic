package com.onlinegym.statistic.service

import com.nhaarman.mockitokotlin2.whenever
import com.onlinegym.statistic.StatisticApplication
import com.onlinegym.statistic.dto.ExerciseType
import com.onlinegym.statistic.entity.UserExerciseRowStatisticData
import com.onlinegym.statistic.entity.Measure
import com.onlinegym.statistic.entity.MeasurePrefix
import com.onlinegym.statistic.utils.PRINCIPAL_NAME
import com.onlinegym.statistic.utils.principal
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension


@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [StatisticApplication::class])
internal class UserExerciseRowStatisticsServiceImplTest {

    @Autowired
    lateinit var serviceUser: UserExerciseRowStatisticsService

    val measures =
        listOf(Measure(measureTypeId = "repetition", actualValue = 10.0, expectedValue = 10.0, MeasurePrefix.COUNT.prefix),
            Measure(measureTypeId = "weight", actualValue = 20.0, expectedValue = 20.0, MeasurePrefix.KILO.prefix))

    val measures2 =
        listOf(Measure(measureTypeId = "repetition", actualValue = 11.0, expectedValue = 10.0, MeasurePrefix.COUNT.prefix),
            Measure(measureTypeId = "weight", actualValue = 20.0, expectedValue = 20.0, MeasurePrefix.KILO.prefix))

    val measures3 =
        listOf(Measure(measureTypeId = "repetition", actualValue = 10.0, expectedValue = 10.0, MeasurePrefix.COUNT.prefix),
            Measure(measureTypeId = "weight", actualValue = 23.0, expectedValue = 20.0, MeasurePrefix.KILO.prefix))
    val measures4 =
        listOf(Measure(measureTypeId = "repetition", actualValue = 10.0, expectedValue = 10.0, MeasurePrefix.COUNT.prefix))
    val measures5 =
        listOf(Measure(measureTypeId = "repetition", actualValue = 15.0, expectedValue = 10.0, MeasurePrefix.COUNT.prefix))

    @BeforeEach
    fun beforeAll() {
        whenever(principal.name).thenReturn(PRINCIPAL_NAME)
        val exercisData1 =
            UserExerciseRowStatisticData(exerciseId = "ex1", userId = principal.name, measures = measures, exerciseType = ExerciseType.STRENGTH)
        val exercisData2 =
            UserExerciseRowStatisticData(exerciseId = "ex1", userId = principal.name, measures = measures2, exerciseType = ExerciseType.STRENGTH)
        val exercisData3 =
            UserExerciseRowStatisticData(exerciseId = "ex1", userId = principal.name, measures = measures3, exerciseType = ExerciseType.STRENGTH)
        val exercisData4 =
            UserExerciseRowStatisticData(exerciseId = "ex2", userId = principal.name, measures = measures4, exerciseType = ExerciseType.STRENGTH)
        val exercisData5 =
            UserExerciseRowStatisticData(exerciseId = "ex2", userId = principal.name, measures = measures5, exerciseType = ExerciseType.STRENGTH)
        serviceUser.save(exercisData1).block()
        serviceUser.save(exercisData2).block()
        serviceUser.save(exercisData3).block()
        serviceUser.save(exercisData4).block()
        serviceUser.save(exercisData5).block()
    }

//    @Test
//    fun `should return most valueble exercise data`() {
//        StepVerifier.create(serviceUser.getRecordForExercise(principalMono, "ex1")).expectNextMatches {
//            it.measures == measures3
//        }.verifyComplete()
//    }
//
//    @Test
//    fun `should return most valueble exercise data with one measure`() {
//        StepVerifier.create(serviceUser.getRecordForExercise(principalMono, "ex2")).expectNextMatches {
//            it.measures == measures5
//        }.verifyComplete()
//    }
}
