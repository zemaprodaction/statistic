package com.onlinegym.statistic.service

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.whenever
import com.onlinegym.statistic.dto.ScheduledWorkout
import com.onlinegym.statistic.dto.TrainingWorkoutProgramDto
import com.onlinegym.statistic.dto.WorkoutDayDto
import com.onlinegym.statistic.dto.WorkoutWeekDto
import com.onlinegym.statistic.entity.UserWorkoutInstance
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.repository.WorkoutProgramSchedulerRepository
import com.onlinegym.statistic.service.user.TrainingWorkoutProgramService
import com.onlinegym.statistic.service.user.WorkoutProgramSchedulerServiceImpl
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import reactor.kotlin.core.publisher.toMono
import reactor.test.StepVerifier
import java.time.LocalDate

internal class WorkoutProgramSchedulerServiceImplTest {

    private val trainingWorkoutProgramService = mock(TrainingWorkoutProgramService::class.java)
    private val userWorkoutInstanceService = mock(UserWorkoutInstanceService::class.java)
    private val repository = mock(WorkoutProgramSchedulerRepository::class.java)
    private val service: WorkoutProgramSchedulerServiceImpl =
        WorkoutProgramSchedulerServiceImpl(trainingWorkoutProgramService, repository, userWorkoutInstanceService)

    private val workoutProgramId = "wpId"

    @BeforeEach
    fun init() {
        whenever(repository.save(any()))
            .then { i -> i.arguments[0].toMono() }
        whenever(userWorkoutInstanceService.addNewListWorkoutInstance(any()))
            .then { i -> i.arguments[0].toMono() }
        whenever(userWorkoutInstanceService.getWorkoutInstanceList(anyList()))
            .then { i -> (i.arguments[0] as List<*>).map {UserWorkoutInstance(userWorkoutProgramId = "1", userId = "1", trainingWorkoutId = "trID") }.toMono() }
    }

    @Test
    fun `when create Scheduler for Workout Program should contains days equal to workout program day`() {
        val listOfDays1 = listOf(WorkoutDayDto())
        val listOfDays2 = listOf(WorkoutDayDto(), WorkoutDayDto())
        val listOfWeeks = listOf(WorkoutWeekDto(listOfDays1), WorkoutWeekDto(listOfDays2))
        whenever(trainingWorkoutProgramService.getTrainingWorkoutProgram(workoutProgramId))
            .thenReturn(TrainingWorkoutProgramDto(id = workoutProgramId, listOfWeeks = listOfWeeks).toMono())
        val userWorkoutProgram = UserWorkoutProgram(userId = "testUser", trainingWorkoutProgramId = workoutProgramId)
        StepVerifier.create(service.generateSchedulerWorkoutProgram(userWorkoutProgram)).expectNextMatches {
            it.listOfDays.size == 3
        }.verifyComplete()
        verify(repository, times(1)).save(any())
    }


    @Test
    fun `when create Scheduler for Workout Program should contains days equal to 1`() {
        val listOfDays1 = listOf(WorkoutDayDto())
        val listOfWeeks = listOf(WorkoutWeekDto(listOfDays1))
        whenever(trainingWorkoutProgramService.getTrainingWorkoutProgram(workoutProgramId))
            .thenReturn(TrainingWorkoutProgramDto(id = workoutProgramId, listOfWeeks = listOfWeeks).toMono())
        val userWorkoutProgram = UserWorkoutProgram(userId = "testUser", trainingWorkoutProgramId = workoutProgramId)
        StepVerifier.create(service.generateSchedulerWorkoutProgram(userWorkoutProgram)).expectNextMatches {
            it.listOfDays.size == 1
        }.verifyComplete()
        verify(repository, times(1)).save(any())
    }

    @Test
    fun `when create Scheduler for Workout Program with one week and one day should contains one day with today date`() {
        val listOfDays1 = listOf(WorkoutDayDto())
        val listOfWeeks = listOf(WorkoutWeekDto(listOfDays1))
        whenever(trainingWorkoutProgramService.getTrainingWorkoutProgram(workoutProgramId))
            .thenReturn(TrainingWorkoutProgramDto(id = workoutProgramId, listOfWeeks = listOfWeeks).toMono())
        val userWorkoutProgram = UserWorkoutProgram(userId = "testUser", trainingWorkoutProgramId = workoutProgramId)
        StepVerifier.create(service.generateSchedulerWorkoutProgram(userWorkoutProgram)).expectNextMatches {
            it.listOfDays.size == 1 &&
                it.listOfDays[0].date.toLocalDate() == LocalDate.now()
        }.verifyComplete()
    }

    @Test
    fun `when create Scheduler for Workout Program with one week and 3 days should contains 3 days with today today + 2, today + 6`() {
        val listOfDays1 = listOf(WorkoutDayDto(dayIndex = 1), WorkoutDayDto(dayIndex = 3), WorkoutDayDto(dayIndex = 7))
        val listOfWeeks = listOf(WorkoutWeekDto(listOfDays1))
        whenever(trainingWorkoutProgramService.getTrainingWorkoutProgram(workoutProgramId))
            .thenReturn(TrainingWorkoutProgramDto(id = workoutProgramId, listOfWeeks = listOfWeeks).toMono())
        val userWorkoutProgram = UserWorkoutProgram(userId = "testUser", trainingWorkoutProgramId = workoutProgramId)
        StepVerifier.create(service.generateSchedulerWorkoutProgram(userWorkoutProgram)).expectNextMatches {
            it.listOfDays[0].date.toLocalDate() == LocalDate.now() &&
                it.listOfDays[1].date.toLocalDate() == LocalDate.now().plusDays(3) &&
                it.listOfDays[2].date.toLocalDate() == LocalDate.now().plusDays(7)
        }.verifyComplete()
    }

    @Test
    fun `when create Scheduler for Workout Program with two week and 3 days and 2 days should contains 5 days with today today + 2, today + 6, today + 7 + day index and so on`() {
        val listOfDays1 = listOf(WorkoutDayDto(dayIndex = 1), WorkoutDayDto(dayIndex = 3), WorkoutDayDto(dayIndex = 7))
        val listOfDays2 = listOf(WorkoutDayDto(dayIndex = 1), WorkoutDayDto(dayIndex = 7))
        val listOfWeeks = listOf(WorkoutWeekDto(listOfDays1), WorkoutWeekDto(listOfDays2))
        whenever(trainingWorkoutProgramService.getTrainingWorkoutProgram(workoutProgramId))
            .thenReturn(TrainingWorkoutProgramDto(id = workoutProgramId, listOfWeeks = listOfWeeks).toMono())
        val userWorkoutProgram = UserWorkoutProgram(userId = "testUser", trainingWorkoutProgramId = workoutProgramId)
        StepVerifier.create(service.generateSchedulerWorkoutProgram(userWorkoutProgram)).expectNextMatches {
            it.listOfDays[0].date.toLocalDate() == LocalDate.now() &&
                it.listOfDays[1].date.toLocalDate() == LocalDate.now().plusDays(3) &&
                it.listOfDays[2].date.toLocalDate() == LocalDate.now().plusDays(7) &&
                it.listOfDays[3].date.toLocalDate() == LocalDate.now().plusDays(8) &&
                it.listOfDays[4].date.toLocalDate() == LocalDate.now().plusDays(14)
        }.verifyComplete()
    }

    @Test
    fun `when create Scheduler for Workout Program with two week and 3 days and 2 days should save workout instance`() {
        val listOfScheduledWorkouts = listOf(ScheduledWorkout(listOfWorkoutPhases = emptyList(), id = "id"))
        val listOfDays1 = listOf(WorkoutDayDto(dayIndex = 7, listOfScheduledWorkouts = listOfScheduledWorkouts))
        val listOfDays2 = listOf(WorkoutDayDto(dayIndex = 7, listOfScheduledWorkouts = listOfScheduledWorkouts))
        val listOfWeeks = listOf(WorkoutWeekDto(listOfDays1), WorkoutWeekDto(listOfDays2))
        whenever(trainingWorkoutProgramService.getTrainingWorkoutProgram(workoutProgramId))
            .thenReturn(TrainingWorkoutProgramDto(id = workoutProgramId, listOfWeeks = listOfWeeks).toMono())
        val userWorkoutProgram = UserWorkoutProgram(userId = "testUser", trainingWorkoutProgramId = workoutProgramId)
        service.generateSchedulerWorkoutProgram(userWorkoutProgram).block()
        verify(userWorkoutInstanceService, times(2)).addNewListWorkoutInstance(any())
    }
}
