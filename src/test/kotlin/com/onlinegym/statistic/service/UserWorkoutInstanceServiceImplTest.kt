package com.onlinegym.statistic.service

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import com.onlinegym.statistic.dto.ExerciseType
import com.onlinegym.statistic.dto.RestTimeDto
import com.onlinegym.statistic.dto.WorkoutInstanceFinishResult
import com.onlinegym.statistic.dto.WorkoutInstanceResultDto
import com.onlinegym.statistic.entity.*
import com.onlinegym.statistic.repository.UserWorkoutInstanceRepository
import org.junit.jupiter.api.Test

import org.mockito.Mockito.*
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono
import reactor.test.StepVerifier

internal class UserWorkoutInstanceServiceImplTest {
    private val userWorkoutInstanceRepository = mock(UserWorkoutInstanceRepository::class.java)
    private val userExerciseRowStatisticsService = mock(UserExerciseRowStatisticsService::class.java)
    private val userWorkoutInstanceRowStatisticService = mock(UserWorkoutInstanceRowStatisticService::class.java)
    private val userWorkoutInstanceCalculatedStatisticService = mock(UserWorkoutInstanceCalculatedStatisticService::class.java)
    private val userWorkoutProgramRowStatisticService = mock(UserWorkoutProgramRowStatisticService::class.java)

    private val service = UserWorkoutInstanceServiceImpl(userWorkoutInstanceRepository, userExerciseRowStatisticsService,
        userWorkoutInstanceRowStatisticService,
        userWorkoutInstanceCalculatedStatisticService, userWorkoutProgramRowStatisticService)

    private val userWorkoutProgramId = "1"

    private val userId = "user1ID"

    private val defaultWorkoutInstance = UserWorkoutInstance(userWorkoutProgramId = userWorkoutProgramId, userId = userId, trainingWorkoutId = "trID")

    @Test
    fun finishWorkoutInstance() {
        whenever(userWorkoutInstanceRepository.findByTrainingWorkoutInstanceAndUserId(any(), any())).thenReturn(defaultWorkoutInstance.toMono())
        whenever(userWorkoutProgramRowStatisticService.save(any())).thenReturn(UserWorkoutProgramRowStatisticData(
            userWorkoutProgramId = userWorkoutProgramId, userId = userId).toMono())
        whenever(userWorkoutInstanceRowStatisticService.save(any())).thenReturn(UserWorkoutInstanceRowStatisticData(
            userWorkoutInstanceId = userWorkoutProgramId,
            workoutInstanceResult = WorkoutInstanceResult(workoutInstanceId = "wi",
                totalWorkoutTime = 10,
                totalWork = 100), userId = "user").toMono())
        whenever(userWorkoutInstanceRowStatisticService.getStatisticForWorkout(any(), any()))
            .thenReturn(Mono.empty())

        whenever(userWorkoutProgramRowStatisticService.getStatisticForWorkoutProgramAndUser(any(), any())).thenReturn(Mono.empty())
        whenever(userExerciseRowStatisticsService.save(anyList())).thenReturn(listOf(UserExerciseRowStatisticData(userId = userId,
            exerciseId = "exeId", exerciseType = ExerciseType.STRENGTH)).toFlux())
        whenever(userWorkoutInstanceCalculatedStatisticService.getWorkoutInstanceResultByWorkoutInstanceResult(any(), any()))
            .thenReturn(WorkoutInstanceFinishResult(restTime = RestTimeDto(planedRestTime = 30), userWorkoutInstanceRowStatisticId = "123").toMono())
        whenever(userWorkoutInstanceRepository.save(any()))
            .thenReturn(defaultWorkoutInstance.toMono())

        val workoutInstanceResultDto = WorkoutInstanceResultDto(workoutInstanceId = userWorkoutProgramId, totalWorkoutTime = 100)
        StepVerifier.create(service.finishWorkoutInstance(User("user", userWorkoutProgramId), workoutInstanceResultDto))
            .expectNextMatches {
                it.restTime.planedRestTime == 30
            }.verifyComplete()
    }
}
