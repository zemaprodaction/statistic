package com.onlinegym.statistic.service

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import com.onlinegym.statistic.dto.*
import com.onlinegym.statistic.entity.*
import com.onlinegym.statistic.repository.UserRepository
import com.onlinegym.statistic.service.user.UserServiceImpl
import com.onlinegym.statistic.service.user.WorkoutProgramSchedulerService
import com.onlinegym.statistic.utils.PRINCIPAL_NAME
import com.onlinegym.statistic.utils.principal
import com.onlinegym.statistic.utils.principalMono
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import reactor.kotlin.core.publisher.toMono
import reactor.test.StepVerifier

internal class UserServiceImplTest {
    private val repository = mock(UserRepository::class.java)
    val service = UserServiceImpl(repository)

    @BeforeEach
    fun beforeAll() {
        whenever(principal.name).thenReturn(PRINCIPAL_NAME)
    }


    @Test
    fun `should throw exception when user try to subscribe new workout program when he has other active workout program`() {
//        StepVerifier.create(userService.subscribeToWorkoutProgram(principalMono, workoutProgramId2))
//            .expectError()
//            .verify()
    }


    @Test
    fun `when user unsubscribe active workout program should became null`() {
//        StepVerifier.create(userService.unsubscribe(principalMono)).expectNextMatches {
//            it.activeWorkoutProgramId === null
//        }.verifyComplete()
    }

    @Test
    fun `should add new workout program to list of workout program when user was subscribed to this workout program early`() {
//        userService.unsubscribe(principalMono).block()
//        userService.subscribeToWorkoutProgram(principalMono, workoutProgramId).block()
//        StepVerifier.create(userService.findById(principalMono).flatMap {
//            userWorkoutProgramService.getActiveWorkoutProgramForUser(it)
//        }).expectNextCount(1).verifyComplete()
    }

    //


    @Test
    fun `when user start workout program should setup status to in_progress`() {
//        whenever(trainingWorkoutProgramRepository.getTrainingWorkoutProgramById("workoutProgram1"))
//            .thenReturn(TrainingWorkoutProgramDto("workoutProgram1", genericWeek()).toMono())
//        userService.userStartWorkoutProgram(principalMono).block()
//        StepVerifier.create(userService.findById(principalMono).flatMap { userWorkoutProgramService.getActiveWorkoutProgramForUser(it) })
//            .expectNextMatches {
//                it.status == UserWorkoutProgramStatus.IN_PROGRESS
//            }.verifyComplete()
    }

//    @Test
//    fun `when finish last workout in workout program statust should be change on finished`() {
//
//
//        val testMeasure = listOf(Measure("repetition", actualValue = 50.0, expectedValue = 100.0, MeasurePrefix.COUNT.prefix))
//        val finishedExercise = listOf(ExerciseDto(measures = testMeasure, exerciseId = "testEx1", actualRestTime = 30),
//            ExerciseDto(measures = testMeasure, exerciseId = "testEx2", actualRestTime = 40))
//        val workoutData =
//            WorkoutDataDto(workoutInstanceId = "workout_instance_3", exercises = finishedExercise, totalWorkoutTime = 100, workoutDataId = "wdId")
//        val workoutProgramStatisticDto =
//            WorkoutProgramStatisticDto(workoutProgramId = "workoutProgram1",
//                workout = WorkoutDto(workoutId = "testWorkoutId1", workoutData = workoutData), amountAllWorkout = 1,
//                expectedFinishedDate = nowUTCTime().plusDays(13))
//
//        whenever(trainingWorkoutProgramRepository.getTrainingWorkoutProgramById("workoutProgram1"))
//            .thenReturn(TrainingWorkoutProgramDto("workoutProgram1", genericShortWeek()).toMono())
//
//        userService.userStartWorkoutProgram(principalMono).block()
//        userService.finishedWorkoutScheduler(principalMono, workoutProgramStatisticDto.toMono()).block()
//        StepVerifier.create(userService.findById(principalMono).flatMap { userWorkoutProgramService.getActiveWorkoutProgramForUser(it) })
//            .expectNextMatches {
//                it.status == UserWorkoutProgramStatus.FINISHED
//            }.verifyComplete()
//        userService.unsubscribe(principalMono).block()
//        userService.subscribeToWorkoutProgram(principalMono, workoutProgramId).block()
//        StepVerifier.create(userService.getUserTrainingInfo(principalMono)).expectNextMatches {
//            it.userWorkoutProgram?.status == UserWorkoutProgramStatus.FINISHED
//        }.verifyComplete()
//    }

    private fun genericWeek(): List<WorkoutWeekDto> {
        val listOfScheduleWorkout = listOf(ScheduledWorkout("workout_instance_1", userWorkoutInstanceId = "123", listOfWorkoutPhases = emptyList()))
        val w1day1 = WorkoutDayDto(1, listOfScheduleWorkout)
        val w1day2 = WorkoutDayDto(2, listOfScheduleWorkout)
        val w1day3 = WorkoutDayDto(3, listOfScheduleWorkout)
        val w2day3 = WorkoutDayDto(3, listOfScheduleWorkout)
        val w2day7 = WorkoutDayDto(7, listOfScheduleWorkout)
        return listOf(WorkoutWeekDto(listOf(w1day1, w1day2, w1day3)), WorkoutWeekDto(listOf(w2day3, w2day7)))
    }

    private fun genericShortWeek(): List<WorkoutWeekDto> {
        val listOfScheduleWorkout = listOf(ScheduledWorkout("workout_instance_1", isFinished = true, userWorkoutInstanceId = "123",
            listOfWorkoutPhases = emptyList()))
        val listOfScheduleWorkout2 = listOf(ScheduledWorkout("workout_instance_2", isFinished = true, userWorkoutInstanceId = "123",
            listOfWorkoutPhases = emptyList()))
        val listOfScheduleWorkout3 = listOf(ScheduledWorkout("workout_instance_3", isFinished = false, userWorkoutInstanceId = "123",
            listOfWorkoutPhases = emptyList()))
        val w1day1 = WorkoutDayDto(1, listOfScheduleWorkout)
        val w1day2 = WorkoutDayDto(2, listOfScheduleWorkout2)
        val w2day7 = WorkoutDayDto(7, listOfScheduleWorkout3)
        return listOf(WorkoutWeekDto(listOf(w1day1, w1day2)), WorkoutWeekDto(listOf(w2day7)))
    }
}
