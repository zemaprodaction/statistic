package com.onlinegym.statistic.service

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import com.onlinegym.statistic.StatisticApplication
import com.onlinegym.statistic.dto.*
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.entity.WorkoutProgramScheduler
import com.onlinegym.statistic.nowUTCTime
import com.onlinegym.statistic.repository.TrainingWorkoutProgramRepository
import com.onlinegym.statistic.service.user.UserService
import com.onlinegym.statistic.utils.PRINCIPAL_NAME
import com.onlinegym.statistic.utils.principal
import com.onlinegym.statistic.utils.principalMono

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import reactor.kotlin.core.publisher.toMono
import reactor.test.StepVerifier

@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [StatisticApplication::class])
internal class UserWorkoutProgramServiceImplTest {

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var userWorkoutInstanceRowStatisticService: UserWorkoutInstanceRowStatisticService

    @Autowired
    lateinit var userWorkoutprogramService: UserWorkoutProgramService
    val id = "workoutProgram1"
    val workoutProgramId = WorkoutProgramDto(id).toMono()

    @MockBean()
    private lateinit var trainingWorkoutProgramRepository: TrainingWorkoutProgramRepository

    @BeforeEach
    fun beforeAll() {
        whenever(principal.name).thenReturn(PRINCIPAL_NAME)
        whenever(trainingWorkoutProgramRepository.getTrainingWorkoutProgramById(id))
            .thenReturn(TrainingWorkoutProgramDto(id, genericShortWeek()).toMono())
//        userService.subscribeToWorkoutProgram(principalMono, workoutProgramId).block()
//        userService.userStartWorkoutProgram(principalMono).block()
    }

    private fun genericShortWeek(): List<WorkoutWeekDto> {
        val listOfScheduleWorkout =
            listOf(ScheduledWorkout("workout_instance_1", isFinished = true, actualDate = nowUTCTime(),
                workoutDataId = "wd1", userWorkoutInstanceId = "123", listOfWorkoutPhases = emptyList()))
        val listOfScheduleWorkout2 =
            listOf(ScheduledWorkout("workout_instance_2", isFinished = true, actualDate = nowUTCTime().plusDays(1),
                workoutDataId = "wd2", userWorkoutInstanceId = "123", listOfWorkoutPhases = emptyList()))
        val listOfScheduleWorkout3 =
            listOf(ScheduledWorkout("workout_instance_3", isFinished = true, actualDate = nowUTCTime().plusDays(16),
                workoutDataId = "wd3", userWorkoutInstanceId = "123", listOfWorkoutPhases = emptyList()))
        val w1day1 = WorkoutDayDto(1, listOfScheduleWorkout)
        val w1day2 = WorkoutDayDto(2, listOfScheduleWorkout2)
        val w2day7 = WorkoutDayDto(7, listOfScheduleWorkout3)
        return listOf(WorkoutWeekDto(listOf(w1day1, w1day2)), WorkoutWeekDto(listOf(w2day7)))
    }
}
