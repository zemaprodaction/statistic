package com.onlinegym.statistic.dto

import com.onlinegym.statistic.entity.Measure
import com.onlinegym.statistic.entity.MeasurePrefix
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

internal class WorkoutInstanceFinishResultTest {

//    @Test
//    fun getTotalWorkForMeasureRepetiton() {
//        val exercises: List<ExerciseDto> = listOf(ExerciseDto(measures = listOf(Measure(measureTypeId = "m_id", 10.0,
//            10.0,
//            measurePrefix = MeasurePrefix.COUNT.prefix))))
//        val workoutDto: WorkoutDto = getWorkoutDto(exercises)
//        val summary = WorkoutInstanceFinishResult.calculateSummary(workoutDto) {
//            calculateActualTotalWork()
//        }
//        Assertions.assertThat(summary.totalWork).isEqualTo(10)
//    }
//
//    @Test
//    fun getTotalWorkForMeasureRepetitonAndWeight() {
//        val exercises: List<ExerciseDto> = listOf(ExerciseDto(measures = listOf(Measure(measureTypeId = "m_id", 10.0,
//            10.0,
//            measurePrefix = MeasurePrefix.COUNT.prefix), Measure(measureTypeId = "m_id2", actualValue = 20.0, expectedValue = 20.0,
//            measurePrefix = MeasurePrefix.KILO.prefix))))
//        val workoutDto: WorkoutDto = getWorkoutDto(exercises)
//        val summary = WorkoutInstanceFinishResult.calculateSummary(workoutDto) {
//            calculateActualTotalWork()
//        }
//        Assertions.assertThat(summary.totalWork).isEqualTo(200)
//    }
//
//    @Test
//    fun getTotalWorkForMeasureTimeAndDistance() {
//        val exercises: List<ExerciseDto> = listOf(ExerciseDto(measures = listOf(Measure(measureTypeId = "m_id", 10.0,
//            10.0,
//            measurePrefix = MeasurePrefix.SEC.prefix), Measure(measureTypeId = "m_id2", actualValue = 20.0, expectedValue = 20.0,
//            measurePrefix = MeasurePrefix.METER.prefix))))
//        val workoutDto: WorkoutDto = getWorkoutDto(exercises)
//        val summary = WorkoutInstanceFinishResult.calculateSummary(workoutDto) {
//            calculateActualTotalWork()
//        }
//        Assertions.assertThat(summary.totalWork).isEqualTo(40)
//    }
//
//
//    private fun getWorkoutDto(exercises: List<ExerciseDto>): WorkoutDto {
//        val workoutInstanceId = "testID"
//        val totalWorkoutTime = 10
//        val workoutDataId = "testId"
//        val workoutData = WorkoutDataDto(exercises, workoutInstanceId, totalWorkoutTime, workoutDataId)
//        val workoutDto = WorkoutDto(workoutId = "test_workout_id", workoutData = workoutData)
//        return workoutDto
//    }
}
