package com.onlinegym.statistic.routing

import com.nhaarman.mockitokotlin2.whenever
import com.onlinegym.statistic.StatisticApplication
import com.onlinegym.statistic.dto.*
import com.onlinegym.statistic.entity.*
import com.onlinegym.statistic.service.*
import com.onlinegym.statistic.service.user.UserSubscriptionService
import com.onlinegym.statistic.utils.principal
import com.onlinegym.statistic.utils.principalMono
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.kotlin.core.publisher.toMono
import reactor.test.StepVerifier
import java.time.LocalDate

@ExtendWith(SpringExtension::class, MockitoExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [StatisticApplication::class])
internal class UserWorkoutInstanceRoutesTest {
    @Autowired
    private lateinit var client: WebTestClient

    @Autowired
    private lateinit var userWorkoutInstanceService: UserWorkoutInstanceService

    @Autowired
    private lateinit var userWorkoutProgramRowStatisticService: UserWorkoutProgramRowStatisticService

    @Autowired
    private lateinit var userExerciseRowStatisticsService: UserExerciseRowStatisticsService

    @Autowired
    private lateinit var userWorkoutInstanceRowStatisticService: UserWorkoutInstanceRowStatisticService

    @Autowired
    private lateinit var userWorkoutProgramService: UserWorkoutProgramService

    @Autowired
    private lateinit var subscriptionService: UserSubscriptionService

    @Autowired
    private lateinit var mongoTemplate: ReactiveMongoTemplate

    private var wi: UserWorkoutInstance = UserWorkoutInstance(userWorkoutProgramId = "wpId", userId = "user", trainingWorkoutId = "trID");

    private val element = ExerciseResultDto(restTime = 10, exerciseType = ExerciseType.STRENGTH, measures = listOf(Measure(measureTypeId = "t", actualValue = 10.0, expectedValue = 10.0)))
    private val exercisesResultList = listOf(element, element, element)
    private var workoutInstanceResultDto = WorkoutInstanceResultDto(workoutInstanceId = wi.id, exercisesResultList, 100)
    @BeforeEach
    fun beforeAll() {
        val listOfSet = listOf(WorkoutSet(restTime = 10), WorkoutSet(restTime = 10), WorkoutSet(restTime = 10))
        val listOfWorkoutPhase = listOf(WorkoutInstancePhase(listOfSets = listOfSet))
        whenever(principal.name).thenReturn("user")
        subscriptionService.subscribeUserToWorkoutProgram(principalMono, SubscribtionDto("wpId").toMono()).block()
        wi = userWorkoutInstanceService
            .addNewWorkoutInstance(UserWorkoutInstance(userWorkoutProgramId = "wpId", userId = "user", listOfWorkoutPhases = listOfWorkoutPhase, trainingWorkoutId = "trID"))
            .block() ?: throw Exception()
        workoutInstanceResultDto = WorkoutInstanceResultDto(workoutInstanceId = wi.id, exercisesResultList, 100)
    }

    @AfterEach
    fun cleanDataBase() {
        mongoTemplate.collectionNames.flatMap {
            mongoTemplate.dropCollection(it)
        }.blockLast()
    }



    @Test
    @WithMockUser
    fun `when request to start workout program should return status 200`() {
        client.post()
            .uri("/${PathVariableConstants.USER_WORKOUT_INSTANCE_FINISH}")
            .body(workoutInstanceResultDto.toMono(), WorkoutInstanceResultDto::class.java)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.restTime.planedRestTime").isEqualTo("30")

        StepVerifier.create(userWorkoutInstanceService.getWorkoutInstance(wi.id)).expectNextMatches {
            it.status == UserWorkoutInstanceStatus.FINISHED
        }.verifyComplete()
    }

    @Test
    @WithMockUser
    fun `when request to start workout program should save row statistic for workout program`() {
        client.post()
            .uri("/${PathVariableConstants.USER_WORKOUT_INSTANCE_FINISH}")
            .body(workoutInstanceResultDto.toMono(), WorkoutInstanceResultDto::class.java)
            .exchange()
            .expectStatus().isOk
        StepVerifier.create(userWorkoutProgramRowStatisticService.getStatisticForWorkoutProgramAndUser("user", "wpId"))
            .expectNextMatches {
                it.startDate.toLocalDate() == LocalDate.now()
            }.verifyComplete()
    }

    @Test
    @WithMockUser
    fun `when request finish workout program should save row statistic for exercise`() {

        client.post()
            .uri("/${PathVariableConstants.USER_WORKOUT_INSTANCE_FINISH}")
            .body(workoutInstanceResultDto.toMono(), WorkoutInstanceResultDto::class.java)
            .exchange()
            .expectStatus().isOk
        StepVerifier.create(userExerciseRowStatisticsService.getUserExerciseStatisticForUser("user"))
            .expectNextCount(3).verifyComplete()
    }

    @Test
    @WithMockUser
    fun `when request to start workout program should save row statistic for workout instance`() {
        userWorkoutProgramService.userStartWorkoutProgram(User("user", "wpId")).block()
        val result = client.post()
            .uri("/${PathVariableConstants.USER_WORKOUT_INSTANCE_FINISH}")
            .body(workoutInstanceResultDto.toMono(), WorkoutInstanceResultDto::class.java)
            .exchange()
            .expectStatus().isOk.returnResult(WorkoutInstanceFinishResult::class.java)
        StepVerifier.create(result.responseBody).expectNextMatches {
            it.percentOfCompletedWork == 100
        }.verifyComplete()
        StepVerifier.create(userWorkoutInstanceRowStatisticService.getStatisticForWorkout("user", "trID"))
            .expectNextCount(1).verifyComplete()
    }
}
