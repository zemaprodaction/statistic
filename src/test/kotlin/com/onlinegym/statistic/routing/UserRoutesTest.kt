package com.onlinegym.statistic.routing

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import com.onlinegym.statistic.StatisticApplication
import com.onlinegym.statistic.dto.*
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.entity.UserWorkoutProgramStatus
import com.onlinegym.statistic.service.user.TrainingWorkoutProgramService
import com.onlinegym.statistic.service.user.UserService
import com.onlinegym.statistic.service.UserWorkoutProgramService
import com.onlinegym.statistic.service.UserWorkoutInstanceService
import com.onlinegym.statistic.service.user.WorkoutProgramSchedulerService
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.kotlin.core.publisher.toMono
import reactor.test.StepVerifier
import java.time.LocalDate

@ExtendWith(SpringExtension::class, MockitoExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [StatisticApplication::class])
internal class UserRoutesTest {
    @Autowired
    private lateinit var client: WebTestClient

    @Autowired
    private lateinit var userWorkoutProgramService: UserWorkoutProgramService

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var schedulerService: WorkoutProgramSchedulerService

    @Autowired
    private lateinit var userWorkoutInstanceService: UserWorkoutInstanceService

    @MockBean
    private lateinit var trainingWorkoutProgramService: TrainingWorkoutProgramService

    private val userId = "user"

    private val workoutProgramId = "testUserWPid"

    private val userWorkoutProgramid = "userWorkoutProgrramId"

    private val testUser = User(userId, userWorkoutProgramid)

    @BeforeEach
    fun beforeAll() {
        val listOfScheduledWorkouts = listOf(ScheduledWorkout(id = "id", listOfWorkoutPhases = emptyList()))
        val listOfDays1 =
            listOf(WorkoutDayDto(dayIndex = 1, listOfScheduledWorkouts = listOfScheduledWorkouts), WorkoutDayDto(dayIndex = 4, listOfScheduledWorkouts))
        val listOfDays2 = listOf(WorkoutDayDto(dayIndex = 7, listOfScheduledWorkouts))
        val listOfWeeks = listOf(WorkoutWeekDto(listOfDays1), WorkoutWeekDto(listOfDays2))
        whenever(trainingWorkoutProgramService.getTrainingWorkoutProgram(any()))
            .thenReturn(TrainingWorkoutProgramDto(id = workoutProgramId, listOfWeeks = listOfWeeks).toMono())
        userService.save(testUser).block()
        userWorkoutProgramService.save(UserWorkoutProgram(id = userWorkoutProgramid, trainingWorkoutProgramId = workoutProgramId, userId = userId))
            .block()
    }

    @Test
    @WithMockUser
    fun `when request to start workout program should return status 200`() {
        client.post()
            .uri("/${PathVariableConstants.USER_START_WORKOUT_PROGRAM}")
            .exchange()
            .expectStatus().isOk
            .expectBody(WorkoutProgramSchedulerDto::class.java)
    }

    @Test
    @WithMockUser
    fun `when request to start workout program should contains all schedule day dto`() {
        client.post()
            .uri("/${PathVariableConstants.USER_START_WORKOUT_PROGRAM}")
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .jsonPath("$.listOfDays").isArray
            .jsonPath("$.listOfDays[0].userWorkoutInstanceList").isArray
            .jsonPath("$.listOfDays[0].userWorkoutInstanceList[0].id").isNotEmpty
    }


    @Test
    @WithMockUser
    fun `when request to start workout program should update user workout program status to in_progress`() {
        client.post()
            .uri("/${PathVariableConstants.USER_START_WORKOUT_PROGRAM}")
            .exchange()
            .expectStatus().isOk
        StepVerifier.create(userWorkoutProgramService
            .getActiveWorkoutProgramForUser(testUser)).expectNextMatches {
            it.status == UserWorkoutProgramStatus.IN_PROGRESS
        }.verifyComplete()
    }

    @Test
    @WithMockUser
    fun `when request to start workout program should generate scheduler for workout program`() {
        client.post()
            .uri("/${PathVariableConstants.USER_START_WORKOUT_PROGRAM}")
            .exchange()
            .expectStatus().isOk
        StepVerifier.create(userWorkoutProgramService
            .getActiveWorkoutProgramForUser(testUser)).expectNextMatches {
            it.workoutProgramSchedulerId != null
        }.verifyComplete()
    }


    @Test
    @WithMockUser
    fun `when request to start workout program should generate scheduler with size 3 with correct date`() {
        client.post()
            .uri("/${PathVariableConstants.USER_START_WORKOUT_PROGRAM}")
            .exchange()
            .expectStatus().isOk
        StepVerifier.create(userWorkoutProgramService.getActiveWorkoutProgramForUser(testUser).flatMap {
            schedulerService.getSchedulerById(it.workoutProgramSchedulerId!!)
        }).expectNextMatches {
            it.listOfDays.size == 3 &&
                it.listOfDays[0].date.toLocalDate() == LocalDate.now() &&
                it.listOfDays[1].date.toLocalDate() == LocalDate.now().plusDays(4) &&
                it.listOfDays[2].date.toLocalDate() == LocalDate.now().plusDays(14)
        }.verifyComplete()
    }

    @Test
    @WithMockUser
    fun `when request to start workout program should save all workout instance for user`() {
        client.post()
            .uri("/${PathVariableConstants.USER_START_WORKOUT_PROGRAM}")
            .exchange()
            .expectStatus().isOk
        StepVerifier.create(userWorkoutInstanceService
            .getAllWorkoutInstanceForUserWorkoutProgram(userId, userWorkoutProgramid)).expectNextMatches {
            it.size == 3
        }.verifyComplete()
    }

    @Test
    @WithMockUser
    fun `when user does not subscribe to workout program should return only user id`() {
        client.post()
            .uri("/${PathVariableConstants.USER_START_WORKOUT_PROGRAM}")
            .exchange()
            .expectStatus().isOk
        client.get()
            .uri("/${PathVariableConstants.USER_TRAINING_INFO}")
            .exchange()
            .expectBody()
            .jsonPath("$.userId").isNotEmpty
    }

    @Test
    @WithMockUser
    fun `when user does not start workout program should return user training info without scheduler`() {
        client.get()
            .uri("/${PathVariableConstants.USER_TRAINING_INFO}")
            .exchange()
            .expectBody()
            .jsonPath("$.userId").isNotEmpty
            .jsonPath("$.userWorkoutProgram").isNotEmpty
    }
}
