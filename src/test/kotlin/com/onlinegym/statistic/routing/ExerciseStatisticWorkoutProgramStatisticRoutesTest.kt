package com.onlinegym.statistic.routing

import com.onlinegym.statistic.StatisticApplication
import com.onlinegym.statistic.repository.UserExerciseRowStatisticRepository
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.beans.factory.annotation.Autowired


@ExtendWith(SpringExtension::class, MockitoExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [StatisticApplication::class])
internal class ExerciseStatisticWorkoutProgramStatisticRoutesTest {

    @Autowired
    private lateinit var client: WebTestClient

    @Autowired
    private lateinit var userExerciseRowStatisticRepository: UserExerciseRowStatisticRepository

//    @Test
//    @WithMockUser
//    fun `when Get user statistic then correct type return`() {
//        client.get()
//            .uri("/${PathVariableConstants.EXERCISE_GROUP_BY_TYPE}")
//            .exchange()
//            .expectStatus()
//            .isOk
//
//    }

}
