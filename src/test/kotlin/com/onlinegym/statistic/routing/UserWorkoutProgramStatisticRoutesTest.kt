package com.onlinegym.statistic.routing

import com.onlinegym.statistic.StatisticApplication
import com.onlinegym.statistic.dto.*
import com.onlinegym.statistic.repository.TrainingWorkoutProgramRepository
import com.onlinegym.statistic.service.user.UserService
import com.onlinegym.statistic.service.UserWorkoutProgramService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient


@ExtendWith(SpringExtension::class, MockitoExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [StatisticApplication::class])
internal class UserWorkoutProgramStatisticRoutesTest {

    @Autowired
    private lateinit var client: WebTestClient

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var userWorkoutProgramService: UserWorkoutProgramService
    val workoutProgramId = "workoutProgram1"

    @MockBean()
    private lateinit var trainingWorkoutProgramRepository: TrainingWorkoutProgramRepository

    @BeforeEach
    fun beforeAll() {

//        userService.save(User(PRINCIPAL_NAME)).block()
    }

    @Test
    @WithMockUser
    fun `when Get user statistic then correct type return`() {

//        client
//            .post()
//            .uri("/${PathVariableConstants.USER_SUBSCRIBE}")
//            .bodyValue(WorkoutProgramDto(workoutProgramId))
//            .exchange()
//            .returnResult<UserWorkoutProgram>()
//            .expectStatus()
//            .isOk
//        StepVerifier.create(userService.findById(PRINCIPAL_NAME))
//            .expectNextMatches {
//                it.activeWorkoutProgramId == workoutProgramId
//            }.verifyComplete()
    }

    private fun genericWeek(): List<WorkoutWeekDto> {
        val listOfScheduleWorkout = listOf(ScheduledWorkout(id = "workout_instance_1", "123", userWorkoutInstanceId = "123", listOfWorkoutPhases = emptyList()))
        val w1day1 = WorkoutDayDto(1, listOfScheduleWorkout)
        val w1day2 = WorkoutDayDto(2, listOfScheduleWorkout)
        val w1day3 = WorkoutDayDto(3, listOfScheduleWorkout)
        val w2day3 = WorkoutDayDto(3, listOfScheduleWorkout)
        val w2day7 = WorkoutDayDto(7, listOfScheduleWorkout)
        return listOf(WorkoutWeekDto(listOf(w1day1, w1day2, w1day3)), WorkoutWeekDto(listOf(w2day3, w2day7)))
    }
}
