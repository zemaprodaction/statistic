package com.onlinegym.statistic.routing

import com.onlinegym.statistic.StatisticApplication
import com.onlinegym.statistic.dto.*
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.entity.UserWorkoutProgramStatus
import com.onlinegym.statistic.service.user.UserService
import com.onlinegym.statistic.service.user.UserWorkoutProgramArchiveService
import com.onlinegym.statistic.service.UserWorkoutProgramService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.http.HttpStatus
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.kotlin.core.publisher.toMono
import reactor.test.StepVerifier


@ExtendWith(SpringExtension::class, MockitoExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [StatisticApplication::class])
@DirtiesContext()
internal class UserSubscriptionRoutesTest {

    @Autowired
    private lateinit var client: WebTestClient

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var userWorkoutProgramService: UserWorkoutProgramService

    @Autowired
    private lateinit var userWorkoutProgramArchiveService: UserWorkoutProgramArchiveService

    @Autowired
    private lateinit var mongoTemplate: ReactiveMongoTemplate

    private val workoutProgramId = "testWPId"

    @BeforeEach
    fun setUp() {
        mongoTemplate.collectionNames.flatMap {
            mongoTemplate.dropCollection(it)
        }.blockLast()
    }

    @Test
    @WithMockUser
    fun `when request to start workout program should return 200`() {
        client.post()
            .uri("/${PathVariableConstants.USER_SUBSCRIBE_TO_WORKOUT_PROGRAM}")
            .exchange()
            .expectStatus()
            .isOk
    }


    @Test
    @WithMockUser
    fun `when request to start workout program should return userWorkoutProgram and save user`() {
        client.post()
            .uri("/${PathVariableConstants.USER_SUBSCRIBE_TO_WORKOUT_PROGRAM}")
            .body(SubscribtionDto(workoutProgramId).toMono(), SubscribtionDto::class.java)
            .exchange()
            .expectBody()
            .jsonPath("$.id").isEqualTo("user")
            .jsonPath("$.activeWorkoutProgramId").isNotEmpty
    }

    @Test
    @WithMockUser
    fun `when user has subscription should return 405 code with message`() {
        userService.save(User("user", "wpId2")).block()
        userWorkoutProgramService.save(UserWorkoutProgram(id = "wpId2", trainingWorkoutProgramId = "wpId2", userId = "user",
            status = UserWorkoutProgramStatus.IN_PROGRESS)).block()
        client.post()
            .uri("/${PathVariableConstants.USER_SUBSCRIBE_TO_WORKOUT_PROGRAM}")
            .body(SubscribtionDto(workoutProgramId).toMono(), SubscribtionDto::class.java)
            .exchange()
            .expectStatus().isEqualTo(HttpStatus.NOT_ACCEPTABLE)
        StepVerifier.create(userService.findById("user"))
            .expectNext(User("user", "wpId2")).verifyComplete()
    }

    @Test
    @WithMockUser
    fun `when user has finished workout program and try subscribe to new program should old workout program added to archive and subscribe to new wp`() {
        userService.save(User("user", "wpId2")).block()
        userWorkoutProgramService.save(UserWorkoutProgram(id = "wpId2", trainingWorkoutProgramId = "wpId2", userId = "user",
            status = UserWorkoutProgramStatus.FINISHED)).block()
        client.post()
            .uri("/${PathVariableConstants.USER_SUBSCRIBE_TO_WORKOUT_PROGRAM}")
            .body(SubscribtionDto(workoutProgramId).toMono(), SubscribtionDto::class.java)
            .exchange()
            .expectStatus().isEqualTo(HttpStatus.OK)
        StepVerifier.create(userWorkoutProgramArchiveService.getAllWorkoutProgramArchiveForUser(User("user")))
            .expectNextMatches {
                it.status == UserWorkoutProgramStatus.ARCHIVED
            }.verifyComplete()
        StepVerifier.create(userService.getAllUsers())
            .expectNextMatches {
                it.size == 1
            }.verifyComplete()
    }


    @Test
    @WithMockUser
    fun `when user does not have active workout program should subscribe succsessfuly to new WP`() {
        userService.save(User("user")).block()
        client.post()
            .uri("/${PathVariableConstants.USER_SUBSCRIBE_TO_WORKOUT_PROGRAM}")
            .body(SubscribtionDto(workoutProgramId).toMono(), SubscribtionDto::class.java)
            .exchange()
            .expectStatus().isEqualTo(HttpStatus.OK)
        StepVerifier.create(userService.findById("user"))
            .expectNextMatches {
                it.id == "user" && it.activeWorkoutProgramId !== null
            }
            .verifyComplete()
    }


    @Test
    @WithMockUser
    fun `when user unsubscribe should clean his active workout program`() {
        userService.save(User("user", activeWorkoutProgramId = "testWp")).block()
        userWorkoutProgramService.save(UserWorkoutProgram(id = "testWp", trainingWorkoutProgramId = "trId", userId = "user")).block()
        client.post()
            .uri("/${PathVariableConstants.USER_UNSUBSCRIBE_TO_WORKOUT_PROGRAM}")
            .exchange()
            .expectStatus().isEqualTo(HttpStatus.OK)

        StepVerifier.create(userService.findById("user"))
            .expectNextMatches {
                it.activeWorkoutProgramId == null
            }.verifyComplete()
    }

    @Test
    @WithMockUser
    fun `when user unsubscribe should put workout program to archive`() {
        userWorkoutProgramService.save(UserWorkoutProgram(id = "testWp", trainingWorkoutProgramId = "test",
            userId = "user", status = UserWorkoutProgramStatus.IN_PROGRESS)).block()

        userService.save(User("user", activeWorkoutProgramId = "testWp")).block()
        client.post()
            .uri("/${PathVariableConstants.USER_UNSUBSCRIBE_TO_WORKOUT_PROGRAM}")
            .exchange()
            .expectStatus().isEqualTo(HttpStatus.OK)

        StepVerifier.create(userWorkoutProgramService.getUserWorkoutProgramById("testWp"))
            .expectNextMatches {
                it.status == UserWorkoutProgramStatus.ARCHIVED
            }.verifyComplete()
    }

}
