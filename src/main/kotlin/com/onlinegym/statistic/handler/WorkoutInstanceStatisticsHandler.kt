package com.onlinegym.statistic.handler

import com.onlinegym.statistic.dto.WorkoutInstanceFinishResult
import com.onlinegym.statistic.routing.PathVariableConstants
import com.onlinegym.statistic.service.UserWorkoutInstanceCalculatedStatisticService
import com.onlinegym.statistic.service.UserWorkoutInstanceRowStatisticService
import com.onlinegym.statistic.service.UserWorkoutInstanceService
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse.ok

class WorkoutInstanceStatisticsHandler(
    private val userWorkoutInstanceService: UserWorkoutInstanceService,
    private val userWorkoutInstanceCalculatedStatisticService: UserWorkoutInstanceCalculatedStatisticService,
    private val userWorkoutInstanceRowStatisticService: UserWorkoutInstanceRowStatisticService) {

//    fun getTheMostPopularWorkoutsForUser(request: ServerRequest) =
//        ok().body(
//            userWorkoutInstanceRowStatisticService.getThe5MostPopularWorkoutForUser(request.principal()),
//            WorkoutRating::class.java
//        )
//
//    fun getUserHistory(request: ServerRequest) =
//        ok().body(
//            userWorkoutInstanceRowStatisticService.getUserHistory(request.principal(), request.queryParam("skip").orElse("0").toLong(),
//                request.queryParam("take").orElse("0").toLong()),
//            UserWorkoutInstanceRowStatisticData::class.java
//        )

    fun getWorkoutInstanceFinishResult(request: ServerRequest) =
        ok().body(
            userWorkoutInstanceRowStatisticService.getUserWorkoutInstanceRowStatistic(
                request.pathVariable(PathVariableConstants.USER_WORKOUT_INSTANCE_STATISTIC_ID)).zipWith(request.principal()).flatMap {
                userWorkoutInstanceService.getWorkoutInstance(it.t1.userWorkoutInstanceId).flatMap {userWorkoutInstance ->
                    userWorkoutInstanceRowStatisticService.getStatisticForWorkout(it.t2.name, userWorkoutInstance.trainingWorkoutId)
                        .flatMap {userWorkoutInstancList ->
                        userWorkoutInstanceCalculatedStatisticService.getWorkoutInstanceResultByWorkoutInstanceResult(it.t1, userWorkoutInstancList)
                    }
                }
            },
            WorkoutInstanceFinishResult::class.java
        )
}

