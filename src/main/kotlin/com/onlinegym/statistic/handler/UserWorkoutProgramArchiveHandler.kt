package com.onlinegym.statistic.handler

import com.onlinegym.statistic.dto.WorkoutRating
import com.onlinegym.statistic.service.UserWorkoutProgramService
import com.onlinegym.statistic.service.user.UserService
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse.ok

class UserWorkoutProgramArchiveHandler(
    private val userWorkoutProgramService: UserWorkoutProgramService) {

//    fun getUserArchiveWorkoutProgramResult(request: ServerRequest) =
//        ok().body(
//            userService.findById(request.principal())
//                .flatMap {
//                    userWorkoutProgramService
//                        .getUserArchiveWorkoutProgramResult(it, request.pathVariable("userWorkoutProgramArchiveId"))
//                },
//            WorkoutRating::class.java
//        )
//    fun subscribeToWorkoutProgram(request: ServerRequest) = ok().body(
//        userService.subscribeToWorkoutProgram(request.principal(), request.bodyToMono(SubscribtionDto::class.java)),
//        UserWorkoutProgram::class.java
//    )

//    fun finishWorkout(request: ServerRequest) =
//        ok().body(
//            userService.finishedWorkoutScheduler(request.principal(), request.bodyToMono(WorkoutProgramStatisticDto::class.java)),
//            WorkoutRating::class.java
//        )

//    fun getUserActiveWorkoutProgramResult(request: ServerRequest) =
//        ok().body(
//            userService.findById(request.principal()).flatMap { userWorkoutProgramService.getUserWorkoutProgramResult(it) },
//            WorkoutRating::class.java
//        )

//    fun getUserArchiveWorkoutProgramResult(request: ServerRequest) =
//        ok().body(
//            userService.findById(request.principal())
//                .flatMap {
//                    userWorkoutProgramService
//                        .getUserArchiveWorkoutProgramResult(it, request.pathVariable("userWorkoutProgramArchiveId"))
//                },
//            WorkoutRating::class.java
//        )

//    fun getAllFinishedWorkoutProgramForUser(request: ServerRequest) =
//        ok().body(
//            userService.findById(request.principal()).flatMap { userWorkoutProgramService.getAllFinishedWorkoutProgramByUser(it) },
//            List::class.java
//        )
}

