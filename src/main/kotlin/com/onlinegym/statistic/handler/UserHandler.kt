package com.onlinegym.statistic.handler

import com.onlinegym.statistic.dto.UserTrainingInfoDto
import com.onlinegym.statistic.entity.WorkoutProgramScheduler
import com.onlinegym.statistic.service.user.UserService
import com.onlinegym.statistic.service.user.UserTrainingInfoService
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse.ok

class UserHandler(
    private val userTrainingInfoService: UserTrainingInfoService) {

    fun getUserTrainingInfo(request: ServerRequest) = ok().body(
        userTrainingInfoService.getUserTrainingInfo(request.principal()),
        UserTrainingInfoDto::class.java
    )

    fun getUserTrainingInfoById(request: ServerRequest) = ok().body(
        userTrainingInfoService.getUserTrainingInfo(request.pathVariable("userId")),
        UserTrainingInfoDto::class.java
    )
}

