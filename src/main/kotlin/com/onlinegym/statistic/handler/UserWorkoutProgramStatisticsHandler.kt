package com.onlinegym.statistic.handler

import com.onlinegym.statistic.dto.UserWorkoutProgramCalculateStatisticData
import com.onlinegym.statistic.routing.PathVariableConstants
import com.onlinegym.statistic.service.UserWorkoutProgramStatisticService
import com.onlinegym.statistic.service.WorkoutProgramStatisticService
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse.ok

class UserWorkoutProgramStatisticsHandler(
    private val userWorkoutProgramStatisticService: UserWorkoutProgramStatisticService) {

//    fun saveWorkoutProgramStatistics(request: ServerRequest) =
//        ok().body(
//            workoutProgramStatisticService.saveWorkoutProgramStatistics(
//                request.principal(),
//                request.bodyToMono(WorkoutProgramStatisticDto::class.java)
//            ),
//            WorkoutProgramStatistic::class.java
//        )
//
//    fun getAllWorkoutProgramStatistics(request: ServerRequest) =
//        ok().body(
//            workoutProgramStatisticService.getAllWorkoutProgramStatistics(request.principal()),
//            WorkoutProgramStatistic::class.java
//        )


    fun getUserWorkoutProgramStatistic(request: ServerRequest) =
        ok().body(
            request.principal().flatMap {
                userWorkoutProgramStatisticService.getUserWorkoutProgramCalculateStatistic(it.name,
                    request.pathVariable(PathVariableConstants.USER_WORKOUT_PROGRAM_ID))
            },
            UserWorkoutProgramCalculateStatisticData::class.java
        )
//    fun getSpecificUserWorkoutProgramStatistic(request: ServerRequest) =
//        ok().body(
//            workoutProgramStatisticService.getWorkoutProgramStatistic(request.pathVariable(PathVariableConstants.USER_ID), request.pathVariable(PathVariableConstants.WORKOUT_PROGRAM_ID)),
//            WorkoutProgramStatistic::class.java
//        )

}

