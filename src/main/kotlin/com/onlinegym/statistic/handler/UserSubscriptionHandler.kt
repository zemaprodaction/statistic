package com.onlinegym.statistic.handler

import com.onlinegym.statistic.dto.SubscribtionDto
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.service.user.UserSubscriptionService
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse.ok

class UserSubscriptionHandler(
    private val userSubscriptionService: UserSubscriptionService) {

    fun subscribeToWorkoutProgram(request: ServerRequest) = ok().body(
        userSubscriptionService.subscribeUserToWorkoutProgram(request.principal(), request.bodyToMono(SubscribtionDto::class.java)),
        UserWorkoutProgram::class.java
    )

    fun unsubscribe(request: ServerRequest) = ok().body(
        userSubscriptionService.unsubscribeUserToWorkoutProgram(request.principal()),
        UserWorkoutProgram::class.java
    )

    fun userRestartWorkoutProgram(request: ServerRequest) = ok().body(
        userSubscriptionService.userRestartWorkoutProgram(request.principal()),
        UserWorkoutProgram::class.java
    )

}

