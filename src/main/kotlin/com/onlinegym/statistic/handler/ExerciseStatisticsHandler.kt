package com.onlinegym.statistic.handler

import com.onlinegym.statistic.dto.DateRange
import com.onlinegym.statistic.entity.ExerciseStatistics
import com.onlinegym.statistic.routing.PathVariableConstants.Companion.EXERCISE_ID
import com.onlinegym.statistic.service.UserExerciseRowStatisticsService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse.ok

class ExerciseStatisticsHandler(private val userExerciseRowStatisticService: UserExerciseRowStatisticsService) {

    var logger: Logger = LoggerFactory.getLogger(ExerciseStatisticsHandler::class.java)

//    fun getExerciseCountGroupingByType(request: ServerRequest) =
//        ok().body(
//            userExerciseRowStatisticService.getExerciseCountGroupingByType(
//                request.principal(),
//                request.queryParam("dateRange").orElse(DateRange.ALL_TIME.name)
//            ),
//            ExerciseStatistics::class.java
//        ).doOnError {
//            logger.error(it.localizedMessage)
//        }
//
    fun getExerciseRecord(request: ServerRequest) =
        ok().body(
            userExerciseRowStatisticService.getRecordForExercise(
                request.principal(),
                request.pathVariable(EXERCISE_ID)
            ),
            ExerciseStatistics::class.java
        ).doOnError {
            logger.error(it.localizedMessage)
        }
}
