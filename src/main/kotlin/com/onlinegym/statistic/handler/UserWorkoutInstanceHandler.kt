package com.onlinegym.statistic.handler

import com.onlinegym.statistic.dto.WorkoutInstanceFinishResult
import com.onlinegym.statistic.dto.WorkoutInstanceResultDto
import com.onlinegym.statistic.entity.UserWorkoutInstance
import com.onlinegym.statistic.service.UserWorkoutInstanceService
import com.onlinegym.statistic.service.user.UserService
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse.ok

class UserWorkoutInstanceHandler(
    private val userService: UserService,
    private val userWorkoutInstanceService: UserWorkoutInstanceService) {

    fun finishWorkoutInstance(request: ServerRequest) = ok().body(
        request.bodyToMono(WorkoutInstanceResultDto::class.java).zipWith(request.principal()).flatMap {
            userService.findById(it.t2.name).flatMap { user ->
                userWorkoutInstanceService.finishWorkoutInstance(user, it.t1)
            }

        },
        WorkoutInstanceFinishResult::class.java
    )

    fun getWorkoutInstance(request: ServerRequest) = ok().body(
        userWorkoutInstanceService.getWorkoutInstance(request.pathVariable("userWorkoutInstanceId")),
        UserWorkoutInstance::class.java
    )

    fun updateWorkoutInstance(request: ServerRequest) = ok().body(
        request.bodyToMono(UserWorkoutInstance::class.java).flatMap {
            userWorkoutInstanceService.updateWorkoutInstance(it)
        },
        UserWorkoutInstance::class.java
    )

//    fun finishWorkout(request: ServerRequest) =
//        ok().body(
//            userService.finishedWorkoutScheduler(request.principal(), request.bodyToMono(WorkoutProgramStatisticDto::class.java)),
//            WorkoutRating::class.java
//        )

//    fun getUserActiveWorkoutProgramResult(request: ServerRequest) =
//        ok().body(
//            userService.findById(request.principal()).flatMap { userWorkoutProgramService.getUserWorkoutProgramResult(it) },
//            WorkoutRating::class.java
//        )


//    fun getAllFinishedWorkoutProgramForUser(request: ServerRequest) =
//        ok().body(
//            userService.findById(request.principal()).flatMap { userWorkoutProgramService.getAllFinishedWorkoutProgramByUser(it) },
//            List::class.java
//        )
}

