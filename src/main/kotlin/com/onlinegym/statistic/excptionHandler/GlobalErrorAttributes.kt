package com.onlinegym.statistic.excptionHandler

import org.springframework.boot.web.reactive.error.DefaultErrorAttributes
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest

@Component
class GlobalErrorAttributes : DefaultErrorAttributes() {

    override fun getErrorAttributes(request: ServerRequest, includeStackTrace: Boolean): MutableMap<String, Any> {
        return this.assembleError(request)
    }

    private fun assembleError(request: ServerRequest): MutableMap<String, Any> {
        val errorAttributes: MutableMap<String, Any> = LinkedHashMap()
        val error: Throwable = getError(request)
        if (error is StatisticServiceException) {
            errorAttributes["status"] = error.status
            errorAttributes["message"] = error.localizedMessage
        } else {
            errorAttributes["status"] = HttpStatus.INTERNAL_SERVER_ERROR
            errorAttributes["message"] = error.localizedMessage
        }
        return errorAttributes
    }
}
