package com.onlinegym.statistic.excptionHandler

import org.springframework.http.HttpStatus

class StatisticServiceException(
    val status: HttpStatus,
    message: String
) : Exception(message)
