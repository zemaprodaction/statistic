package com.onlinegym.statistic

import com.onlinegym.statistic.handler.*
import com.onlinegym.statistic.repository.*
import com.onlinegym.statistic.routing.*
import com.onlinegym.statistic.service.*
import com.onlinegym.statistic.service.user.*
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.support.GenericApplicationContext
import org.springframework.context.support.beans

val beans = beans {
    bean<UserServiceImpl>()
    bean<UserWorkoutInstanceServiceImpl>()
    bean<UserSubscriptionServiceImpl>()
    bean<UserSubscriptionValidateServiceImpl>()
    bean<WorkoutProgramSchedulerServiceImpl>()
    bean<TrainingWorkoutProgramServiceImpl>()
    bean<UserWorkoutProgramRowStatisticRepositoryImpl>()
    bean<UserWorkoutProgramRowStatisticServiceImpl>()
    bean<UserWorkoutInstanceCalculatedStatisticServiceImpl>()
    bean<UserTrainingInfoServiceImpl>()
    bean<UserSubscriptionHandler>()
    bean<UserWorkoutInstanceHandler>()
    bean { UserSubscriptionRoutes(ref()).router() }
    bean { UserWorkoutProgramRoutes(ref()).router() }
    bean { UserRoutes(ref()).router() }
    bean { UserWorkoutInstanceRoutes(ref()).router() }
    bean { WorkoutInstanceStatistic(ref()).router() }
    bean { UserWorkoutProgramStatisticRoutes(ref()).router() }
    bean { ExerciseStatisticRoutes(ref()).router() }

    bean<UserWorkoutProgramStatisticsHandler>()
    bean<ExerciseStatisticsHandler>()
    bean<WorkoutInstanceStatisticsHandler>()
    bean<UserWorkoutProgramHandler>()
    bean<TrainerHandler>()
    bean<UserHandler>()
    bean<UserRepositoryImpl>()
    bean<UserWorkoutProgramArchiveRepositoryImpl>()
    bean<UserWorkoutProgramArchiveServiceImpl>()
    bean<UserWorkoutInstanceRowStatisticServiceImpl>()

    bean<ExerciseRepositoryWebClient>()
    bean<UserWorkoutProgramStatisticServiceImpl>()
    bean<UserWorkoutInstanceRowStatisticRepositoryImpl>()
    bean<UserWorkoutProgramServiceImpl>()

    bean<UserWorkoutInstanceRepositoryImpl>()
    bean<WorkoutProgramSchedulerRepositoryImpl>()
    bean<UserSchedulerDayRepositoryImpl>()

    bean { TrainingWorkoutProgramRepositoryImpl() }

//    bean { WorkoutRoutes(ref()).router() }


//    bean { TrainerRoutes(ref()).router() }
//    bean { UserWorkoutProgramRoutes(ref()).router() }
//    bean { ExerciseStatisticRoutes(ref()).router() }
    bean { UserExerciseRowStatisticsServiceImpl(ref(), ref()) }
    bean { UserWorkoutProgramRepositoryImpl(ref()) }
    bean { UserExerciseRowStatisticRepositoryImpl(ref()) }
    bean { WorkoutInstanceCalculateStatisticsServiceImpl(ref(), ref()) }
}

class BeansInitializer : ApplicationContextInitializer<GenericApplicationContext> {
    override fun initialize(context: GenericApplicationContext) {
        beans.initialize(context)
    }

}


