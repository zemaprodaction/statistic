package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.Exercise
import reactor.core.publisher.Mono

interface ExerciseRepository {

    fun findExerciseById(exerciseId: String): Mono<Exercise>
}
