package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.entity.UserWorkoutProgramStatus
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import reactor.core.publisher.Flux

class UserWorkoutProgramArchiveRepositoryImpl(private val template: ReactiveMongoTemplate) : UserWorkoutProgramArchiveRepository {

    override fun findAllUserWorkoutProgramArchiveByUserId(userId: String): Flux<UserWorkoutProgram> =
        Query(Criteria(EntityFieldConstant.FIELD_USER_ID).`is`(userId).and(EntityFieldConstant.FIELD_USER_WORKOUT_PROGRAM_STATUS)
            .`is`(UserWorkoutProgramStatus.ARCHIVED)).let {
            template.find(it, UserWorkoutProgram::class.java)
        }

}

