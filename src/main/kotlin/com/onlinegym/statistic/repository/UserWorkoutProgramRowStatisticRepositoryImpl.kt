package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.UserWorkoutProgramRowStatisticData
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import reactor.core.publisher.Mono

class UserWorkoutProgramRowStatisticRepositoryImpl(private val template: ReactiveMongoTemplate) : UserWorkoutProgramRowStatisticRepository {

    override fun save(rowStatisticData: UserWorkoutProgramRowStatisticData): Mono<UserWorkoutProgramRowStatisticData> =
        template.save(rowStatisticData)

    override fun findStatisticByUserIdAndUserWorkoutProgramId(userId: String, userWorkoutProgramId: String): Mono<UserWorkoutProgramRowStatisticData> =
        Query(Criteria(EntityFieldConstant.FIELD_USER_ID).`is`(userId)
            .and(EntityFieldConstant.FIELD_USER_WORKOUT_PROGRAM_ID).`is`(userWorkoutProgramId)).let {
            template.findOne(it, UserWorkoutProgramRowStatisticData::class.java)
        }
}
