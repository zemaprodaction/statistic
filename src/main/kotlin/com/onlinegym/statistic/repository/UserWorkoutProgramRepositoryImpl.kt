package com.onlinegym.statistic.repository

import com.mongodb.client.result.DeleteResult
import com.onlinegym.statistic.entity.UserWorkoutProgram
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

class UserWorkoutProgramRepositoryImpl(private val template: ReactiveMongoTemplate) : UserWorkoutProgramRepository {

    override fun findWorkoutProgramByUserAndTrainingWorkoutProgramId(userId: String, workoutProgramId: String) =
        Query(Criteria(EntityFieldConstant.FIELD_USER_ID).`is`(userId).and("trainingWorkoutProgramId")
            .`is`(workoutProgramId)).let {
            template.findOne(it, UserWorkoutProgram::class.java)
        }

    override fun findAllWorkoutProgramByUser(userName: String) =
        Query(Criteria(EntityFieldConstant.FIELD_USER_ID).`is`(userName)).let {
            template.find(it, UserWorkoutProgram::class.java)
        }

    override fun findWorkoutProgramById(userWorkoutProgramId: String): Mono<UserWorkoutProgram> =
        template.findById(userWorkoutProgramId, UserWorkoutProgram::class.java)


    override fun save(userWorkoutProgram: UserWorkoutProgram) = template.save(userWorkoutProgram)
    override fun delete(userWorkoutProgram: UserWorkoutProgram): Mono<DeleteResult> = template.remove(userWorkoutProgram.toMono())

}
