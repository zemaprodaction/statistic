package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.UserWorkoutInstanceRowStatisticData
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


class UserWorkoutInstanceRowStatisticRepositoryImpl(private val template: ReactiveMongoTemplate) : UserWorkoutInstanceRowStatisticRepository {
    //
//
//    override fun save(workoutDataList: List<WorkoutData>) = template.insertAll(workoutDataList)
//
//    override fun findByUserIdAndWorkoutId(userId: String, workoutId: String) =
//        Query.query(isUserIdCriteria(userId).and(EntityFieldConstant.FIELD_WORKOUT_ID).`is`(workoutId)).let {
//            template.find(it, WorkoutData::class.java)
//        }
//
//    override fun findByUserIdAndWorkoutProgramId(name: String, currentWorkoutProgramId: String) =
//        Query.query(isUserIdCriteria(name).and(EntityFieldConstant.FIELD_WORKOUT_PROGRAM_ID).`is`(currentWorkoutProgramId)).let {
//            template.find(it, WorkoutData::class.java)
//        }
//
//    override fun findTheMostPopularWorkoutTraining(userId: String): Flux<WorkoutRating> =
//        Aggregation.newAggregation(
//            Aggregation.match(isUserIdCriteria(userId)),
//            Aggregation.group(EntityFieldConstant.FIELD_WORKOUT_ID).count().`as`("rating"),
//            Aggregation.sort(Sort.by(Sort.Direction.DESC, "rating"))
//        ).let {
//            template.aggregate(it, WorkoutData::class.java, WorkoutRating::class.java)
//        }
//
//    override fun getUserHistory(userID: String, skip: Long, take: Long): Flux<WorkoutData> =
//        Query(isUserIdCriteria(userID)).with(Sort.by(Sort.Direction.DESC, EntityFieldConstant.FIELD_DATE)).let {
//            template.find(it, WorkoutData::class.java).skip(skip).take(take)
//        }
//
    override fun findById(workoutDataId: String) = template.findById(workoutDataId, UserWorkoutInstanceRowStatisticData::class.java)
//
//    override fun getUserHistory(userID: String): Flux<WorkoutData> =
//        Query(isUserIdCriteria(userID)).with(Sort.by(Sort.Direction.DESC, EntityFieldConstant.FIELD_DATE)).let {
//            template.find(it, WorkoutData::class.java)
//        }
    override fun save(workoutInstanceStatisticData: UserWorkoutInstanceRowStatisticData): Mono<UserWorkoutInstanceRowStatisticData> =
        template.save(workoutInstanceStatisticData)

    override fun findByListWorkoutInstance(workoutInstanceListId: List<String>): Flux<UserWorkoutInstanceRowStatisticData> {
        TODO("Not yet implemented")
    }

//
//
//    override fun findByUserId(userId: String) = Query(isUserIdCriteria(userId)).let {
//        template.find(it, WorkoutData::class.java)
//    }

    private fun isUserIdCriteria(name: String) = Criteria(EntityFieldConstant.FIELD_USER_ID)
        .`is`(name)

    override fun findByUserWorkoutInstanceIdList(userWorkoutInstanceIdList: List<String>): Flux<UserWorkoutInstanceRowStatisticData> =
        Query(Criteria(EntityFieldConstant.USER_WORKOUT_INSTANCE_ID)
            .`in`(userWorkoutInstanceIdList)).let {
            template.find(it, UserWorkoutInstanceRowStatisticData::class.java)
        }

}
