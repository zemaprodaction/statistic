package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.SchedulerDay
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface UserSchedulerDayRepository {

    fun save(userSchedulerDay: SchedulerDay): Mono<SchedulerDay>

    fun saveAll(userSchedulerDay: List<SchedulerDay>): Flux<SchedulerDay>

    fun findSchedulerDayById(id: String): Mono<SchedulerDay>

}
