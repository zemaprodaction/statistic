package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.UserWorkoutInstance
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

class UserWorkoutInstanceRepositoryImpl(private val template: ReactiveMongoTemplate) : UserWorkoutInstanceRepository {

    override fun findAllWorkoutInstanceByUserIdAndUserWorkoutProgramId(userId: String, userWorkotProgramId: String) =
        Query.query(Criteria(EntityFieldConstant.FIELD_USER_ID)
            .`is`(userId).and(EntityFieldConstant.FIELD_USER_WORKOUT_PROGRAM_ID).`is`(userWorkotProgramId)).let {
            template.find(it, UserWorkoutInstance::class.java)
        }

    override fun findById(id: String): Mono<UserWorkoutInstance> = template.findById(id, UserWorkoutInstance::class.java)

    override fun findByTrainingWorkoutInstanceAndUserId(userId: String, userWorkoutInstanceId: String): Mono<UserWorkoutInstance> =
        Query.query(Criteria(EntityFieldConstant.FIELD_USER_ID)
            .`is`(userId).and(EntityFieldConstant.ID).`is`(userWorkoutInstanceId)).let {
            template.findOne(it, UserWorkoutInstance::class.java)
        }

    override fun findByTrainingWorkoutIdAndUserId(userId: String, trainingWorkoutId: String): Flux<UserWorkoutInstance> =
        Query.query(Criteria(EntityFieldConstant.FIELD_USER_ID)
            .`is`(userId).and(EntityFieldConstant.TRAINING_WORKOUT_ID).`is`(trainingWorkoutId)).let {
            template.find(it, UserWorkoutInstance::class.java)
        }

    override fun save(userWorkoutInstance: UserWorkoutInstance): Mono<UserWorkoutInstance> =
        template.save(userWorkoutInstance)

    override fun saveAll(userWorkoutInstanceList: List<UserWorkoutInstance>): Flux<UserWorkoutInstance> =
        template.insertAll(userWorkoutInstanceList)

    override fun findByRangeId(userWorkoutInstanceId: List<String>) =
        Query(Criteria(EntityFieldConstant.ID).`in`(userWorkoutInstanceId)).let {
            template.find(it, UserWorkoutInstance::class.java)
        }
}
