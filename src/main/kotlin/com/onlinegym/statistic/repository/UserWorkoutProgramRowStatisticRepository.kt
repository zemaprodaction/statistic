package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.UserWorkoutProgramRowStatisticData
import reactor.core.publisher.Mono

interface UserWorkoutProgramRowStatisticRepository {
    fun save(rowStatisticData: UserWorkoutProgramRowStatisticData): Mono<UserWorkoutProgramRowStatisticData>

    fun findStatisticByUserIdAndUserWorkoutProgramId(userId: String, userWorkoutProgramId: String): Mono<UserWorkoutProgramRowStatisticData>
}
