package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.WorkoutProgramScheduler
import reactor.core.publisher.Mono

interface WorkoutProgramSchedulerRepository {

    fun save(workoutProgramScheduler: WorkoutProgramScheduler): Mono<WorkoutProgramScheduler>

    fun findSchedulerById(id: String): Mono<WorkoutProgramScheduler>
    fun deleteById(workoutProgramSchedulerId: String): Mono<Long>

}
