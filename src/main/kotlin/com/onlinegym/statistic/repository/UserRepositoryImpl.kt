package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.User
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

class UserRepositoryImpl(private val template: ReactiveMongoTemplate) : UserRepository {

    override fun save(user: User): Mono<User> {
        return template.save(user)
    }

    override fun findUserById(userId: String): Mono<User> {
        return template.findById(userId, User::class.java)
    }

    override fun findAllUsers(): Flux<User> =
        template.findAll(User::class.java)


}
