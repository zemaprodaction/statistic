package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.Exercise
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono


class ExerciseRepositoryWebClient : ExerciseRepository {

    val trainingServiceHost = System.getenv("TRAINING-SERVICE_HOST") ?: "default_value"

    val trainingServicePort = System.getenv("TRAINING-SERVICE_PORT") ?: "default_value"

    override fun findExerciseById(exerciseId: String): Mono<Exercise> {
        val webClient = WebClient.create("${trainingServiceHost}:${trainingServicePort}/exercise")
        return webClient.get().uri("/$exerciseId")
            .accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(Exercise::class.java)

    }
}
