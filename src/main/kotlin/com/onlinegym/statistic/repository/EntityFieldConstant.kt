package com.onlinegym.statistic.repository

class EntityFieldConstant {
    companion object {
        const val ID = "id"
        const val FIELD_AMOUNT_WORKOUT = "amountAllWorkout"
        const val FIELD_EXERCISE_STATISTIC = "exerciseStatistics"
        const val FIELD_EXERCISE_STATISTIC_DOT_EXERCISE_ID = "exerciseStatistics.exerciseId"
        const val FIELD_EXERCISE_DATA_LIST = "exerciseDataList"
        const val FIELD_DATE = "date"
        const val FIELD_EXERCISES = "exercises"
        const val FIELD_EXERCISE_ID = "exerciseId"
        const val FIELD_USER_ID = "userId"
        const val FIELD_USER_WORKOUT_PROGRAM_STATUS = "status"

        const val FIELD_WORKOUT_STATISTIC = "workoutsStatistics"
        const val FIELD_WORKOUT_ID = "workoutId"
        const val FIELD_WORKOUT_DATA_LIST = "workoutDataList"
        const val FIELD_WORKOUTS_ON_PROGRAM = "workoutProgramStatistic.workoutsOnProgram"
        const val FINISHED_WORKOUT_INSTANCE = "finishedWorkoutInstance"

        const val FIELD_WORKOUT_PROGRAM_ID = "workoutProgramId"
        const val FIELD_USER_WORKOUT_PROGRAM_ID = "userWorkoutProgramId"
        const val USER_WORKOUT_PROGRAM_STATISTIC = "workoutProgramStatistic"
        const val USER_WORKOUT_PROGRAM_STATISTIC_ID = "workoutProgramStatistic.id"
        const val USER_WORKOUT_INSTANCE_ID = "userWorkoutInstanceId"
        const val TRAINING_WORKOUT_ID = "trainingWorkoutId"
    }
}
