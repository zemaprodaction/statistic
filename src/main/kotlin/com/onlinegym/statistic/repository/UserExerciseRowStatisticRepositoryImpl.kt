package com.onlinegym.statistic.repository

import com.onlinegym.statistic.dto.DateRange
import com.onlinegym.statistic.entity.UserExerciseRowStatisticData
import com.onlinegym.statistic.repository.EntityFieldConstant.Companion.FIELD_DATE
import com.onlinegym.statistic.repository.EntityFieldConstant.Companion.FIELD_EXERCISE_ID
import com.onlinegym.statistic.repository.EntityFieldConstant.Companion.FIELD_USER_ID
import com.onlinegym.statistic.repository.EntityFieldConstant.Companion.ID
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

class UserExerciseRowStatisticRepositoryImpl(private val template: ReactiveMongoTemplate) : UserExerciseRowStatisticRepository {

    override fun save(userExerciseRowStatisticData: UserExerciseRowStatisticData): Mono<UserExerciseRowStatisticData> =
        template.save(userExerciseRowStatisticData)

    override fun save(userExerciseRowStatisticDataList: List<UserExerciseRowStatisticData>): Flux<UserExerciseRowStatisticData> =
        template.insertAll(userExerciseRowStatisticDataList)

    override fun findByUserId(userId: String) =
        Query(Criteria(FIELD_USER_ID).`is`(userId)).let { query ->
            template.find(query, UserExerciseRowStatisticData::class.java)
        }

    override fun findAllExerciseDataStatisticsByUserAndDataRange(userId: String, dataRange: DateRange): Flux<UserExerciseRowStatisticData> =
        Query(Criteria(FIELD_USER_ID).`is`(userId).and(FIELD_DATE).gte(dataRange.getDate())).let {
            template.find(it, UserExerciseRowStatisticData::class.java)
        }

    override fun findExerciseDataById(id: String): Mono<UserExerciseRowStatisticData> = template.findById(id, UserExerciseRowStatisticData::class.java)

    override fun findExerciseDataByExerciseId(userId: String, exerciseId: String) =
        Query(Criteria(FIELD_USER_ID).`is`(userId).and(FIELD_EXERCISE_ID).`is`(exerciseId)).let { query ->
            template.find(query, UserExerciseRowStatisticData::class.java)
        }

    override fun findExerciseDataByRangeId(ids: List<String>): Flux<UserExerciseRowStatisticData> =
        Query(Criteria(ID).`in`(ids)).let {
            template.find(it, UserExerciseRowStatisticData::class.java)
        }


}
