package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.UserWorkoutInstance
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface UserWorkoutInstanceRepository {
    fun findAllWorkoutInstanceByUserIdAndUserWorkoutProgramId(userId: String, userWorkotProgramId: String): Flux<UserWorkoutInstance>

    fun findById(id: String): Mono<UserWorkoutInstance>

    fun findByTrainingWorkoutInstanceAndUserId(userId: String, userWorkoutInstanceId: String): Mono<UserWorkoutInstance>

    fun findByTrainingWorkoutIdAndUserId(userId: String, trainingWorkoutId: String): Flux<UserWorkoutInstance>

    fun save(userWorkoutInstance: UserWorkoutInstance): Mono<UserWorkoutInstance>

    fun saveAll(userWorkoutInstanceList: List<UserWorkoutInstance>): Flux<UserWorkoutInstance>

    fun findByRangeId(userWorkoutInstanceId: List<String>): Flux<UserWorkoutInstance>
}
