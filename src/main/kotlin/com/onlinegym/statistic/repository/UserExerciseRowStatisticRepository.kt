package com.onlinegym.statistic.repository

import com.onlinegym.statistic.dto.DateRange
import com.onlinegym.statistic.entity.UserExerciseRowStatisticData
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface UserExerciseRowStatisticRepository {

    fun save(userExerciseRowStatisticData: UserExerciseRowStatisticData): Mono<UserExerciseRowStatisticData>

    fun save(userExerciseRowStatisticDataList: List<UserExerciseRowStatisticData>): Flux<UserExerciseRowStatisticData>

    fun findByUserId(userId: String): Flux<UserExerciseRowStatisticData>

    fun findAllExerciseDataStatisticsByUserAndDataRange(userId: String, dataRange: DateRange): Flux<UserExerciseRowStatisticData>

    fun findExerciseDataById(id: String): Mono<UserExerciseRowStatisticData>

    fun findExerciseDataByRangeId(ids: List<String>): Flux<UserExerciseRowStatisticData>

    fun findExerciseDataByExerciseId(userId: String, exerciseId: String): Flux<UserExerciseRowStatisticData>
}
