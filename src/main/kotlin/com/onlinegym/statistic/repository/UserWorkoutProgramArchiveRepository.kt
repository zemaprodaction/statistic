package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.entity.UserWorkoutProgramArchive
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface UserWorkoutProgramArchiveRepository {

//    fun findUserWorkoutProgramArchiveById(workoutProgramArchiveId: String): Mono<UserWorkoutProgram>

    fun findAllUserWorkoutProgramArchiveByUserId(userId: String): Flux<UserWorkoutProgram>
}
