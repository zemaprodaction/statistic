package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.UserWorkoutInstanceRowStatisticData
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface UserWorkoutInstanceRowStatisticRepository {

    fun findByUserWorkoutInstanceIdList(userWorkoutInstanceIdList: List<String>): Flux<UserWorkoutInstanceRowStatisticData>

    fun findById(workoutDataId: String): Mono<UserWorkoutInstanceRowStatisticData>

    fun save(workoutInstanceStatisticData: UserWorkoutInstanceRowStatisticData): Mono<UserWorkoutInstanceRowStatisticData>
//
//    fun save(workoutDataList: List<WorkoutData>): Flux<WorkoutData>
//
//    fun findByUserId(userId: String): Flux<WorkoutData>
//
//
    fun findByListWorkoutInstance(workoutInstanceListId: List<String>): Flux<UserWorkoutInstanceRowStatisticData>
//
//    fun findByUserIdAndWorkoutProgramId(name: String, currentWorkoutProgramId: String): Flux<WorkoutData>
//
//    fun findTheMostPopularWorkoutTraining(userId: String): Flux<WorkoutRating>
//    fun getUserHistory(userID: String, skip: Long, take: Long): Flux<WorkoutData>
//
//    fun getUserHistory(userID: String): Flux<WorkoutData>
}
