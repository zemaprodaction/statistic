package com.onlinegym.statistic.repository

import com.onlinegym.statistic.dto.TrainingWorkoutProgramDto
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient

class TrainingWorkoutProgramRepositoryImpl : TrainingWorkoutProgramRepository {

    val trainingServiceHost = System.getenv("TRAINING-SERVICE_HOST") ?: "default_value"

    val trainingServicePort = System.getenv("TRAINING-SERVICE_PORT") ?: "default_value"

    private val webClient = WebClient.create("${trainingServiceHost}:${trainingServicePort}/workoutProgram")

    override fun getTrainingWorkoutProgramById(id: String) =
        webClient.get().uri("/$id")
            .accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(TrainingWorkoutProgramDto::class.java)

}
