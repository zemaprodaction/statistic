package com.onlinegym.statistic.repository

import com.onlinegym.statistic.dto.TrainingWorkoutProgramDto
import reactor.core.publisher.Mono

interface TrainingWorkoutProgramRepository {
    fun getTrainingWorkoutProgramById(id: String): Mono<TrainingWorkoutProgramDto>
}
