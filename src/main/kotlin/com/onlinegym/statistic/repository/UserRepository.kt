package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.User
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface UserRepository {

    fun save(user: User): Mono<User>

    fun findUserById(userId: String): Mono<User>
    fun findAllUsers(): Flux<User>

}
