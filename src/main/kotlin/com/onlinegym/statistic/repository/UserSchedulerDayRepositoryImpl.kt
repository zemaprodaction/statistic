package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.SchedulerDay
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

class UserSchedulerDayRepositoryImpl(private val template: ReactiveMongoTemplate) : UserSchedulerDayRepository {
    override fun save(userSchedulerDay: SchedulerDay): Mono<SchedulerDay> = template.save(userSchedulerDay)
    override fun saveAll(userSchedulerDay: List<SchedulerDay>): Flux<SchedulerDay> =
            template.insertAll(userSchedulerDay)


    override fun findSchedulerDayById(id: String): Mono<SchedulerDay> =
        template.findById(id, SchedulerDay::class.java)
}
