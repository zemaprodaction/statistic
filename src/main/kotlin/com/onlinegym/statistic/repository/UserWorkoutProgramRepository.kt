package com.onlinegym.statistic.repository

import com.mongodb.client.result.DeleteResult
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface UserWorkoutProgramRepository {

    fun findWorkoutProgramByUserAndTrainingWorkoutProgramId(userId: String, workoutProgramId: String): Mono<UserWorkoutProgram>
    fun findWorkoutProgramById(userWorkoutProgramId: String): Mono<UserWorkoutProgram>
    fun save(userWorkoutProgram: UserWorkoutProgram): Mono<UserWorkoutProgram>
    fun delete(userWorkoutProgram: UserWorkoutProgram): Mono<DeleteResult>
    fun findAllWorkoutProgramByUser(userName: String): Flux<UserWorkoutProgram>

}
