package com.onlinegym.statistic.repository

import com.onlinegym.statistic.entity.WorkoutProgramScheduler
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import reactor.core.publisher.Mono

class WorkoutProgramSchedulerRepositoryImpl(private val template: ReactiveMongoTemplate) : WorkoutProgramSchedulerRepository {

    override fun save(workoutProgramScheduler: WorkoutProgramScheduler): Mono<WorkoutProgramScheduler> =
        template.save(workoutProgramScheduler)

    override fun findSchedulerById(id: String): Mono<WorkoutProgramScheduler> =
        template.findById(id, WorkoutProgramScheduler::class.java)

    override fun deleteById(workoutProgramSchedulerId: String) =
        Query(Criteria(EntityFieldConstant.ID).`is`(workoutProgramSchedulerId)).let { query ->
            template.remove(query, WorkoutProgramScheduler::class.java).map { it.deletedCount }
        }
}
