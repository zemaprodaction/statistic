package com.onlinegym.statistic.routing

import com.onlinegym.statistic.handler.UserHandler
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class UserRoutes(
    private val userHandler: UserHandler,
) {

    fun router(): RouterFunction<ServerResponse> = router {
////        POST(PathVariableConstants.USER_UNSUBSCRIBE, userHandler::unsubscribe)
        GET(PathVariableConstants.USER_TRAINING_INFO, userHandler::getUserTrainingInfo)
        GET(PathVariableConstants.USER_TRAINING_INFO_BY_ID, userHandler::getUserTrainingInfoById)
    }
}
