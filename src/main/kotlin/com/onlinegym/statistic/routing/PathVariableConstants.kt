package com.onlinegym.statistic.routing

class PathVariableConstants {

    companion object {
        const val WORKOUT_INSTANCE_ID = "workoutInstanceId"
        const val WORKOUT_PROGRAM_ID = "workoutProgramId"
        const val USER_WORKOUT_INSTANCE_STATISTIC_ID = "userWorkoutInstanceStatisticId"
        const val EXERCISE_ID = "exerciseId"
        const val USER_ID = "userId"
        const val USER_WORKOUT_INSTANCE_ID = "userWorkoutInstanceId"
        const val COMMON_USER_STATISTIC = "common_user_statistic"
        const val CURRENT_WORKOUT_PROGRAM_STATISTIC = "current_workout_program_statistic"

        const val EXERCISE = "exercise"
        const val EXERCISE_GROUP_BY_TYPE = "${EXERCISE}/group_by_type"
        const val EXERCISE_RECORD = "${EXERCISE}/{${EXERCISE_ID}}/record"
        const val WORKOUT_PROGRAM = "workoutProgram"
        const val FINISHED_WORKOUT = "workout/finish"
        const val WORKOUT_PROGRAM_WITH_ID = "workoutProgram/{${WORKOUT_PROGRAM_ID}}"
        const val WORKOUT_PROGRAM_WITH_ID_AND_USER_ID = "workoutProgram/{${WORKOUT_PROGRAM_ID}}/{${USER_ID}}"
        const val WORKOUT = "workout"
        const val MOST_POPULAR_WORKOUT = "${WORKOUT}/most_popular"
        const val WORKOUT_HISTORY = "${WORKOUT}/history"

        const val TRAINER_USER_TRAINING_INFO = "trainer/user/userInfo/{userId}"
        const val USER_WORKOUT_PROGRAM_RESULT = "user/workout-program/result/active"

        const val USER_WORKOUT_PROGRAM_ARCHIVE = "user/workout-program/archive"
        const val USER_RESTART_WORKOUT_PROGRAM = "workout-program/restart"


        const val USER_WORKOUT_PROGRAM = "userWorkoutProgram"
        const val USER_WORKOUT_PROGRAM_ID = "userWorkoutProgramId"
        const val USER_WORKOUT_PROGRAM_WITH_ID = "userWorkoutProgramStatistic/{${USER_WORKOUT_PROGRAM_ID}}"
        const val USER_SUBSCRIBE_TO_WORKOUT_PROGRAM = "subscribe"
        const val USER_UNSUBSCRIBE_TO_WORKOUT_PROGRAM = "unsubscribe"

        const val USER_START_WORKOUT_PROGRAM = "userWorkoutProgram/start"

        const val USER_WORKOUT_PROGRAM_RESULT_FOR_ARCHIVE = "user-workout-program-archive/{userWorkoutProgramArchiveId}/result"

        const val USER_WORKOUT_INSTANCE = "user_workout_instance"
        const val GET_USER_WORKOUT_INSTANCE_ID = "user_workout_instance/{userWorkoutInstanceId}"
        const val USER_WORKOUT_INSTANCE_FINISH = "${USER_WORKOUT_INSTANCE}/finish"

        const val USER_TRAINING_INFO = "user/training-info"
        const val USER_TRAINING_INFO_BY_ID = "${USER_TRAINING_INFO}/{userId}"

    }

}
