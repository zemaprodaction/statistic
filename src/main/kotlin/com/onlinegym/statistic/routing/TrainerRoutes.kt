package com.onlinegym.statistic.routing

import com.onlinegym.statistic.handler.TrainerHandler
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class TrainerRoutes(
    private val trainerHandler: TrainerHandler,
) {

    fun router(): RouterFunction<ServerResponse> = router {
//        GET(PathVariableConstants.TRAINER_USER_TRAINING_INFO, trainerHandler::getUserTrainingInfo)
//        GET(PathVariableConstants.USER_WORKOUT_INSTANCE_BY_ID, trainerHandler::getUserWorkoutInstance)
//        PUT(PathVariableConstants.USER_WORKOUT_INSTANCE, trainerHandler::updateUserWorkoutInstance)
    }
}
