package com.onlinegym.statistic.routing

import com.onlinegym.statistic.handler.UserWorkoutInstanceHandler
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class UserWorkoutInstanceRoutes(
    private val handler: UserWorkoutInstanceHandler,
) {

    fun router(): RouterFunction<ServerResponse> = router {
        POST(PathVariableConstants.USER_WORKOUT_INSTANCE_FINISH, handler::finishWorkoutInstance)
        GET(PathVariableConstants.GET_USER_WORKOUT_INSTANCE_ID, handler::getWorkoutInstance)
        PUT(PathVariableConstants.USER_WORKOUT_INSTANCE, handler::updateWorkoutInstance)
    }
}
