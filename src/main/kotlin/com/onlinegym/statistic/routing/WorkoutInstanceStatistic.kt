package com.onlinegym.statistic.routing

import com.onlinegym.statistic.handler.WorkoutInstanceStatisticsHandler
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class WorkoutInstanceStatistic(
    private val workoutInstanceStatisticsHandler: WorkoutInstanceStatisticsHandler,
) {

    fun router(): RouterFunction<ServerResponse> = router {

////        GET(PathVariableConstants.MOST_POPULAR_WORKOUT, workoutStatisticsHandler::getTheMostPopularWorkoutsForUser)
////        GET(PathVariableConstants.WORKOUT_HISTORY, workoutStatisticsHandler::getUserHistory)
        GET("workout-instance/{${PathVariableConstants.USER_WORKOUT_INSTANCE_STATISTIC_ID}}/summary", workoutInstanceStatisticsHandler::getWorkoutInstanceFinishResult)
    }
}
