package com.onlinegym.statistic.routing

import com.onlinegym.statistic.handler.UserWorkoutProgramHandler
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class UserWorkoutProgramRoutes(
    private val userWorkoutProgramHandler: UserWorkoutProgramHandler,
) {

    fun router(): RouterFunction<ServerResponse> = router {
        POST(PathVariableConstants.USER_START_WORKOUT_PROGRAM, userWorkoutProgramHandler::userStartWorkoutProgram)
//        POST(PathVariableConstants.USER_WORKOUT_PROGRAM, userWorkoutProgramHandler::subscribeToWorkoutProgram)
//        GET(PathVariableConstants.USER_WORKOUT_PROGRAM_RESULT, userWorkoutProgramHandler::getUserActiveWorkoutProgramResult)
//        GET(PathVariableConstants.USER_WORKOUT_PROGRAM_RESULT_FOR_ARCHIVE, userWorkoutProgramHandler::getUserArchiveWorkoutProgramResult)
//        GET(PathVariableConstants.USER_WORKOUT_PROGRAM_ARCHIVE, userWorkoutProgramHandler::getAllFinishedWorkoutProgramForUser)
    }
}
