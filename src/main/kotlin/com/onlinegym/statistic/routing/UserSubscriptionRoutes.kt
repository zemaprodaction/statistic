package com.onlinegym.statistic.routing

import com.onlinegym.statistic.handler.UserSubscriptionHandler
import com.onlinegym.statistic.handler.UserWorkoutProgramHandler
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class UserSubscriptionRoutes(
    private val handler: UserSubscriptionHandler,
) {

    fun router(): RouterFunction<ServerResponse> = router {
        POST(PathVariableConstants.USER_SUBSCRIBE_TO_WORKOUT_PROGRAM, handler::subscribeToWorkoutProgram)
        POST(PathVariableConstants.USER_UNSUBSCRIBE_TO_WORKOUT_PROGRAM, handler::unsubscribe)
        POST(PathVariableConstants.USER_RESTART_WORKOUT_PROGRAM, handler::userRestartWorkoutProgram)
    }
}
