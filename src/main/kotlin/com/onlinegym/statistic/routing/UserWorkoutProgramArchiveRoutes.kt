package com.onlinegym.statistic.routing

import com.onlinegym.statistic.handler.UserWorkoutProgramArchiveHandler
import com.onlinegym.statistic.handler.UserWorkoutProgramHandler
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class UserWorkoutProgramArchiveRoutes(
    private val handler: UserWorkoutProgramArchiveHandler,
) {

    fun router(): RouterFunction<ServerResponse> = router {
//        GET(PathVariableConstants.USER_WORKOUT_PROGRAM_RESULT_FOR_ARCHIVE, handler::getUserArchiveWorkoutProgramResult)
//        GET(PathVariableConstants.USER_WORKOUT_PROGRAM_ARCHIVE, handler::getAllFinishedWorkoutProgramForUser)
    }
}
