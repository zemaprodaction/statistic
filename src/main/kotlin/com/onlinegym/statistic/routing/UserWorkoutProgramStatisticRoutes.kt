package com.onlinegym.statistic.routing

import com.onlinegym.statistic.handler.UserWorkoutProgramStatisticsHandler
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class UserWorkoutProgramStatisticRoutes(
    private val userWorkoutProgramStatisticsHandler: UserWorkoutProgramStatisticsHandler
) {

    fun router(): RouterFunction<ServerResponse> = router {

//        PUT(PathVariableConstants.WORKOUT_PROGRAM, workoutProgramStatisticsHandler::saveWorkoutProgramStatistics)
        GET(PathVariableConstants.USER_WORKOUT_PROGRAM_WITH_ID, userWorkoutProgramStatisticsHandler::getUserWorkoutProgramStatistic)
//        GET(PathVariableConstants.WORKOUT_PROGRAM_WITH_ID_AND_USER_ID, workoutProgramStatisticsHandler::getSpecificUserWorkoutProgramStatistic)
    }
}
