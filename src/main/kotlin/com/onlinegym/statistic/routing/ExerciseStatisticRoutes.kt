package com.onlinegym.statistic.routing

import com.onlinegym.statistic.handler.ExerciseStatisticsHandler
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class ExerciseStatisticRoutes(
    private val exerciseStatisticsHandler: ExerciseStatisticsHandler,
) {

    fun router(): RouterFunction<ServerResponse> = router {

//        GET(PathVariableConstants.EXERCISE_GROUP_BY_TYPE, exerciseStatisticsHandler::getExerciseCountGroupingByType)

        GET(PathVariableConstants.EXERCISE_RECORD, exerciseStatisticsHandler::getExerciseRecord)

    }
}
