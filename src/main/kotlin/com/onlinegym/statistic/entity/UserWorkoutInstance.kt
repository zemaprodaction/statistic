package com.onlinegym.statistic.entity

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class UserWorkoutInstance(
    @Id
    val id: String = ObjectId().toHexString(),
    val userWorkoutProgramId: String,
    val userId: String,
    val trainingWorkoutId: String,
    val listOfWorkoutPhases: List<WorkoutInstancePhase> = emptyList(),
    val status: UserWorkoutInstanceStatus = UserWorkoutInstanceStatus.NOT_STARTED,
    val percentComplete: Int = 0,
    val workoutDifficalt: Int? = null
)
