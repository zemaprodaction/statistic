package com.onlinegym.statistic.entity

enum class UserWorkoutInstanceStatus {
    NOT_STARTED, IN_PROGRESS, FINISHED

}
