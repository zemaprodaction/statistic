package com.onlinegym.statistic.entity

data class Measure(
    val measureTypeId: String,
    val actualValue: Double,
    val expectedValue: Double,
    val measurePrefix: String = ""
)

enum class MeasureMap {

    ACTUAL {
        override fun getValue(measure: Measure): Double = measure.actualValue
    },

    EXPECTED {
        override fun getValue(measure: Measure): Double = measure.expectedValue
    };

    abstract fun getValue(measure: Measure): Double
}
