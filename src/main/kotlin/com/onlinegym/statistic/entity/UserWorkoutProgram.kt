package com.onlinegym.statistic.entity

import com.onlinegym.statistic.dto.UserWorkoutProgramDto
import com.onlinegym.statistic.dto.WorkoutProgramSchedulerDto
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id

data class UserWorkoutProgram(
    @Id
    val id: String = ObjectId().toHexString(),
    val trainingWorkoutProgramId: String,
    val workoutProgramSchedulerId: String? = null,
    val userId: String,
    val status: UserWorkoutProgramStatus = UserWorkoutProgramStatus.NOT_STARTED,
)

fun UserWorkoutProgram.toDto(workoutProgramScheduler: WorkoutProgramSchedulerDto) = UserWorkoutProgramDto(
    id = this.id,
    trainingWorkoutProgramId = this.trainingWorkoutProgramId,
    workoutProgramScheduler = workoutProgramScheduler,
    status = this.status
)
fun UserWorkoutProgram.toDto() = UserWorkoutProgramDto(
    id = this.id,
    trainingWorkoutProgramId = this.trainingWorkoutProgramId,
    status = this.status
)

