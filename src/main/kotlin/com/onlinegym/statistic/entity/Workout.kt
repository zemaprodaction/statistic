package com.onlinegym.statistic.entity

import org.springframework.data.annotation.Id

data class WorkoutStatisticId(
    val workoutId: String = "",
    val userId: String,
)

data class WorkoutStatistic(
    @Id
    val reference: WorkoutStatisticId
)
