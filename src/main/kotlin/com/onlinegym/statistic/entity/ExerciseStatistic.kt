package com.onlinegym.statistic.entity

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id

data class ExerciseStatistics(
    @Id
    val id: String = ObjectId().toHexString(),
    val exerciseId: String = "",
    val userId: String,
    val userExerciseRowStatisticDataList: List<UserExerciseRowStatisticData> = ArrayList()
)
