package com.onlinegym.statistic.entity

import org.springframework.data.annotation.Id

data class User(
    @Id
    val id: String,
    val activeWorkoutProgramId: String? = null
)
