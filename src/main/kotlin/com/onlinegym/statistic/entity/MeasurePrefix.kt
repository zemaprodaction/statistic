package com.onlinegym.statistic.entity

enum class MeasurePrefix(val prefix: String) {
    METER("meter"), COUNT("times"), KILO("kg"), SEC("sec")
}
