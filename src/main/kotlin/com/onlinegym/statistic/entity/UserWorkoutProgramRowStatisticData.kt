package com.onlinegym.statistic.entity

import com.onlinegym.statistic.nowUTCTime
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import java.time.LocalDateTime

data class UserWorkoutProgramRowStatisticData(
    @Id
    val id: String = ObjectId().toHexString(),
    val userWorkoutProgramId: String = "",
    val startDate: LocalDateTime = nowUTCTime(),
    val endDate: LocalDateTime? = null,
    val userId: String
)
