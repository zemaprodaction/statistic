package com.onlinegym.statistic.entity

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import java.time.LocalDateTime

data class UserWorkoutProgramArchive(
    @Id
    val id: String = ObjectId().toHexString(),
    val schedulerUserId: String,
    val startDate: LocalDateTime,
    val endDate: LocalDateTime,
    val userId: String,
    val userWorkoutProgramId: String
)
