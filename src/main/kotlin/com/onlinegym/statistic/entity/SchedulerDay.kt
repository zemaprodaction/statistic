package com.onlinegym.statistic.entity

import com.onlinegym.statistic.dto.SchedulerDayDto
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import java.time.LocalDateTime

data class SchedulerDay(
    @Id
    val id: String = ObjectId().toHexString(),
    val userWorkoutInstanceList: List<String>,
    val date: LocalDateTime
)

fun SchedulerDay.toDto(userWorkoutInstanceList: List<UserWorkoutInstance>) = SchedulerDayDto(
    id = this.id,
    userWorkoutInstanceList = userWorkoutInstanceList,
    date = this.date
)
