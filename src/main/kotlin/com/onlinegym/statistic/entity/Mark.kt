package com.onlinegym.statistic.entity

enum class Mark {
    BAD, MIDDLE, PERFECT
}
