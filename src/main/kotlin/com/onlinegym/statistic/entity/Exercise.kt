package com.onlinegym.statistic.entity

data class Exercise(
    val id: String,
    val exerciseType: String
)
