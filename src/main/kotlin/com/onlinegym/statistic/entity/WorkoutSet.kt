package com.onlinegym.statistic.entity

data class WorkoutSet(
    val listOfMeasures: List<Measure> = emptyList(),
    val exerciseId: String = "",
    val restTime: Int = 60,
    val workoutSchedulerId: String = ""
)

