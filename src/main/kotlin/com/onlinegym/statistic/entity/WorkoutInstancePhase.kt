package com.onlinegym.statistic.entity

data class WorkoutInstancePhase(
    var name: String = "",
    val listOfSets: List<WorkoutSet> = ArrayList(),
    val isOptional: Boolean = false
)
