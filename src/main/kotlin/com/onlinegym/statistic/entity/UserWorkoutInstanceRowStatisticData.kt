package com.onlinegym.statistic.entity

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import java.time.LocalDateTime

data class UserWorkoutInstanceRowStatisticData(
    @Id
    val id: String = ObjectId().toHexString(),
    val userWorkoutInstanceId: String,
    val userId: String,
    val date: LocalDateTime = LocalDateTime.now(),
    val workoutInstanceResult: WorkoutInstanceResult,
    val workoutDifficalt: Int = 3,
)
