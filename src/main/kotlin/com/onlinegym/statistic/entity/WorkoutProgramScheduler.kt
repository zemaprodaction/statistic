package com.onlinegym.statistic.entity

import com.onlinegym.statistic.dto.SchedulerDayDto
import com.onlinegym.statistic.dto.WorkoutProgramSchedulerDto
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id

data class WorkoutProgramScheduler(
    @Id
    val id: String = ObjectId().toHexString(),
    val listOfDays: List<SchedulerDay>
)

fun WorkoutProgramScheduler.toDto(schedulerDayDtoList: List<SchedulerDayDto>) = WorkoutProgramSchedulerDto(
    id = this.id,
    listOfDays = schedulerDayDtoList,
)
