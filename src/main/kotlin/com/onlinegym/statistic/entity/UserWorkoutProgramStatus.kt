package com.onlinegym.statistic.entity

enum class UserWorkoutProgramStatus {
    NOT_STARTED,
    IN_PROGRESS,
    FINISHED,
    ARCHIVED
}
