package com.onlinegym.statistic.entity

import com.onlinegym.statistic.dto.ExerciseResultDto
import com.onlinegym.statistic.dto.WorkoutInstanceResultDto

data class WorkoutInstanceResult(
    val workoutInstanceId: String,
    val exercisesRowStatisticListId: List<String> = emptyList(),
    val totalWorkoutTime: Int,
    val totalWork: Int,
    val workoutDifficalt: Int = 3
)

fun WorkoutInstanceResult.toDto(exercisesResultDtoList: List<ExerciseResultDto>) = WorkoutInstanceResultDto(
    workoutInstanceId = this.workoutInstanceId,
    exercisesResultList = exercisesResultDtoList,
    totalWorkoutTime = this.totalWorkoutTime,
    workoutDifficalt = this.workoutDifficalt
)
