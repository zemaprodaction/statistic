package com.onlinegym.statistic

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

@DslMarker
annotation class StatisticsDsl

const val TIME_FORMAT = "HH:mm:ss.SSS"

fun Int.formatTime(): String {
    val formatter = DateTimeFormatter.ofPattern(TIME_FORMAT).withZone(ZoneId.of("UTC"))
    val instant = Instant.ofEpochMilli((this * 1000).toLong())
    return formatter.format(instant)
}

fun nowUTCTime(): LocalDateTime {
    return LocalDateTime.now(ZoneOffset.UTC).withNano(0)
}
