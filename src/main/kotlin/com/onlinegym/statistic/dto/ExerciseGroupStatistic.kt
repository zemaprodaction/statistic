package com.onlinegym.statistic.dto

data class ExerciseGroupStatistic(
    val groupingStatistic: Map<String, Int>
)
