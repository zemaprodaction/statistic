package com.onlinegym.statistic.dto

import java.time.LocalDateTime

//TODO maybe ненужно
data class WorkoutProgramStatisticDto(
    val workoutProgramId: String = "", // Не нужен можем взять из базы
    val workout: WorkoutDto,
    val amountAllWorkout: Int,
    val expectedFinishedDate: LocalDateTime
)

