package com.onlinegym.statistic.dto

data class SubscribtionDto(
    val trainingWorkoutProgramId: String
)
