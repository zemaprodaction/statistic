package com.onlinegym.statistic.dto

import com.onlinegym.statistic.entity.WorkoutInstanceResult

data class WorkoutInstanceResultDto(
    val workoutInstanceId: String,
    val exercisesResultList: List<ExerciseResultDto> = emptyList(),
    val totalWorkoutTime: Int,
    val workoutDifficalt: Int = 3,
)

fun WorkoutInstanceResultDto.toEntity(exercisesRowStatisticListId: List<String>) = WorkoutInstanceResult(
    workoutInstanceId = this.workoutInstanceId,
    exercisesRowStatisticListId = exercisesRowStatisticListId,
    totalWorkoutTime = this.totalWorkoutTime,
    totalWork = this.exercisesResultList.sumBy { it.totalWork.second.toInt() },
    workoutDifficalt = this.workoutDifficalt
)
