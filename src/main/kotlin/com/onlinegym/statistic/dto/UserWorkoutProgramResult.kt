package com.onlinegym.statistic.dto

import com.onlinegym.statistic.entity.Mark
import java.time.LocalDateTime

data class UserWorkoutProgramResult(
    val daysExpired: Long,
    val finishedDay: LocalDateTime?,
    val takesDays: Long,
    val workoutStatistic: UserWorkoutStatistic,
    val workoutProgramMark: Int,
    val averagePercentWorkoutComplete: Int = 0,
    val startDate: LocalDateTime? = null
)

data class UserWorkoutStatistic(
    val workoutFinishedAmount: Int,
    val workoutMarkMapToCount: Map<Mark, Int>
)



