package com.onlinegym.statistic.dto

data class UserWorkoutProgramCalculateStatisticData(
    val averagePercentComplete: Int,
    val percentDone: Int = 0,
)
