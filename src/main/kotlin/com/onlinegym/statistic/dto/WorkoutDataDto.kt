package com.onlinegym.statistic.dto

data class WorkoutDataDto(
    val exerciseResultDtos: List<ExerciseResultDto>,
    val workoutInstanceId: String,
    val totalWorkoutTime: Int,
    val workoutDataId: String?
)
