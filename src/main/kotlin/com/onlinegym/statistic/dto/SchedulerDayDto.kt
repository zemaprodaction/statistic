package com.onlinegym.statistic.dto

import com.onlinegym.statistic.entity.UserWorkoutInstance
import java.time.LocalDateTime

data class SchedulerDayDto (
    val id: String,
    val userWorkoutInstanceList: List<UserWorkoutInstance>,
    val date: LocalDateTime
)
