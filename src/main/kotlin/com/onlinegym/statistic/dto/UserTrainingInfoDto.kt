package com.onlinegym.statistic.dto

import com.onlinegym.statistic.entity.UserWorkoutProgram

data class UserTrainingInfoDto(
    val userId: String,
    val userWorkoutProgram: UserWorkoutProgramDto? = null
)
