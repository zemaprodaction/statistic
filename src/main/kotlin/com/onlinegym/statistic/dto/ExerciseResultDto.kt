package com.onlinegym.statistic.dto

import com.onlinegym.statistic.entity.Measure
import com.onlinegym.statistic.entity.MeasurePrefix
import kotlin.math.pow

data class ExerciseResultDto(
    val exerciseType: ExerciseType,
    val exerciseId: String = "",
    val restTime: Int = 0,
    val actualRestTime: Int = 0,
    val measures: List<Measure> = emptyList(),
){
    val totalWork: Pair<Double, Double>
        get() {
            when {
                measures.size == 1 -> {
                    return Pair(measures[0].expectedValue, measures[0].actualValue)
                }
                measures.size == 2 && measures.find { it.measurePrefix == MeasurePrefix.METER.prefix } != null
                    && measures.find { it.measurePrefix == MeasurePrefix.SEC.prefix } !== null -> {
                    val distance = measures.find { it.measurePrefix == MeasurePrefix.METER.prefix }!!
                    val time = measures.find { it.measurePrefix == MeasurePrefix.SEC.prefix }!!
                    return distance.expectedValue.pow(2.0) / time.expectedValue to distance.expectedValue.pow(2.0) / time.expectedValue
                }
                measures.size == 2-> {
                    return measures[0].expectedValue * measures[1].expectedValue to measures[0].actualValue * measures[1].actualValue
                }
            }
            return 0.0 to 0.0
        }
}

