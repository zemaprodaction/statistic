package com.onlinegym.statistic.dto

import com.onlinegym.statistic.entity.Measure
import com.onlinegym.statistic.entity.MeasurePrefix
import kotlin.math.pow

enum class ExerciseType {

    STRENGTH {
        override fun calculateWork(measures: List<Measure>, getValue: (Measure) -> Double): Int {
            val repetitions = measures.filter { it.measurePrefix == MeasurePrefix.COUNT.prefix }.sumByDouble { getValue(it) }
            val weight = measures.filter { it.measurePrefix == MeasurePrefix.KILO.prefix }.sumByDouble { getValue(it) }
            return (repetitions.takeIf { weight == 0.0 } ?: repetitions * weight).toInt()
        }
    },

    CARDIO {
        override fun calculateWork(measures: List<Measure>, getValue: (Measure) -> Double): Int {
            val distance = measures.filter { it.measurePrefix == MeasurePrefix.METER.prefix }.sumByDouble { getValue(it) }
            val time = measures.filter { it.measurePrefix == MeasurePrefix.SEC.prefix }.sumByDouble { getValue(it) }
            return when {
                time > 0 && distance == 0.0 -> time
                time.toInt() == 0 && distance > 0 -> distance
                else -> (distance.pow(2.0) / time)
            }.toInt()
        }
    },

    YOGA {
        // хз как рассчитать для йоги, да и нужно ли
        override fun calculateWork(measures: List<Measure>, getValue: (Measure) -> Double): Int {
            return 100
        }
    };

    abstract fun calculateWork(measures: List<Measure>, getValue: (Measure) -> Double): Int
}
