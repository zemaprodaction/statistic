package com.onlinegym.statistic.dto

import java.time.LocalDateTime

data class WorkoutProgramDto(
    val id: String,
    val startDate: LocalDateTime? = null,
    val endDate: LocalDateTime? = null
)
