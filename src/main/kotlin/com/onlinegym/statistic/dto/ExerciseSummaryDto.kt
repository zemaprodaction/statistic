package com.onlinegym.statistic.dto

import com.onlinegym.statistic.entity.UserExerciseRowStatisticData

data class ExerciseSummaryDto(
    val exerciseId: String,
    val summary: List<Map<String, Double>>,
    val percentOfCompletedWork: Int,
    val uniqueExerciseResultDtos: List<UserExerciseRowStatisticData> = emptyList()
)
