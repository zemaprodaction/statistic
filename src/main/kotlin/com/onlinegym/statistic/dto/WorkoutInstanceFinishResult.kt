package com.onlinegym.statistic.dto

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import java.time.LocalDateTime

data class WorkoutInstanceFinishResult
    (
    val userWorkoutInstanceRowStatisticId: String,
    val restTime: RestTimeDto = RestTimeDto(),
    val percentOfCompletedWork: Int = 0,
    val exerciseSummary: List<ExerciseSummaryDto> = emptyList(),
    val totalWork: Int = 0,
    val totalWorkoutTime: Int = 0,
    val workoutProgress: List<Int> = emptyList(),
    val date: LocalDateTime = LocalDateTime.now(),
    val trainingWorkoutInstanceId: String = "",
    val exerciseCount: Int = 0,
)

