package com.onlinegym.statistic.dto

import com.onlinegym.statistic.entity.UserWorkoutProgramStatus

data class UserWorkoutProgramDto(
    val id: String,
    val trainingWorkoutProgramId: String,
    val workoutProgramScheduler: WorkoutProgramSchedulerDto? = null,
    val status: UserWorkoutProgramStatus = UserWorkoutProgramStatus.NOT_STARTED,
)
