package com.onlinegym.statistic.dto

import com.onlinegym.statistic.entity.WorkoutInstancePhase
import java.time.LocalDateTime

data class TrainingWorkoutProgramDto(
    val id: String,
    val listOfWeeks: List<WorkoutWeekDto> = ArrayList(),
)

data class WorkoutWeekDto(
    val listOfDays: List<WorkoutDayDto> = emptyList()
)

data class WorkoutDayDto(
    val dayIndex: Int = -1,
    val listOfScheduledWorkouts: List<ScheduledWorkout> = emptyList()
)


data class ScheduledWorkout(
    val id: String,
    var name: String = "",
    val workoutId: String = "",
    val workoutProgramId: String = "",
    var isFinished: Boolean = false,
    var actualDate: LocalDateTime? = null,
    val workoutDataId: String? = null,
    val percentComplete: Int = 0,
    val userWorkoutInstanceId: String = "",
    val listOfWorkoutPhases: List<WorkoutInstancePhase>
)
