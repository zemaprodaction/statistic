package com.onlinegym.statistic.dto

import org.springframework.data.annotation.Id

data class WorkoutRating(
    @Id
    val workoutId: String,
    val rating: Int
)
