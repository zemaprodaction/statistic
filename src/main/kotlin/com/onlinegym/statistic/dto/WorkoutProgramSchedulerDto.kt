package com.onlinegym.statistic.dto

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id

data class WorkoutProgramSchedulerDto(
    @Id
    val id: String = ObjectId().toHexString(),
    val listOfDays: List<SchedulerDayDto>
)

