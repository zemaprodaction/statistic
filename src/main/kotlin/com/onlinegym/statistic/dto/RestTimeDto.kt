package com.onlinegym.statistic.dto

data class RestTimeDto(
    val actualRestTime: Int = 0,
    val planedRestTime: Int = 0,
)
