package com.onlinegym.statistic.dto

data class WorkoutDto(
    val id: String? = null,
    val workoutId: String,
    val workoutData: WorkoutDataDto
)
