package com.onlinegym.statistic.dto

import java.time.LocalDateTime

enum class DateRange {
    MONTH {
        override fun getDate(): LocalDateTime {
            return LocalDateTime.now().minusMonths(1)
        }
    },
    ALL_TIME {
        override fun getDate(): LocalDateTime {
            return LocalDateTime.of(2000, 2, 16, 0, 0, 0)
        }
    },
    WEEK {
        override fun getDate(): LocalDateTime {
            return LocalDateTime.now().minusDays(7)
        }
    },
    YEAR {
        override fun getDate(): LocalDateTime {
            return LocalDateTime.now().minusYears(1)
        }
    };

    abstract fun getDate(): LocalDateTime
}
