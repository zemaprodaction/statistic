package com.onlinegym.statistic.configuration

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.convert.MappingMongoConverter
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.web.server.SecurityWebFilterChain

@Configuration
class SecurityConfiguration {

    @Bean
    fun securityWebFilterChain(
        http: ServerHttpSecurity): SecurityWebFilterChain? {
        http.apply {
            oauth2ResourceServer().jwt()
            authorizeExchange().anyExchange().authenticated()
            csrf().disable()
        }
        return http.build()
    }

    @Autowired
    fun setMapKeyDotReplacement(mongoConverter: MappingMongoConverter) {
        mongoConverter.setMapKeyDotReplacement("#")
    }
}
