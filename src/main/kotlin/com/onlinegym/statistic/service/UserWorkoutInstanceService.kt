package com.onlinegym.statistic.service

import com.onlinegym.statistic.dto.WorkoutInstanceFinishResult
import com.onlinegym.statistic.dto.WorkoutInstanceResultDto
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutInstance
import reactor.core.publisher.Mono

interface UserWorkoutInstanceService {

    fun getAllWorkoutInstanceForUserWorkoutProgram(userId: String, workoutProgramId: String): Mono<List<UserWorkoutInstance>>

    fun getAllUserWorkoutInstanceForTrainingWorkout(userId: String, trainingWorkoutId: String): Mono<List<UserWorkoutInstance>>

    fun getWorkoutInstance(userWorkoutInstanceId: String): Mono<UserWorkoutInstance>
    fun updateWorkoutInstance(userWorkoutInstance: UserWorkoutInstance): Mono<UserWorkoutInstance>

    fun getWorkoutInstanceList(userWorkoutInstanceId: List<String>): Mono<List<UserWorkoutInstance>>

    fun finishWorkoutInstance(user: User, workoutInstanceResultDto: WorkoutInstanceResultDto): Mono<WorkoutInstanceFinishResult>
//
//    fun updateWorkoutInstance(userWorkoutInstance: Mono<UserWorkoutInstance>): Mono<UserWorkoutInstance>
//
//    fun addNewWorkoutInstance(userId: String, workoutProgramId: String, scheduledDayDto: ScheduledWorkout): Mono<UserWorkoutInstance>
    fun addNewWorkoutInstance(userWorkoutInstance: UserWorkoutInstance): Mono<UserWorkoutInstance>
//
//    fun addNewListWorkoutInstance(userId: String, userWorkoutProgramId: String, scheduledDayDto: List<ScheduledWorkout>):
//        Mono<List<UserWorkoutInstance>>

    fun addNewListWorkoutInstance(userWorkoutInstance: List<UserWorkoutInstance>):
        Mono<List<UserWorkoutInstance>>

}
