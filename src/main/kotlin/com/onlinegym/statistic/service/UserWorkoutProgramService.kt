package com.onlinegym.statistic.service

import com.mongodb.client.result.DeleteResult
import com.onlinegym.statistic.dto.UserWorkoutProgramResult
import com.onlinegym.statistic.dto.WorkoutProgramSchedulerDto
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.entity.UserWorkoutProgramArchive
import reactor.core.publisher.Mono
import java.security.Principal

interface UserWorkoutProgramService {

    fun save(userWorkoutProgram: UserWorkoutProgram): Mono<UserWorkoutProgram>

    fun delete(userWorkoutProgram: UserWorkoutProgram): Mono<DeleteResult>

    fun getActiveWorkoutProgramForUser(user: User): Mono<UserWorkoutProgram>

    fun getUserWorkoutProgramByTrainingWorkoutProgramIdAndUser(userId: String, workoutProgramId: String): Mono<UserWorkoutProgram>

    fun getUserWorkoutProgramById(userWorkoutProgramId: String): Mono<UserWorkoutProgram>

    fun deleteSchedulerFromUserWorkoutProgram(userWorkoutProgram: UserWorkoutProgram): Mono<UserWorkoutProgram>

//    fun getUserWorkoutProgramResult(user: User): Mono<UserWorkoutProgramResult>

//    fun getUserArchiveWorkoutProgramResult(user: User, userWorkoutProgramArchiveId: String): Mono<UserWorkoutProgramResult>

//    fun getAllFinishedWorkoutProgramByUser(user: User): Mono<List<UserWorkoutProgramArchive>>
    fun userStartWorkoutProgram(user: User): Mono<WorkoutProgramSchedulerDto>
}
