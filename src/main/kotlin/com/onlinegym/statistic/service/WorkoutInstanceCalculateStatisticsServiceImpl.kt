package com.onlinegym.statistic.service

import com.onlinegym.statistic.dto.WorkoutInstanceResultDto
import reactor.core.publisher.Mono
import java.security.Principal

class WorkoutInstanceCalculateStatisticsServiceImpl(private val userWorkoutInstanceRowService: UserWorkoutInstanceRowStatisticService,
    private val userExerciseRowService: UserExerciseRowStatisticsService) : WorkoutInstanceCalculateStatisticsService {

//    override fun calculateWorkoutSummaryWithId(userId: String, workoutDto: WorkoutDto, workoutId: String, id: String) =
//        workoutDataService.findByUserIdAndWorkoutId(userId, workoutId).collectList().map {
//            calculateSummary(workoutDto) {
//                calculatePlannedRestTimeFormat()
//                calculateActualRestTimeFormat()
//                calculatePlannedRestTime()
//                calculateActualRestTime()
//                calculatePercentOfCompletedWork()
//                calculateActualTotalWork()
//                addExerciseSummary()
//                addWorkoutProgressFrom(it)
//                addWorkoutTotalTime()
//                addId(id)
//                countAllExerciseInWorkout()
//                addWorkoutInstanceId()
//            }
//        }
//
//    override fun  calculateWorkoutSummaryWithoutId(userId: String, workoutDto: WorkoutDto, workoutId: String): Mono<WorkoutSummaryDto> =
//        workoutDataService.findByUserIdAndWorkoutId(userId, workoutId).collectList().map {
//            calculateSummary(workoutDto) {
//                calculatePlannedRestTimeFormat()
//                calculateActualRestTimeFormat()
//                calculatePlannedRestTime()
//                calculateActualRestTime()
//                calculatePercentOfCompletedWork()
//                calculateActualTotalWork()
//                addExerciseSummary()
//                addWorkoutProgressFrom(it)
//                addWorkoutTotalTime()
//                countAllExerciseInWorkout()
//                addWorkoutInstanceId()
//            }
//        }

//    override fun getWorkoutDtoByWorkoutDataId(workoutDataId: String): Mono<WorkoutDto> =
//        workoutDataService.getWorkoutDataById(workoutDataId).flatMap {
//            userExerciseRowService.findExerciseByRangeId(it.exerciseIdList).collectList().map { exerciseDataStatisticList ->
//                val workoutDataDto =
//                    WorkoutDataDto(exercises = exerciseDataStatisticList.map { ex -> ex.toExerciseDto() },
//                        it.workoutInstanceId, it.totalWorkoutTime, it.id)
//                WorkoutDto(it.id, it.workoutId, workoutDataDto)
//            }
//        }

//    override fun getWorkoutInstanceFinishResult(userId: Mono<out Principal>, workoutInstanceId: String): Mono<WorkoutInstanceResultDto> =
//        userId.flatMap {
//            getWorkoutDtoByWorkoutDataId(workoutInstanceId).flatMap { workoutDto ->
//                calculateWorkoutSummaryWithId(it.name, workoutDto, workoutDto.workoutId, workoutDto.id ?: "")
//            }
//        }

}
