package com.onlinegym.statistic.service

import com.onlinegym.statistic.entity.UserWorkoutProgramRowStatisticData
import com.onlinegym.statistic.repository.UserWorkoutProgramRowStatisticRepository

class UserWorkoutProgramRowStatisticServiceImpl(private val repository: UserWorkoutProgramRowStatisticRepository) : UserWorkoutProgramRowStatisticService {

    override fun save(rowStatisticData: UserWorkoutProgramRowStatisticData) =
        repository.save(rowStatisticData)


    override fun getStatisticForWorkoutProgramAndUser(userId: String, userWorkoutProgramId: String) =
        repository.findStatisticByUserIdAndUserWorkoutProgramId(userId, userWorkoutProgramId)

}
