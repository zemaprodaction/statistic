package com.onlinegym.statistic.service

import com.mongodb.client.result.DeleteResult
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.entity.UserWorkoutProgramStatus
import com.onlinegym.statistic.entity.toDto
import com.onlinegym.statistic.repository.UserWorkoutProgramRepository
import com.onlinegym.statistic.service.user.WorkoutProgramSchedulerService
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

class UserWorkoutProgramServiceImpl(
    private val schedulerService: WorkoutProgramSchedulerService,
    private val userWorkoutInstanceService: UserWorkoutInstanceService,
    private val userWorkoutProgramRepository: UserWorkoutProgramRepository) : UserWorkoutProgramService {

    override fun save(userWorkoutProgram: UserWorkoutProgram) = userWorkoutProgramRepository.save(userWorkoutProgram)

    override fun delete(userWorkoutProgram: UserWorkoutProgram): Mono<DeleteResult> = userWorkoutProgramRepository.delete(userWorkoutProgram)


    override fun userStartWorkoutProgram(user: User) =
        if (user.activeWorkoutProgramId == null) {
            error(Exception("User does not subscribe to any workout program"))
        } else {
            getActiveWorkoutProgramForUser(user).flatMap { userWp ->
                schedulerService.generateSchedulerWorkoutProgram(userWp).flatMap { scheduler ->
                    save(userWp.copy(status = UserWorkoutProgramStatus.IN_PROGRESS,
                        workoutProgramSchedulerId = scheduler.id)).map {
                        scheduler
                    }
                }
            }
        }


    override fun getActiveWorkoutProgramForUser(user: User): Mono<UserWorkoutProgram> {
        return if (user.activeWorkoutProgramId == null) {
            Mono.empty()
        } else {
            userWorkoutProgramRepository.findWorkoutProgramById(user.activeWorkoutProgramId)
        }
    }

    override fun getUserWorkoutProgramById(userWorkoutProgramId: String): Mono<UserWorkoutProgram> =
        userWorkoutProgramRepository.findWorkoutProgramById(userWorkoutProgramId)

    override fun deleteSchedulerFromUserWorkoutProgram(userWorkoutProgram: UserWorkoutProgram) =
        schedulerService.removeScheduler(userWorkoutProgram.workoutProgramSchedulerId ?: throw Exception("User does not start workout program"))
            .flatMap {
                userWorkoutProgramRepository.save(userWorkoutProgram.copy(workoutProgramSchedulerId = null))
            }


    override fun getUserWorkoutProgramByTrainingWorkoutProgramIdAndUser(userId: String, workoutProgramId: String) =
        userWorkoutProgramRepository.findWorkoutProgramByUserAndTrainingWorkoutProgramId(userId, workoutProgramId)

//    override fun getUserWorkoutProgramResult(user: User) = getActiveWorkoutProgramForUser(user).flatMap {
//        calculateStatisticForuserWorkoutProgram(it.schedulerUser ?: UserWorkoutProgramScheduler(emptyMap()))
//    }

//    private fun calculateStatisticForuserWorkoutProgram(schedulerUser: UserWorkoutProgramScheduler) =
//        userWorkoutProgramStatisticService.getMapOfMarkOfWorkoutToNumberWorkout(schedulerUser)
//            .zipWith(userWorkoutProgramStatisticService.getWorkoutProgramMark(schedulerUser))
//            .map { markMap ->
//                val expiredDays = userWorkoutProgramStatisticService.calculateExpiredDay(schedulerUser)
//                val finishedDay = userWorkoutProgramStatisticService.getActualFinishedDateWorkoutProgram(schedulerUser)
//                val takeDays = userWorkoutProgramStatisticService.getHowManyDaysWorkoutProgramTakes(schedulerUser)
//                val amountOfFinishedWorkouts = userWorkoutProgramStatisticService.getAmountOfFinishedWorkoutProgram(schedulerUser)
//                val workoutStatistic = UserWorkoutStatistic(workoutFinishedAmount = amountOfFinishedWorkouts, markMap.t1)
//                val startDate = userWorkoutProgramStatisticService.getFirstActualWorkoutDay(schedulerUser)
//                UserWorkoutProgramResult(daysExpired = expiredDays, finishedDay = finishedDay,
//                    takesDays = takeDays, workoutStatistic = workoutStatistic, workoutProgramMark = markMap.t2.toInt(), startDate = startDate)
//            }.flatMap { result ->
//                userWorkoutProgramStatisticService.getAveragePercentCompleteOfWorkout(schedulerUser).map { averagePercent ->
//                    result.copy(averagePercentWorkoutComplete = averagePercent.toInt())
//                }
//
//            }

    //    override fun getAllFinishedWorkoutProgramByUser(user: User): Mono<List<UserWorkoutProgramArchive>> {
//        return userWorkoutProgramArchiveService.getAllWorkoutProgramArchiveForUser(user).collectList()

//    }

}

