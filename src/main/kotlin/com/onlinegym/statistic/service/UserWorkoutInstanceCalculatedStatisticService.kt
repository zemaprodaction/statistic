package com.onlinegym.statistic.service

import com.onlinegym.statistic.dto.WorkoutInstanceFinishResult
import com.onlinegym.statistic.entity.UserWorkoutInstanceRowStatisticData
import reactor.core.publisher.Mono

interface UserWorkoutInstanceCalculatedStatisticService {

    fun getWorkoutInstanceResultByWorkoutInstanceResult(result: UserWorkoutInstanceRowStatisticData,
        userWorkoutInstanceRowStatisticDataList: List<UserWorkoutInstanceRowStatisticData>): Mono<WorkoutInstanceFinishResult>
}
