package com.onlinegym.statistic.service

import com.onlinegym.statistic.dto.UserWorkoutProgramCalculateStatisticData
import com.onlinegym.statistic.entity.UserWorkoutInstanceStatus
import com.onlinegym.statistic.repository.UserWorkoutProgramRowStatisticRepository
import com.onlinegym.statistic.service.user.UserService
import reactor.core.publisher.Mono

class UserWorkoutProgramStatisticServiceImpl(
    private val userService: UserService,
    private val userWorkoutProgramService: UserWorkoutProgramService,
    private val userWorkoutInstanceService: UserWorkoutInstanceService) :
    UserWorkoutProgramStatisticService {
    //
//    override fun getActualFinishedDateWorkoutProgram(schedulerUser: UserWorkoutProgramScheduler): LocalDateTime? {
//        val dates = schedulerUser.calendar.values.flatMap { it.scheduledWorkoutList }.filter {
//            it.actualDate !== null
//        }
//        return if (dates.isEmpty()) {
//            null
//        } else {
//            dates.maxOf { it.actualDate!! }
//        }
//    }
//
//
//    override fun getExpectFinishedDateForWorkoutProgram(schedulerUser: UserWorkoutProgramScheduler): LocalDateTime {
//        return schedulerUser.calendar.keys.maxOf { it }
//    }
//
//    override fun calculateExpiredDay(schedulerUser: UserWorkoutProgramScheduler): Long {
//        val expectedFinishDate = getExpectFinishedDateForWorkoutProgram(schedulerUser)
//        val actualFinishedDate = getActualFinishedDateWorkoutProgram(schedulerUser)
//        return if (actualFinishedDate === null) {
//            -1L
//        } else {
//            Duration.between(expectedFinishDate, actualFinishedDate).toDays()
//        }
//    }
//
//    override fun getFirstActualWorkoutDay(schedulerUser: UserWorkoutProgramScheduler): LocalDateTime? {
//        return schedulerUser.calendar.values
//            .flatMap { it.scheduledWorkoutList.map { scheduledWorkout -> scheduledWorkout.actualDate } }
//            .filterNotNull()
//            .minOfOrNull {
//                it
//            }
//    }
//
//    override fun getHowManyDaysWorkoutProgramTakes(schedulerUser: UserWorkoutProgramScheduler): Long {
//        val firstDay = getFirstActualWorkoutDay(schedulerUser)
//        val lastDay = getActualFinishedDateWorkoutProgram(schedulerUser)
//        return if (lastDay === null || firstDay === null) {
//            -1L
//        } else {
//            Duration.between(firstDay, lastDay).toDays()
//        }
//    }
//
//    override fun getMapOfMarkOfWorkoutToNumberWorkout(schedulerUser: UserWorkoutProgramScheduler): Mono<Map<Mark, Int>> {
//        return Flux.concat(schedulerUser.calendar.values.flatMap { it.scheduledWorkoutList }.filter { it.isFinished }
//            .map { getMarkForWorkout(it) }).collectList().map {
//            it.groupingBy { it }.eachCount()
//        }
//    }
//
//    override fun getAmountOfFinishedWorkoutProgram(schedulerUser: UserWorkoutProgramScheduler): Int {
//        return schedulerUser.calendar.values.flatMap { it.scheduledWorkoutList }.filter { it.isFinished }.size
//    }
//
//    override fun getAveragePercentCompleteOfWorkout(schedulerUser: UserWorkoutProgramScheduler): Mono<Double> {
//        return Flux.concat(schedulerUser.calendar.values.flatMap { it.scheduledWorkoutList }.map {
//            if (it.workoutDataId == null) {
//                return Mono.just(0.0)
//            }
//            workoutDataStatisticService.getWorkoutDataById(it.workoutDataId).map { wd -> wd?.percentOfCompletedWork ?: 0 }
//        }).collectList().map {
//            it.average()
//        }
//    }
//
//    override fun getWorkoutProgramMark(schedulerUser: UserWorkoutProgramScheduler): Mono<Double> =
//        getAveragePercentCompleteOfWorkout(schedulerUser).map {
//            val expiredDay = calculateExpiredDay(schedulerUser)
//            val mark = (it - expiredDay * 20)
//            if (mark < 0) {
//                0.0
//            } else {
//                mark
//            }
//        }
//
//    private fun getMarkForWorkout(ScheduledWorkout: ScheduledWorkout): Mono<Mark> {
//        return workoutDataStatisticService.getWorkoutDataById(ScheduledWorkout.workoutDataId!!).map {
//            when {
//                it.percentOfCompletedWork > 85 -> {
//                    Mark.PERFECT
//                }
//                it.percentOfCompletedWork > 70 -> {
//                    Mark.MIDDLE
//                }
//                else -> {
//                    Mark.BAD
//                }
//            }
//        }
//    }
    override fun getUserWorkoutProgramCalculateStatistic(userId: String, userWorkoutProgramId: String) =
        userService.findById(userId).flatMap {
            userWorkoutProgramService.getActiveWorkoutProgramForUser(it).flatMap {_ ->
                userWorkoutInstanceService.getAllWorkoutInstanceForUserWorkoutProgram(userId, userWorkoutProgramId).map { userWorkoutInstanceList ->
                    val averagePercentComplete = userWorkoutInstanceList.map { wi -> wi.percentComplete }.average().toInt()
                    val finishedWorkoutInstance = userWorkoutInstanceList.filter { it.status == UserWorkoutInstanceStatus.FINISHED }.size
                    val percent = (finishedWorkoutInstance * 100) / userWorkoutInstanceList.size
                    UserWorkoutProgramCalculateStatisticData(averagePercentComplete = averagePercentComplete, percentDone = percent)
                }
            }
        }
}
