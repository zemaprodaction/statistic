package com.onlinegym.statistic.service

import com.onlinegym.statistic.entity.UserWorkoutInstanceRowStatisticData
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface UserWorkoutInstanceRowStatisticService {
    fun save(userWorkoutInstance: UserWorkoutInstanceRowStatisticData): Mono<UserWorkoutInstanceRowStatisticData>
//
//    fun save(workoutDataList: List<WorkoutData>): Flux<WorkoutData>
//
    fun getStatisticForWorkout(userId: String, trainingWorkoutId: String): Mono<List<UserWorkoutInstanceRowStatisticData>>
    fun getUserWorkoutInstanceRowStatistic(id: String): Mono<UserWorkoutInstanceRowStatisticData>
//
//    fun findByUserId(principal: Mono<out Principal>): Flux<WorkoutData>
//
//    fun findByUserIdAndWorkoutId(userId: String, workoutId: String): Flux<WorkoutData>
//
//    fun findByUserIdAndWorkoutProgramId(name: String, currentWorkoutProgramId: String): Flux<WorkoutData>
//
//    fun getThe5MostPopularWorkoutForUser(principal: Mono<out Principal>): Flux<WorkoutRating>
//
//    fun getUserHistory(principal: Mono<out Principal>, skip: Long, take: Long): Flux<WorkoutData>
//
//    fun getWorkoutDataById(workoutDataId: String): Mono<WorkoutData>

}
