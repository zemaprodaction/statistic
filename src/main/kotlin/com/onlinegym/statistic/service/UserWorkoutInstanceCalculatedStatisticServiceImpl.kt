package com.onlinegym.statistic.service

import com.onlinegym.statistic.StatisticsDsl
import com.onlinegym.statistic.dto.*
import com.onlinegym.statistic.entity.UserExerciseRowStatisticData
import com.onlinegym.statistic.entity.UserWorkoutInstanceRowStatisticData
import com.onlinegym.statistic.entity.WorkoutInstanceResult
import reactor.core.publisher.Mono

class UserWorkoutInstanceCalculatedStatisticServiceImpl(private val exerciseStatisticService: UserExerciseRowStatisticsService)
    : UserWorkoutInstanceCalculatedStatisticService {

    override fun getWorkoutInstanceResultByWorkoutInstanceResult(result: UserWorkoutInstanceRowStatisticData,
        userWorkoutInstanceRowStatisticDataList: List<UserWorkoutInstanceRowStatisticData>): Mono<WorkoutInstanceFinishResult> =
        calculateSummary(result) {
            calculatePlannedRestTime()
            calculateActualRestTime()
            calculateActualTotalWork()
            calculatePercentOfCompletedWork()
            addExerciseSummary()
            addWorkoutTotalTime()
            countAllExerciseInWorkout()
            addWorkoutInstanceId()
            addWorkoutProgressFrom(userWorkoutInstanceRowStatisticDataList)
        }

    private fun calculateSummary(userWorkoutInstanceRowStatisticData: UserWorkoutInstanceRowStatisticData, calculator: SummaryCalculator.() -> Unit): Mono<WorkoutInstanceFinishResult> =
        exerciseStatisticService.getExerciseByRangeId(userWorkoutInstanceRowStatisticData.workoutInstanceResult.exercisesRowStatisticListId).map {
            val calculatorNew = SummaryCalculator(userWorkoutInstanceRowStatisticData, it)
            calculatorNew.calculator()
            calculatorNew.calculate()
        }
}

@StatisticsDsl
class SummaryCalculator(private val userWorkoutInstanceRowStatisticData: UserWorkoutInstanceRowStatisticData,
    private val exerciseRowStatisticData: List<UserExerciseRowStatisticData>) {

    private var calculatedData: WorkoutInstanceFinishResult = WorkoutInstanceFinishResult(userWorkoutInstanceRowStatisticData.id)

    private val actualWorkCalculator: List<UserExerciseRowStatisticData>.() -> Int = {
        map { exercise -> exercise.totalWork.second.toInt() }.reduceRight { ex1, ex2 -> ex1 + ex2 }
    }

    private val expectedWorkCalculator: List<UserExerciseRowStatisticData>.() -> Int = {
        map { exercise -> exercise.totalWork.first.toInt() }.reduceRight { ex1, ex2 -> ex1 + ex2 }
    }

    fun calculatePlannedRestTime() = apply {
        val plannedRestTime = exerciseRowStatisticData.sumBy { it.restTime }
        calculatedData = calculatedData.copy(restTime = calculatedData.restTime.copy(planedRestTime = plannedRestTime))
    }

    fun calculateActualRestTime() = apply {
        val actualRestTime = exerciseRowStatisticData.sumBy { it.actualRestTime }
        calculatedData = calculatedData.copy(restTime = calculatedData.restTime.copy(actualRestTime = actualRestTime))
    }

    fun calculatePercentOfCompletedWork() = apply {
        calculatedData = calculatedData.copy(percentOfCompletedWork = runCatching {
            val summaryForExercises = exerciseRowStatisticData.getSummary()
            val actualPercentOfCompletion = summaryForExercises.sumOf { it.percentOfCompletedWork }
            val expectedPercentOfCompletion = summaryForExercises.map { it.uniqueExerciseResultDtos }.size * 100
            actualPercentOfCompletion * 100 / expectedPercentOfCompletion
        }.getOrElse { 0 }.toInt())
    }

    fun calculateActualTotalWork() = apply {
        calculatedData = calculatedData.copy(totalWork = exerciseRowStatisticData.actualWorkCalculator())
    }


    fun addExerciseSummary() = apply {
        calculatedData = calculatedData.copy(exerciseSummary = exerciseRowStatisticData.getSummary())
    }

    fun addWorkoutTotalTime() = apply {
        calculatedData = calculatedData.copy(totalWorkoutTime = userWorkoutInstanceRowStatisticData.workoutInstanceResult.totalWorkoutTime)
    }

    fun countAllExerciseInWorkout() = apply {
        calculatedData = calculatedData.copy(exerciseCount = exerciseRowStatisticData.size)
    }

    fun addWorkoutInstanceId() = apply {
        calculatedData = calculatedData.copy(trainingWorkoutInstanceId = userWorkoutInstanceRowStatisticData
            .workoutInstanceResult.workoutInstanceId)
    }

    fun addWorkoutProgressFrom(workoutInstanceResultDtoList: List<UserWorkoutInstanceRowStatisticData>) = apply {
        calculatedData = calculatedData.copy(workoutProgress = workoutInstanceResultDtoList.map { it.workoutInstanceResult.totalWork })
    }


    fun calculate() = calculatedData

    private fun List<UserExerciseRowStatisticData>.getSummary(): List<ExerciseSummaryDto> {
        return groupBy { it.exerciseId }.values.map { distinctExercises ->
            val exerciseMeasures =
                filter { it.exerciseId == distinctExercises.first().exerciseId }.map { it.measures.distinctBy { it.measureTypeId } }
                    .flatMap { it.toList() }

            val summary = exerciseMeasures.map { measure ->
                mapOf(measure.measureTypeId to exerciseMeasures.filter { it.measureTypeId == measure.measureTypeId }
                    .sumByDouble { it.actualValue })
            }.distinctBy { it.keys }

            val percentOfWork = with(distinctExercises) { actualWorkCalculator() * 100 / expectedWorkCalculator() }

            ExerciseSummaryDto(distinctExercises.first().exerciseId, summary, percentOfWork, distinctExercises)
        }
    }
}
