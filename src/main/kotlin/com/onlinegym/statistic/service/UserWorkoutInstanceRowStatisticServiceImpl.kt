package com.onlinegym.statistic.service

import com.onlinegym.statistic.entity.UserWorkoutInstanceRowStatisticData
import com.onlinegym.statistic.repository.UserWorkoutInstanceRepository
import com.onlinegym.statistic.repository.UserWorkoutInstanceRowStatisticRepository
import reactor.core.publisher.Mono

class UserWorkoutInstanceRowStatisticServiceImpl(private val workoutInstanceRepository: UserWorkoutInstanceRepository,
    private val repository: UserWorkoutInstanceRowStatisticRepository) : UserWorkoutInstanceRowStatisticService {

    override fun save(userWorkoutInstance: UserWorkoutInstanceRowStatisticData) = repository.save(userWorkoutInstance)

    //
//    override fun save(workoutDataList: List<WorkoutData>) = repository.save(workoutDataList)
//
//    override fun findByUserId(userId: String) = repository.findByUserId(userId)
//
//    override fun findByUserIdAndWorkoutProgramId(name: String, currentWorkoutProgramId: String) =
//        repository.findByUserIdAndWorkoutProgramId(name, currentWorkoutProgramId)
//
//    override fun findByUserId(principal: Mono<out Principal>) = principal.flatMapMany {
//        repository.findByUserId(it.name)
//    }
//
//    override fun findByUserIdAndWorkoutId(userId: String, workoutId: String) = repository.findByUserIdAndWorkoutId(userId, workoutId)
//
//    override fun getUserHistory(principal: Mono<out Principal>, skip: Long, take: Long): Flux<WorkoutData> = principal.flatMapMany {
//        if (take == 0L) {
//            repository.getUserHistory(it.name)
//        } else {
//            repository.getUserHistory(it.name, skip, take)
//        }
//    }
//
//    override fun getWorkoutDataById(workoutDataId: String): Mono<WorkoutData> {
//        return repository.findById(workoutDataId)
//    }
//
//    override fun getThe5MostPopularWorkoutForUser(principal: Mono<out Principal>) = principal.flatMapMany {
//        repository.findTheMostPopularWorkoutTraining(it.name).take(5)
//    }
    override fun getStatisticForWorkout(userId: String, trainingWorkoutId: String): Mono<List<UserWorkoutInstanceRowStatisticData>> =
        workoutInstanceRepository.findByTrainingWorkoutIdAndUserId(userId, trainingWorkoutId).collectList().flatMap {
            repository.findByUserWorkoutInstanceIdList(it.map { userWI -> userWI.id }).collectList()
        }

    override fun getUserWorkoutInstanceRowStatistic(id: String): Mono<UserWorkoutInstanceRowStatisticData> =
        repository.findById(id)

//        repository.findByUserWorkoutInstanceId(trainingWorkoutId)

}
