package com.onlinegym.statistic.service

import com.onlinegym.statistic.dto.ExerciseResultDto
import com.onlinegym.statistic.dto.WorkoutInstanceFinishResult
import com.onlinegym.statistic.dto.WorkoutInstanceResultDto
import com.onlinegym.statistic.dto.toEntity
import com.onlinegym.statistic.entity.*
import com.onlinegym.statistic.repository.UserWorkoutInstanceRepository
import reactor.core.publisher.Mono

class UserWorkoutInstanceServiceImpl(private val userWorkoutInstanceRepository: UserWorkoutInstanceRepository,
    private val userExerciseRowStatisticsService: UserExerciseRowStatisticsService,
    private val userWorkoutInstanceRowStatisticService: UserWorkoutInstanceRowStatisticService,
    private val userWorkoutInstanceCalculatedStatisticService: UserWorkoutInstanceCalculatedStatisticService,
    private val userWorkoutProgramRowStatisticService: UserWorkoutProgramRowStatisticService) : UserWorkoutInstanceService {

    override fun getAllWorkoutInstanceForUserWorkoutProgram(userId: String,
        workoutProgramId: String): Mono<List<UserWorkoutInstance>> =
        userWorkoutInstanceRepository.findAllWorkoutInstanceByUserIdAndUserWorkoutProgramId(userId, workoutProgramId).collectList()

    override fun getWorkoutInstance(userWorkoutInstanceId: String): Mono<UserWorkoutInstance> =
        userWorkoutInstanceRepository.findById(userWorkoutInstanceId)

    override fun getWorkoutInstanceList(userWorkoutInstanceId: List<String>): Mono<List<UserWorkoutInstance>> =
        userWorkoutInstanceRepository.findByRangeId(userWorkoutInstanceId).collectList()


    override fun addNewWorkoutInstance(userWorkoutInstance: UserWorkoutInstance): Mono<UserWorkoutInstance> =
        userWorkoutInstanceRepository.save(userWorkoutInstance)


    override fun finishWorkoutInstance(user: User, workoutInstanceResultDto: WorkoutInstanceResultDto): Mono<WorkoutInstanceFinishResult> =
        userWorkoutInstanceRepository.findByTrainingWorkoutInstanceAndUserId(user.id, workoutInstanceResultDto.workoutInstanceId).flatMap {
            userWorkoutProgramRowStatisticService.getStatisticForWorkoutProgramAndUser(it.userId, it.userWorkoutProgramId)
                .switchIfEmpty(userWorkoutProgramRowStatisticService.save(createUserWorkoutProgramRowStatistic(it)))
                .flatMap { _ ->
                    val exerciseStatisticList =
                        workoutInstanceResultDto.exercisesResultList.map { set -> createUserExerciseStatisticData(set, it) }
                    userWorkoutInstanceRowStatisticService.getStatisticForWorkout(it.userId, it.trainingWorkoutId)
                        .defaultIfEmpty(emptyList())
                        .flatMap { userWorkoutInstanceRowStatisticList ->
                            userExerciseRowStatisticsService.save(exerciseStatisticList).collectList().flatMap { exerciseStatisticList ->
                                userWorkoutInstanceRowStatisticService.save(createUserWorkoutInstanceRowStatisticData(it, workoutInstanceResultDto
                                    .toEntity(exerciseStatisticList.map { exerciseStat -> exerciseStat.id })))
                                    .flatMap { userWorkoutInstanceRowStatisticData ->
                                        userWorkoutInstanceCalculatedStatisticService
                                            .getWorkoutInstanceResultByWorkoutInstanceResult(userWorkoutInstanceRowStatisticData,
                                                userWorkoutInstanceRowStatisticList.plus(userWorkoutInstanceRowStatisticData))
                                    }.flatMap { result ->
                                        userWorkoutInstanceRepository.save(it.copy(status = UserWorkoutInstanceStatus.FINISHED,
                                            percentComplete = result.percentOfCompletedWork, workoutDifficalt = workoutInstanceResultDto.workoutDifficalt))
                                            .map {
                                                result
                                            }
                                    }
                            }
                        }
                }
        }

    override fun updateWorkoutInstance(userWorkoutInstance: UserWorkoutInstance): Mono<UserWorkoutInstance> =
        userWorkoutInstanceRepository.save(userWorkoutInstance)


    override fun getAllUserWorkoutInstanceForTrainingWorkout(userId: String, trainingWorkoutId: String): Mono<List<UserWorkoutInstance>> =
        userWorkoutInstanceRepository.findByTrainingWorkoutIdAndUserId(userId, trainingWorkoutId).collectList()


    private fun createUserWorkoutProgramRowStatistic(it: UserWorkoutInstance) =
        UserWorkoutProgramRowStatisticData(userWorkoutProgramId = it.userWorkoutProgramId, userId = it.userId)

    private fun createUserExerciseStatisticData(set: ExerciseResultDto, it: UserWorkoutInstance) =
        UserExerciseRowStatisticData(exerciseId = set.exerciseId, userId = it.userId,
            exerciseType = set.exerciseType, measures = set.measures, actualRestTime = set.actualRestTime, restTime = set.restTime)

    private fun createUserWorkoutInstanceRowStatisticData(it: UserWorkoutInstance, result: WorkoutInstanceResult) =
        UserWorkoutInstanceRowStatisticData(userId = it.userId, userWorkoutInstanceId = it.id,
            workoutInstanceResult = result, workoutDifficalt = result.workoutDifficalt)

    //    override fun updateWorkoutInstance(userWorkoutInstance: Mono<UserWorkoutInstance>): Mono<UserWorkoutInstance> =
//        userWorkoutInstance.flatMap {
//            userWorkoutInstanceRepository.save(it)
//        }
//
//
//    override fun addNewWorkoutInstance(userId: String, workoutProgramId: String, scheduledDayDto: ScheduledWorkout): Mono<UserWorkoutInstance> {
//        val userWorkoutSet = createUserWorkoutSet(workoutProgramId, userId, scheduledDayDto)
//        return userWorkoutInstanceRepository.save(userWorkoutSet)
//    }
//
//    private fun createUserWorkoutSet(workoutProgramId: String, userId: String, scheduledDayDto: ScheduledWorkout) =
//        UserWorkoutInstance(userWorkoutProgramId = workoutProgramId, userId = userId,
//            listOfWorkoutPhases = scheduledDayDto.listOfWorkoutPhases)
//
//    override fun addNewListWorkoutInstance(userId: String, userWorkoutProgramId: String, scheduledDayDto: List<ScheduledWorkout>):
//        Mono<List<UserWorkoutInstance>> =
//        userWorkoutInstanceRepository.saveAll(scheduledDayDto.map {
//            createUserWorkoutSet(userWorkoutProgramId, userId, it)
//        }).collectList()
//
    override fun addNewListWorkoutInstance(userWorkoutInstance: List<UserWorkoutInstance>): Mono<List<UserWorkoutInstance>> =
        userWorkoutInstanceRepository.saveAll(userWorkoutInstance).collectList()

}

