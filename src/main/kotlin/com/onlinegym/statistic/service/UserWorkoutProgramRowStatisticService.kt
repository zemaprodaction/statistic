package com.onlinegym.statistic.service

import com.onlinegym.statistic.entity.UserWorkoutProgramRowStatisticData
import reactor.core.publisher.Mono

interface UserWorkoutProgramRowStatisticService {

    fun save(rowStatisticData: UserWorkoutProgramRowStatisticData): Mono<UserWorkoutProgramRowStatisticData>

    fun getStatisticForWorkoutProgramAndUser(userId: String, userWorkoutProgramId: String): Mono<UserWorkoutProgramRowStatisticData>
}
