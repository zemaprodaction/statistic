package com.onlinegym.statistic.service

import com.onlinegym.statistic.dto.UserWorkoutProgramCalculateStatisticData
import reactor.core.publisher.Mono

interface UserWorkoutProgramStatisticService {

    fun getUserWorkoutProgramCalculateStatistic(userId: String, userWorkoutProgramId: String): Mono<UserWorkoutProgramCalculateStatisticData>

}
