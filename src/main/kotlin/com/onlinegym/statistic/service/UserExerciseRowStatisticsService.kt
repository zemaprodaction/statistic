package com.onlinegym.statistic.service

import com.onlinegym.statistic.entity.UserExerciseRowStatisticData
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.security.Principal

interface UserExerciseRowStatisticsService {

//    fun getExerciseCountGroupingByType(principal: Mono<out Principal>, dataRange: String): Mono<ExerciseGroupStatistic>

    fun save(userExerciseRowStatisticData: UserExerciseRowStatisticData): Mono<UserExerciseRowStatisticData>

    fun save(userExerciseRowStatisticDataList: List<UserExerciseRowStatisticData>): Flux<UserExerciseRowStatisticData>

    fun getUserExerciseStatisticForUser(principal: Mono<out Principal>): Flux<UserExerciseRowStatisticData>

    fun getUserExerciseStatisticForUser(userid: String): Flux<UserExerciseRowStatisticData>
//
//    fun findExerciseById(id: String): Mono<ExerciseData>
//
    fun getExerciseByRangeId(ids: List<String>): Mono<List<UserExerciseRowStatisticData>>
//
    fun getRecordForExercise(principal: Mono<out Principal>, exerciseId: String): Mono<UserExerciseRowStatisticData>
}
