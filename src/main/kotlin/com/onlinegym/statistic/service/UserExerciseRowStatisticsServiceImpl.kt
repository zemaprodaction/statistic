package com.onlinegym.statistic.service

import com.onlinegym.statistic.entity.UserExerciseRowStatisticData
import com.onlinegym.statistic.repository.UserExerciseRowStatisticRepository
import com.onlinegym.statistic.repository.ExerciseRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.security.Principal

class UserExerciseRowStatisticsServiceImpl(
    private val exerciseRepository: ExerciseRepository,
    private val userExerciseRowStatisticRepository: UserExerciseRowStatisticRepository) : UserExerciseRowStatisticsService {

    override fun getUserExerciseStatisticForUser(principal: Mono<out Principal>): Flux<UserExerciseRowStatisticData> {
        return principal.flatMapMany {
            userExerciseRowStatisticRepository.findByUserId(it.name)
        }
    }

    override fun getUserExerciseStatisticForUser(userid: String): Flux<UserExerciseRowStatisticData> =
        userExerciseRowStatisticRepository.findByUserId(userid)

    //
//    override fun findExerciseById(id: String): Mono<ExerciseData> = exerciseDataStatisticRepository.findExerciseDataById(id)
    override fun getExerciseByRangeId(ids: List<String>): Mono<List<UserExerciseRowStatisticData>> =
        userExerciseRowStatisticRepository.findExerciseDataByRangeId(ids).collectList()

    override fun getRecordForExercise(principal: Mono<out Principal>, exerciseId: String): Mono<UserExerciseRowStatisticData> {
        return principal.flatMap { p ->
            userExerciseRowStatisticRepository.findExerciseDataByExerciseId(p.name, exerciseId).collectList().map {
                it.maxByOrNull { exerciseData -> exerciseData.measures.sumOf { m -> m.actualValue } }
                    ?: UserExerciseRowStatisticData(exerciseId = exerciseId, userId = p.name)
            }
        }
    }
//
//    override fun getExerciseCountGroupingByType(principal: Mono<out Principal>, dataRange: String): Mono<ExerciseGroupStatistic> {
//        val cache: Map<String, Exercise> = mutableMapOf()
//        return principal.flatMap {
//            exerciseDataStatisticRepository.findAllExerciseDataStatisticsByUserAndDataRange(it.name, DateRange.valueOf(dataRange.toUpperCase()))
//                .collectList()
//                .map { exerciseDataList ->
//                    val map = mutableMapOf<String, Int>()
//                    exerciseDataList.forEach { exerciseData ->
//                        val exercise = cache.getOrDefault(exerciseData.exerciseId,
//                            exerciseRepository.findExerciseById(exerciseData.exerciseId).block() ?: throw Exception("Wrong exercise"))
//                        exercise.exerciseType.let { type ->
//                            map.merge(type.toLowerCase(), 1, Int::plus)
//                        }
//                    }
//                    ExerciseGroupStatistic(map)
//                }
//        }
//    }

    override fun save(userExerciseRowStatisticData: UserExerciseRowStatisticData): Mono<UserExerciseRowStatisticData> =
        userExerciseRowStatisticRepository.save(userExerciseRowStatisticData)


    override fun save(userExerciseRowStatisticDataList: List<UserExerciseRowStatisticData>) =
        userExerciseRowStatisticRepository.save(userExerciseRowStatisticDataList)

}
