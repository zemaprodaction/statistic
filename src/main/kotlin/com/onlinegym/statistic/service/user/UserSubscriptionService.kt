package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.dto.*
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import reactor.core.publisher.Mono
import java.security.Principal

interface UserSubscriptionService {
    fun subscribeUserToWorkoutProgram(principalMono: Mono<out Principal>, subscribtionDto: Mono<SubscribtionDto>): Mono<User>
    fun unsubscribeUserToWorkoutProgram(principal: Mono<out Principal>): Mono<User>

    fun userRestartWorkoutProgram(principal: Mono<out Principal>): Mono<UserWorkoutProgram>
}
