package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.repository.TrainingWorkoutProgramRepository

class TrainingWorkoutProgramServiceImpl(private val repository: TrainingWorkoutProgramRepository) : TrainingWorkoutProgramService {

    override fun getTrainingWorkoutProgram(workoutProgramId: String) = repository.getTrainingWorkoutProgramById(workoutProgramId)
}
