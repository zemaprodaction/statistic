package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.entity.UserWorkoutProgramStatus
import com.onlinegym.statistic.repository.UserWorkoutProgramArchiveRepository
import com.onlinegym.statistic.repository.UserWorkoutProgramRepository

class UserWorkoutProgramArchiveServiceImpl(private val repository: UserWorkoutProgramArchiveRepository,
    private val workoutProgramRepository: UserWorkoutProgramRepository) : UserWorkoutProgramArchiveService {
//    override fun getUserWorkoutProgramArchiveById(userWorkoutProgramId: String): Mono<UserWorkoutProgramArchive> =
//        repository.findUserWorkoutProgramArchiveById(userWorkoutProgramId)
//
    override fun getAllWorkoutProgramArchiveForUser(user: User) = repository
        .findAllUserWorkoutProgramArchiveByUserId(user.id)

    override fun addWorkoutProgramToArchive(userWorkoutProgram: UserWorkoutProgram) =
        workoutProgramRepository.save(userWorkoutProgram.copy(status = UserWorkoutProgramStatus.ARCHIVED))


}
