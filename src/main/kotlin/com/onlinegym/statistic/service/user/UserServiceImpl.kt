package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.repository.UserRepository
import reactor.core.publisher.Mono
import java.security.Principal

class UserServiceImpl(
    private val repository: UserRepository,
) : UserService {

    override fun save(user: User): Mono<User> {
        return repository.save(user)
    }

    override fun findById(userId: String): Mono<User> {
        return repository.findUserById(userId).switchIfEmpty(repository.save(User(userId)))
    }

    override fun findById(principal: Mono<out Principal>): Mono<User> {
        return principal.flatMap {
            findById(it.name)
        }
    }

    //    override fun subscribeToWorkoutProgram(principal: Mono<out Principal>, subscribtionDto: Mono<SubscribtionDto>): Mono<User> =
//        principal.flatMap {
//            findById(it.name).defaultIfEmpty(User(id = it.name))
//        }
//            .zipWith(workoutProgram).flatMap {
    override fun getAllUsers(): Mono<List<User>> = repository.findAllUsers().collectList()
//                userWorkoutProgramService.getActiveWorkoutProgramForUser(it.t1)
//                    .defaultIfEmpty(UserWorkoutProgram(userId = "", userWorkoutProgramId = ""))
//                    .flatMap { activeUserWp ->
//                        if (!it.t1.activeWorkoutProgramId.isNullOrBlank() && activeUserWp.status !== UserWorkoutProgramStatus.FINISHED) {
//                            Mono.error(StatisticServiceException(HttpStatus.NOT_ACCEPTABLE,
//                                "Number of allowed subscriptions has been reached (1 subscriptions is allowed), unsubscribe first"))
//                        } else {
//                            val monoWP: Mono<UserWorkoutProgram>
////                            if (activeUserWp.status === UserWorkoutProgramStatus.FINISHED) {
////                                monoWP = userWorkoutProgramService.delete(activeUserWp).flatMap {
////                                    addToArchive(activeUserWp)
////                                }
////                            } else {
//                                monoWP = activeUserWp.toMono()
////                            }
//                            monoWP.flatMap { _ ->
//                                userWorkoutProgramService.getUserWorkoutProgramByIdAndUser(it.t1.id, it.t2.id)
//                                    .defaultIfEmpty(UserWorkoutProgram(userWorkoutProgramId = it.t2.id, userId = it.t1.id))
//                                    .flatMap { userWp ->
//                                        userWorkoutProgramService.save(userWp).flatMap { _ ->
//                                            val newUser = it.t1.copy(activeWorkoutProgramId = it.t2.id)
//                                            save(newUser)
//                                        }
//                                    }
//                            }
//                        }
//                    }
//            }

//    private fun addToArchive(activeUserWp: UserWorkoutProgram): Mono<UserWorkoutProgram> {
//        val defaultScheduler = UserWorkoutProgramScheduler(emptyMap())
//        val firstDate = userWorkoutProgramStatisticService.getFirstActualWorkoutDay(activeUserWp.schedulerUser
//            ?: defaultScheduler)
//        val lastDate = userWorkoutProgramStatisticService.getActualFinishedDateWorkoutProgram(
//            activeUserWp.schedulerUser ?: defaultScheduler)
//        val archive = UserWorkoutProgramArchive(schedulerUserId = activeUserWp.schedulerUser ?: defaultScheduler, endDate = lastDate ?: LocalDateTime.now(),
//            startDate = firstDate ?: throw Exception("Workout program was not started"), userId = activeUserWp.userId,
//            userWorkoutProgramId = activeUserWp.userWorkoutProgramId)
//        return userWorkoutProgramArchiveService.save(archive).map { activeUserWp }
//    }


//    override fun userStartWorkoutProgram(principal: Mono<out Principal>) =
//        principal.flatMap {
//            findById(it.name)
//        }.flatMap {
//            if (it.activeWorkoutProgramId == null) {
//                error(Exception("User does not subscribe to any workout program"))
//            } else {
//                userWorkoutProgramService.getActiveWorkoutProgramForUser(it).flatMap { userWp ->
//                    if (userWp.schedulerUser != null) {
//                        userWp.schedulerUser.toMono()
//                    } else {
//                        schedulerService.createSchedulerToWorkoutProgram(it.activeWorkoutProgramId, userWp.userId).flatMap { scheduler ->
//                            userWorkoutProgramService.getActiveWorkoutProgramForUser(it).defaultIfEmpty(
//                                UserWorkoutProgram(it.activeWorkoutProgramId, userWorkoutProgramId = "", scheduler, it.id,
//                                    status = UserWorkoutProgramStatus.IN_PROGRESS))
//                                .flatMap { uwp ->
//                                    userWorkoutProgramService
//                                        .save(uwp.copy(schedulerUser = scheduler, status = UserWorkoutProgramStatus.IN_PROGRESS))
//                                        .map {
//                                            scheduler
//                                        }
//                                }
//
//                        }
//                    }
//                }
//
//            }
//        }
//

//    override fun finishedWorkoutScheduler(principal: Mono<out Principal>, workoutProgramStatisticDto: Mono<WorkoutProgramStatisticDto>) =
//        principal.flatMap {
//            findById(it.name)
//        }.flatMap {
//            userWorkoutProgramService.getActiveWorkoutProgramForUser(it).zipWith(workoutProgramStatisticDto).flatMap { tuple ->
//                val scheduler = tuple.t1.schedulerUser
//                if (scheduler?.calendar == null) {
//                    error(Exception("User does not start workout program"))
//                }
//                workoutProgramStatisticService.saveWorkoutProgramStatistics(it.id, tuple.t2).flatMap { workoutSummaryDto ->
//                    val calendar = scheduler.calendar
//                    val updateScheduler = schedulerService.workoutProgramScheduler(calendar, tuple, workoutSummaryDto)
//                    var isThereNonFinishedWorkout = false
//
//                    if (updateScheduler !== null) {
//                        isThereNonFinishedWorkout = updateScheduler.calendar.values.flatMap { it.scheduledWorkoutList }.find {
//                            !it.isFinished
//                        } !== null
//                    }
//                    if (isThereNonFinishedWorkout) {
//                        userWorkoutProgramService.save(tuple.t1.copy(schedulerUser = updateScheduler)).flatMap { _ ->
//                            workoutProgramStatisticService.saveWorkoutProgramStatistics(it.id, tuple.t2)
//                        }.map { workoutSummaryDto }
//                    } else {
//                        userWorkoutProgramService.save(tuple.t1.copy(schedulerUser = updateScheduler, status = UserWorkoutProgramStatus.FINISHED))
//                            .map { workoutSummaryDto }
//                    }
//                }
//
//            }
//        }

//


}
