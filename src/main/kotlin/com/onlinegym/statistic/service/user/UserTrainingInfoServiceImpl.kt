package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.dto.UserTrainingInfoDto
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.WorkoutProgramScheduler
import com.onlinegym.statistic.entity.toDto
import com.onlinegym.statistic.repository.UserRepository
import com.onlinegym.statistic.service.UserWorkoutProgramService
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.security.Principal

class UserTrainingInfoServiceImpl(
    private val userWorkoutProgramService: UserWorkoutProgramService,
    private val userService: UserService,
    private val schedulerService: WorkoutProgramSchedulerService,
) : UserTrainingInfoService {

    override fun getUserTrainingInfo(principal: Mono<out Principal>) =
        principal.flatMap {
            getUserTrainingInfo(it.name)
        }

    override fun getUserTrainingInfo(userId: String): Mono<UserTrainingInfoDto> =
        userService.findById(userId).flatMap {
            if (it.activeWorkoutProgramId == null) {
                UserTrainingInfoDto(userId = it.id).toMono()
            } else {
                userWorkoutProgramService.getActiveWorkoutProgramForUser(it)
                    .flatMap { userWP ->
                        if (userWP.workoutProgramSchedulerId !== null) {
                            schedulerService.getSchedulerById(userWP.workoutProgramSchedulerId)
                                .map { scheduler ->
                                    UserTrainingInfoDto(userId = userWP.userId, userWorkoutProgram = userWP.toDto(scheduler))

                                }
                        } else {
                            UserTrainingInfoDto(userId = userWP.userId, userWorkoutProgram = userWP.toDto()).toMono()
                        }

                    }
            }
        }

}
