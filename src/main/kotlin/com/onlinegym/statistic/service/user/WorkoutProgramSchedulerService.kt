package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.dto.TrainingWorkoutProgramDto
import com.onlinegym.statistic.dto.WorkoutProgramSchedulerDto
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.entity.WorkoutProgramScheduler
import reactor.core.publisher.Mono

interface WorkoutProgramSchedulerService {

    fun generateSchedulerWorkoutProgram(userWorkoutProgram: UserWorkoutProgram): Mono<WorkoutProgramSchedulerDto>
    fun getSchedulerById(workoutProgramSchedulerId: String): Mono<WorkoutProgramSchedulerDto>
    fun removeScheduler(workoutProgramSchedulerId: String): Mono<Long>
//    fun createSchedulerToWorkoutProgram(workoutProgram: String, userId: String): Mono<WorkoutProgramScheduler>
//    fun workoutProgramScheduler(calendar: Map<LocalDateTime, ScheduledDay>,
//        it: Tuple2<UserWorkoutProgram, WorkoutProgramStatisticDto>, workoutSummaryDto: WorkoutSummaryDto): UserWorkoutProgramScheduler?
}
