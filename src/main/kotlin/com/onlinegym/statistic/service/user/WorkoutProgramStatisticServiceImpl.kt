package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.dto.*
import com.onlinegym.statistic.service.*
import reactor.core.publisher.Mono
import reactor.util.function.Tuple2
import java.security.Principal

class WorkoutProgramStatisticServiceImpl(
) : WorkoutProgramStatisticService {

//    override fun saveWorkoutProgramStatistics(
//        principal: Mono<out Principal>,
//        workoutProgramDto: Mono<WorkoutProgramStatisticDto>
//    ): Mono<WorkoutSummaryDto> {
//
//        return Mono.zip(principal, workoutProgramDto)
//            .flatMap { tuple ->
//                saveWorkoutProgramStatistics(tuple.t1.name, tuple.t2)
//            }
//    }

//    override fun saveWorkoutProgramStatistics(principalId: String, workoutProgramDto: WorkoutProgramStatisticDto): Mono<WorkoutSummaryDto> {
//
//        val workoutProgramStatistic = WorkoutProgramStatistic(workoutProgramId = workoutProgramDto.workoutProgramId,
//            finishedWorkoutInstance = listOf(workoutProgramDto.workout.workoutId), userId = principalId)
//        return workoutProgramStatisticRepository.saveWorkoutProgramStatistic(principalId, workoutProgramStatistic).flatMap { _ ->
//            userExerciseRowStatisticsService.save(createExerciseDataList(principalId, workoutProgramDto)).collectList().flatMap {
//                val exerciseDataList = it.map { it.id }
//                workoutStatisticsService.calculateWorkoutSummaryWithoutId(principalId, workoutProgramDto.workout, workoutProgramDto.workout.workoutId)
//                    .flatMap { summary ->
//                        workoutDataStatisticService.save(workoutProgramDto.workout.toWorkoutData(principalId,
//                            exerciseDataList, workoutProgramDto.workoutProgramId, summary.percentOfCompletedWork, summary.totalWork)).map {
//                            summary.copy(workoutDataId = it.id)
//                        }
//                    }
//            }
//        }
//    }

//    private fun createExerciseDataList(userId: String, t2: WorkoutProgramStatisticDto): List<ExerciseData> {
//        val listExercise = t2.workout.workoutData.exercises
//        return listExercise.map {
//            ExerciseData(exerciseId = it.exerciseId, measures = it.measures, userId = userId,
//                actualRestTime = it.actualRestTime, restTime = it.restTime, exerciseType = it.exerciseType)
//        }
//    }
//
//
//    override fun getAllWorkoutProgramStatistics(principal: Mono<out Principal>): Flux<WorkoutProgramStatistic> =
//        principal.flatMapMany {
//            workoutProgramStatisticRepository.findWorkoutProgramForUser(it.name)
//        }

//    override fun getWorkoutProgramStatistic(principalMono: Mono<out Principal>, workoutProgramId: String): Mono<CurrentWorkoutProgramStatistic> =
//        principalMono.flatMap { principal ->
//            getWorkoutProgramStatistic(principal.name, workoutProgramId)
//        }

//    override fun getWorkoutProgramStatistic(userId: String, workoutProgramId: String): Mono<UserWorkoutProgramCalculateStatisticData> =
//        workoutProgramStatisticRepository.findWorkoutProgramById()
//        workoutDataStatisticService.findByUserIdAndWorkoutProgramId(userId, workoutProgramId).collectList().flatMap {
//            workoutProgramStatisticRepository.findWorkoutProgramById(userId, workoutProgramId).map { wps ->
//                val average = it.map { workout -> workout.percentOfCompletedWork }.average().toInt()
//                CurrentWorkoutProgramStatistic(average, 0, wps)
//            }.flatMap {
//                getPercentProgressOfWorkoutProgram(userId, workoutProgramId).map { percent ->
//                    it.copy(percentDone = percent)
//                }
//            }
//        }

//    override fun getPercentProgressOfWorkoutProgram(userId: String, workoutProgramId: String) =
//        userWorkoutProgramService.getUserWorkoutProgramByIdAndUser(userId, workoutProgramId)
//            .defaultIfEmpty(UserWorkoutProgram("", "", null, userId = ""))
//            .map {
//                if (it.schedulerUser != null) {
//                    val finshedWorkout = it.schedulerUser.calendar.values.flatMap { it.scheduledWorkoutList }.filter {
//                        it.isFinished
//                    }.size
//                    (finshedWorkout * 100) / it.schedulerUser.calendar.values.size
//                } else {
//                    0
//                }
//            }


    private fun Tuple2<Principal, WorkoutProgramStatisticDto>.statistic() = t2
    private fun Tuple2<Principal, WorkoutProgramStatisticDto>.principal() = t1
}
