package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram

interface UserSubscriptionValidateService {

    fun isUserCanSubscribeToWorkoutProgram(user: User, workoutProgram: UserWorkoutProgram?): Boolean

    fun isUserCanArchiveCurrentWorkoutProgram(user: User, workoutProgram: UserWorkoutProgram): Boolean
}
