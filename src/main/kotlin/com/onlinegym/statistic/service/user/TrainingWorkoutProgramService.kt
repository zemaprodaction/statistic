package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.dto.TrainingWorkoutProgramDto
import reactor.core.publisher.Mono

interface TrainingWorkoutProgramService {
    fun getTrainingWorkoutProgram(workoutProgramId: String): Mono<TrainingWorkoutProgramDto>
}
