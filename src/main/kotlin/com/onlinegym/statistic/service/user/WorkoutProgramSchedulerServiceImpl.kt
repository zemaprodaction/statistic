package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.dto.WorkoutProgramSchedulerDto
import com.onlinegym.statistic.dto.WorkoutWeekDto
import com.onlinegym.statistic.entity.*
import com.onlinegym.statistic.nowUTCTime
import com.onlinegym.statistic.repository.WorkoutProgramSchedulerRepository
import com.onlinegym.statistic.service.UserWorkoutInstanceService
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.LocalDateTime

class WorkoutProgramSchedulerServiceImpl(private val trainingWorkoutProgramService: TrainingWorkoutProgramService,
    private val repository: WorkoutProgramSchedulerRepository, private val userWorkoutInstanceService: UserWorkoutInstanceService) :
    WorkoutProgramSchedulerService {

    override fun generateSchedulerWorkoutProgram(userWorkoutProgram: UserWorkoutProgram): Mono<WorkoutProgramSchedulerDto> =
        trainingWorkoutProgramService.getTrainingWorkoutProgram(userWorkoutProgram.trainingWorkoutProgramId).flatMap {
            val listOfDaysMono =
                it.listOfWeeks
                    .flatMapIndexed { index: Int, week: WorkoutWeekDto -> week.listOfDays.mapIndexed { dayIndex, day -> index to dayIndex to day } }
                    .map { weekDayWorkout ->
                        val weekIndex = weekDayWorkout.first.first
                        val dayIndex = weekDayWorkout.first.second
                        val workout = weekDayWorkout.second
                        val scheduledWorkoutList = workout.listOfScheduledWorkouts.map { sheduleWorkout ->
                            UserWorkoutInstance(userWorkoutProgramId = userWorkoutProgram.id,
                                trainingWorkoutId = sheduleWorkout.workoutId,
                                userId = userWorkoutProgram.userId,
                                listOfWorkoutPhases = sheduleWorkout.listOfWorkoutPhases)
                        }
                        userWorkoutInstanceService.addNewListWorkoutInstance(scheduledWorkoutList).map { workoutList ->
                            if (weekIndex == 0 && dayIndex == 0) {
                                SchedulerDay(userWorkoutInstanceList = workoutList.map { workout -> workout.id }, date = LocalDateTime.now())
                            } else {
                                SchedulerDay(userWorkoutInstanceList = workoutList.map { workout -> workout.id }, date = LocalDateTime.now()
                                    .plusDays((weekIndex * 7 + workout.dayIndex).toLong()))
                            }
                        }
                    }
            Flux.fromIterable(listOfDaysMono).flatMap { day -> day }.collectList().flatMap { dayList ->
                repository.save(WorkoutProgramScheduler(listOfDays = dayList.sortedBy { day -> day.date }))
            }.flatMap {
                convertSchedulerToSchedulerDto(it)
            }
        }

    override fun getSchedulerById(workoutProgramSchedulerId: String): Mono<WorkoutProgramSchedulerDto> =
        repository.findSchedulerById(workoutProgramSchedulerId).flatMap {
            convertSchedulerToSchedulerDto(it)
        }

    private fun convertSchedulerToSchedulerDto(it: WorkoutProgramScheduler): Mono<WorkoutProgramSchedulerDto> {
        val schedulerDayDtoListMono = it.listOfDays.map { schedulerDay ->
            userWorkoutInstanceService.getWorkoutInstanceList(schedulerDay.userWorkoutInstanceList).map { userWorkoutInstanceList ->
                schedulerDay.toDto(userWorkoutInstanceList)
            }
        }
        return Flux.fromIterable(schedulerDayDtoListMono).flatMap { it }.collectList().map { schedulerDayList ->
            it.toDto(schedulerDayList.sortedBy { it.date })
        }
    }

    override fun removeScheduler(workoutProgramSchedulerId: String): Mono<Long> =
        repository.deleteById(workoutProgramSchedulerId)


    //    override fun createSchedulerToWorkoutProgram(workoutProgram: Mono<TrainingWorkoutProgramDto>, userId: String): Mono<WorkoutProgramScheduler> =
//        workoutProgram.flatMap {
    val today: LocalDateTime = nowUTCTime()
//            val schedulerMap = mutableMapOf<LocalDateTime, String>()
//            val firstDay = it.listOfWeeks[0].listOfDays[0]
//            val prevDayIndex = firstDay.dayIndex
//            val userWorkoutInstanceList = mutableListOf<UserWorkoutInstance>()
//            val scheduleDayList = mutableListOf<SchedulerDay>()
//
//            val userWorkoutInstanceListForFirstDay: List<UserWorkoutInstance> = getWorkoutInstanceListForDay(firstDay, it.id, userId)
//            userWorkoutInstanceList.addAll(userWorkoutInstanceListForFirstDay)
//            val firstScheduleDay: SchedulerDay = getScheduleDay(userWorkoutInstanceListForFirstDay, today)
//            scheduleDayList.add(firstScheduleDay)
//            schedulerMap[today] = firstScheduleDay.id
//            it.listOfWeeks.forEachIndexed { index, workoutWeek ->
//                workoutWeek.listOfDays.forEachIndexed { dayIndex, day ->
//                    if (index != 0 || dayIndex != 0) {
//                        val plusDate = index * 7 + kotlin.math.abs(day.dayIndex - prevDayIndex)
//                        val dateOfWorkout = today.plusDays(plusDate.toLong())
//                        val listWorkoutInstanceForThisDay = getWorkoutInstanceListForDay(day, it.id, userId)
//                        userWorkoutInstanceList.addAll(listWorkoutInstanceForThisDay)
//                        day.listOfScheduledWorkouts.map { workout ->
//                            val scheduleDay = getScheduleDay(listWorkoutInstanceForThisDay, dateOfWorkout)
//                            scheduleDayList.add(scheduleDay)
//                            schedulerMap[dateOfWorkout] = scheduleDay.id
//                        }
//                    }
//                }
//            }
//            schedulerDayRepository.saveAll(scheduleDayList).collectList().flatMap {
//                userWorkoutInstanceService.addNewListWorkoutSetToUserWorkoutProgram(userWorkoutInstanceList).flatMap {
//                    schedulerRepository.save(WorkoutProgramScheduler(schedulerMap))
//                }
//            }
//        }

//    private fun getScheduleDay(userWorkoutInstanceList: List<UserWorkoutInstance>, today: LocalDateTime): SchedulerDay =
//        SchedulerDay(scheduledWorkoutList = userWorkoutInstanceList.map { it.id }, date = today)
//
//
//    private fun getWorkoutInstanceListForDay(firstDay: WorkoutDay, workoutProgramId: String, userId: String): List<UserWorkoutInstance> =
//        firstDay.listOfScheduledWorkouts.map { scWorkout ->
//            UserWorkoutInstance(workoutProgramId = workoutProgramId, userId = userId, listOfWorkoutPhases = scWorkout.listOfWorkoutPhases)
//        }
//
//
//    override fun createSchedulerToWorkoutProgram(workoutProgram: String, userId: String): Mono<WorkoutProgramScheduler> {
//        val monoWorkoutProgram = trainingWorkoutProgramRepository
//            .getTrainingWorkoutProgramById(workoutProgram)
//
//        return createSchedulerToWorkoutProgram(monoWorkoutProgram, userId)
//    }

    //TODO may be need remove it
//    override fun workoutProgramScheduler(calendar: Map<LocalDateTime, ScheduledDay>,
//        it: Tuple2<UserWorkoutProgram, WorkoutProgramStatisticDto>, workoutSummaryDto: WorkoutSummaryDto): UserWorkoutProgramScheduler? {
//        val workoutDay = calendar[it.t2.expectedFinishedDate] ?: error(Exception("Wrong date"))
//        val workoutInstance =
//            workoutDay.scheduledWorkoutList.first { scheduleWorkoutId -> scheduleWorkoutId == it.t2.workout.workoutData.workoutInstanceId }
//        worko
//        val updateWorkoutInstance =
//            workoutInstance.copy(actualDate = LocalDateTime.now(), isFinished = true, workoutDataId = workoutSummaryDto.workoutDataId,
//                percentComplete = workoutSummaryDto.percentOfCompletedWork)
//        val updateDay =
//            workoutDay.copy(scheduledWorkoutList = workoutDay.scheduledWorkoutList.minus(workoutInstance)
//                .plus(updateWorkoutInstance))
//        val updateCalendar = calendar.plus(it.t2.expectedFinishedDate to updateDay)
//        return it.t1.schedulerUser?.copy(calendar = updateCalendar)
//    }

}
