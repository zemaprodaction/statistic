package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.entity.UserWorkoutProgramArchive
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface UserWorkoutProgramArchiveService {

//    fun getUserWorkoutProgramArchiveById(userWorkoutProgramId: String): Mono<UserWorkoutProgram>
//
    fun getAllWorkoutProgramArchiveForUser(user: User): Flux<UserWorkoutProgram>

    fun addWorkoutProgramToArchive(userWorkoutProgram: UserWorkoutProgram): Mono<UserWorkoutProgram>
}
