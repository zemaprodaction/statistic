package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.dto.UserTrainingInfoDto
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import reactor.core.publisher.Mono
import java.security.Principal

interface UserTrainingInfoService {
    fun getUserTrainingInfo(principal: Mono<out Principal>): Mono<UserTrainingInfoDto>
    fun getUserTrainingInfo(userId: String): Mono<UserTrainingInfoDto>

}
