package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.dto.SubscribtionDto
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.excptionHandler.StatisticServiceException
import com.onlinegym.statistic.service.UserWorkoutProgramService
import org.springframework.http.HttpStatus
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import reactor.util.function.Tuple2
import java.security.Principal

class UserSubscriptionServiceImpl(private val userService: UserService,
    private val validationService: UserSubscriptionValidateService,
    private val userWorkoutProgramService: UserWorkoutProgramService,
    private val userWorkoutProgramArchiveService: UserWorkoutProgramArchiveService) :
    UserSubscriptionService {
    override fun subscribeUserToWorkoutProgram(principalMono: Mono<out Principal>, subscribtionDto: Mono<SubscribtionDto>): Mono<User> =
        principalMono.flatMap { principal ->
            userService.findById(principal.name)
                .defaultIfEmpty(User(id = principal.name))
                .zipWith(subscribtionDto)
                .flatMap { userWithSubscriptionDto ->
                    userWorkoutProgramService.getActiveWorkoutProgramForUser(userWithSubscriptionDto.t1)
                        .flatMap {
                            when {
                                validationService.isUserCanSubscribeToWorkoutProgram(userWithSubscriptionDto.t1, it) -> {
                                    saveUser(userWithSubscriptionDto)
                                }
                                validationService.isUserCanArchiveCurrentWorkoutProgram(userWithSubscriptionDto.t1, it) -> {
                                    userWorkoutProgramArchiveService.addWorkoutProgramToArchive(it).flatMap {
                                        saveUser(userWithSubscriptionDto)
                                    }
                                }
                                else -> {
                                    Mono.error(StatisticServiceException(HttpStatus.NOT_ACCEPTABLE,
                                        "Number of allowed subscriptions has been reached (1 subscriptions is allowed), unsubscribe first"))
                                }
                            }
                        }.switchIfEmpty(saveUser(userWithSubscriptionDto))
                }
        }

    override fun userRestartWorkoutProgram(principal: Mono<out Principal>): Mono<UserWorkoutProgram> {
        return principal.flatMap {
            userService.findById(it.name)
        }.flatMap {
            userWorkoutProgramService.getActiveWorkoutProgramForUser(it).flatMap { userWp ->
                if (userWp.workoutProgramSchedulerId === null) {
                    userWp.toMono()
                } else {
                    userWorkoutProgramService.deleteSchedulerFromUserWorkoutProgram(userWp)
                }
            }
        }
    }

    override fun unsubscribeUserToWorkoutProgram(principal: Mono<out Principal>): Mono<User> =
        principal.flatMap {
            userService.findById(it.name).flatMap { user ->
                userWorkoutProgramService.getUserWorkoutProgramById(user.activeWorkoutProgramId
                    ?: error(Exception("Active workout program is null"))).flatMap { userWP ->
                    userWorkoutProgramArchiveService.addWorkoutProgramToArchive(userWP)
                }
                    .flatMap { userService.save(user.copy(activeWorkoutProgramId = null)) }
            }
        }


    private fun saveUser(userWithSubscriptionDto: Tuple2<User, SubscribtionDto>) =
        userWorkoutProgramService.save(UserWorkoutProgram(trainingWorkoutProgramId = userWithSubscriptionDto.t2.trainingWorkoutProgramId,
            userId = userWithSubscriptionDto.t1.id)).flatMap {
            userService.save(User(userWithSubscriptionDto.t1.id, it.id))
        }

}
