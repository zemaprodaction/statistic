package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.dto.UserTrainingInfoDto
import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import reactor.core.publisher.Mono
import java.security.Principal

interface UserService {

    fun save(user: User): Mono<User>

    fun findById(userId: String): Mono<User>

    fun findById(principal: Mono<out Principal>): Mono<User>

    fun getAllUsers(): Mono<List<User>>

//    fun finishedWorkoutScheduler(principal: Mono<out Principal>, workoutProgramStatisticDto: Mono<WorkoutProgramStatisticDto>): Mono<WorkoutSummaryDto>
}
