package com.onlinegym.statistic.service.user

import com.onlinegym.statistic.entity.User
import com.onlinegym.statistic.entity.UserWorkoutProgram
import com.onlinegym.statistic.entity.UserWorkoutProgramStatus

class UserSubscriptionValidateServiceImpl : UserSubscriptionValidateService {

    override fun isUserCanSubscribeToWorkoutProgram(user: User, workoutProgram: UserWorkoutProgram?): Boolean {
        return user.activeWorkoutProgramId == null
    }

    override fun isUserCanArchiveCurrentWorkoutProgram(user: User, workoutProgram: UserWorkoutProgram): Boolean =
        workoutProgram.status == UserWorkoutProgramStatus.FINISHED
}
